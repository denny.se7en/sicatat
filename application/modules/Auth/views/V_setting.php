<div class="card-body">
    <?= alertMsg() ?>
    <form action="<?= base_url('auth/update_account') ?>" method="post">
        <div class="input-group mb-3">
            <input type="text" name="username" id="username" class="form-control" placeholder="Username" value="<?= $user->username; ?>">
            <div class="input-group-append">
                <div class="input-group-text">
                    <span class="fas fa-user-circle"></span>
                </div>
            </div>
        </div>
        <div class="input-group mb-3">
            <input type="password" name="password" id="password" class="form-control" placeholder="Password Lama">
            <div class="input-group-append">
                <div class="input-group-text">
                    <span class="fas fa-lock"></span>
                </div>
            </div>
        </div>
        <div class="input-group mb-3">
            <input type="password" name="password_baru" id="password_baru" class="form-control" placeholder="Password Baru">
            <div class="input-group-append">
                <div class="input-group-text">
                    <span class="fas fa-lock"></span>
                </div>
            </div>
        </div>
        <div class="input-group mb-3">
            <input type="password" name="password_baru2" id="password_baru2" class="form-control" placeholder="Ulangi Password Baru">
            <div class="input-group-append">
                <div class="input-group-text">
                    <span class="fas fa-lock"></span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-8">

            </div>
            <!-- /.col -->
            <div class="col-4">
                <button name="update" id="update" type="submit" class="btn btn-sm btn-primary btn-block">Update</button>
            </div>
            <!-- /.col -->
        </div>
    </form>
</div>

<script>
    document.getElementById("password_baru").addEventListener("keyup", testpassword2);
    document.getElementById("password_baru2").addEventListener("keyup", testpassword2);

    function testpassword2() {
        var fst = document.getElementById("password_baru");
        var snd = document.getElementById("password_baru2");
        if (fst.value == snd.value) {
            document.getElementById("update").disabled = false;
            console.log('benar');
            snd.style.borderColor = "#2EFE2E";
        } else {
            document.getElementById("update").disabled = true;
            console.log('salah');
            snd.style.borderColor = "red";
        }
    }
</script>