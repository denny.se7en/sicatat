<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Si Catat | Log in</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?= base_url('assets/') ?>plugins/fontawesome-free/css/all.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="<?= base_url('assets/') ?>plugins/icheck-bootstrap/icheck-bootstrap.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?= base_url('assets/') ?>css/adminlte.min.css">
    <link rel="stylesheet" href="<?= base_url('assets/') ?>bootstrap/css/bootstrap.min.css">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<style>
    .backstretch img {
        opacity: 0.5;
        filter: alpha(opacity=50);
    }
</style>

<body class="hold-transition login-page">
    <!-- <div style="background-image: url('assets/img/photo1.png');"> -->
    <div class="login-box">
        <div class="login-logo">
            <img src="<?= base_url('assets/') ?>img/logo-pemkot.png" alt="Logo" width="100px" height="auto">
        </div>
        <!-- /.login-logo -->
        <div class="card">
            <div class="login-logo p-2">
                <div><b>Si Catat </b>Aplikasi</div>
            </div>
            <div class="card-body login-card-body">
                <div class="pb-3 d-flex justify-content-center" style="font-weight: bold;">Login</div>
                <?= alertMsg() ?>
                <form action="<?= base_url('Auth/login_action') ?>" method="post">
                    <div class="input-group mb-3">
                        <input type="text" name="username" id="username" class="form-control" placeholder="Username">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-user-circle"></span>
                            </div>
                        </div>
                    </div>
                    <div class="input-group mb-3">
                        <input type="password" name="password" id="password" class="form-control" placeholder="Password">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-lock"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-8">

                        </div>
                        <!-- /.col -->
                        <div class="col-4">
                            <button type="submit" class="btn btn-sm btn-primary btn-block">Login</button>
                        </div>
                        <!-- /.col -->
                    </div>
                </form>

            </div>
            <!-- /.login-card-body -->
        </div>
    </div>
    <!-- /.login-box -->

    <!-- jQuery -->
    <script src="<?= base_url('assets/') ?>plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="<?= base_url('assets/') ?>plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- AdminLTE App -->
    <script src="<?= base_url('assets/') ?>js/adminlte.min.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>assets/js/jquery.backstretch.min.js"></script>
    <script>
        $.backstretch("<?= base_url() ?>assets/img/login_bg.jpg", {
            speed: 5
        });
    </script>
</body>

</html>