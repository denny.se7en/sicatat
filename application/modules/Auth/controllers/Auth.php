<?php (defined('BASEPATH')) or exit('No direct script access allowed');

class Auth extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    private function _guard()
    {
        if ($this->session->jabatan == 'admin') {
            redirect('Dashboard');
        }

        if ($this->session->jabatan == 'viewer') {
            redirect('Dashboard');
        }

    }

    public function index()
    {
        $this->_guard();

        redirect('Auth/login');
    }

    public function login()
    {
        $this->_guard();

        $data = [];
        $this->load->view('V_auth', $data);
    }

    public function login_action()
    {
        $this->_guard();

        $username = $this->input->post('username');
        $password = $this->input->post('password');

        $pengguna = $this->db->select('p.id, username, password, nama, no_hp, r.name as jabatan, role_id')
            ->from('pengguna p')
            ->join('role r', 'r.id = p.role_id', 'left')
            ->where(['username' => $username, 'p._active' => '1'])
            ->get()
            ->row();

        if ($pengguna && password_verify($password, $pengguna->password)) {

            $session_data = [
                'logged_in' => TRUE,
                'nama'      => $pengguna->nama,
                'no_hp'     => $pengguna->no_hp,
                'jabatan'   => $pengguna->jabatan,
                'role_id'   => $pengguna->role_id,
                'user_id'   => $pengguna->id
            ];

            $this->session->set_userdata($session_data);
        } else {
            setAlert('danger', 'Password/username anda salah');
        }

        redirect('Auth/login');
    }

    public function logout()
    {
        if (!$this->session->logged_in) {
            redirect('');
        }
        $this->halocovid->logout();
        $this->session->sess_destroy();

        redirect('');
    }

    public function generate()
    {
        echo password_hash('12345', PASSWORD_BCRYPT, ['cost' => 11]);
    }

    public function setting()
    {
        $user_id = $this->session->userdata('user_id');

        $user = $this->db->from('pengguna')
            ->where(['id' => $user_id])
            ->get()
            ->row();

        $data = [
            'user'      => $user,
            'title'     => 'Setting',
            'navbar'    => true,
            'header'    => true,
            'padding'   => false
        ];
        $this->template->admin('V_setting', $data);
    }
    public function update_account()
    {
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $password_baru = $this->input->post('password_baru');

        $pengguna = $this->db->select('p.id, username, password, nama, no_hp, r.name as jabatan, role_id')
            ->from('pengguna p')
            ->join('role r', 'r.id = p.role_id', 'left')
            ->where(['username' => $username])
            ->get()
            ->row();

        if ($pengguna && password_verify($password, $pengguna->password)) {

            $password_baru = password_hash($password_baru, PASSWORD_BCRYPT, ['cost' => 11]);

            $update = [
                'username' => $username,
                'password' => $password_baru
            ];

            $this->db->where(['id' => $pengguna->id])
                ->update('pengguna', $update);

            setAlert('success', 'Password/username berhasil dirubah');
        } else {
            setAlert('danger', 'Password/username anda salah');
        }
        redirect('Auth/setting');
    }
}
