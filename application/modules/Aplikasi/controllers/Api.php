<?php

(defined('BASEPATH')) or exit('No direct script access allowed');

class Api extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        if ($this->session->jabatan != 'admin') {
            redirect('');
        }

        $this->load->model('Dt_aplikasi');
    }

    public function get_aplikasi($id)
    {
        $aplikasi = $this->db->from('aplikasi a')
            ->where(['id' => $id,'_active' => 1])
            ->get()
            ->row();

        echo json_encode($aplikasi);
    }

    public function get_ajax()
    {
        $kegiatans   = $this->Dt_aplikasi->get_datatables();
        $data   = [];
        $no     = $this->input->post('start');

        foreach ($kegiatans as $index => $item) {
            $no++;
            $item->no = $no . '. ';
        }

        $kegiatans = array_values($kegiatans);

        $output = [
            "draw"              => $this->input->post('draw'),
            "recordsTotal"      => $this->Dt_aplikasi->count_all(),
            "recordsFiltered"   => $this->Dt_aplikasi->count_filtered(),
            "data"              => $kegiatans,
        ];

        echo json_encode($output);
    }

    public function get_versi()
    {
        $id = $this->input->post('id_h');
        $versi = $this->Dt_aplikasi->get_datatables_versi($id);
        $no     = $this->input->post('start');

        foreach ($versi as $index => $item) {
            $no++;
            $item->no = $no . '. ';
        }

        $versi = array_values($versi);

        $output = [
            "draw"              => $this->input->post('draw'),
            "recordsTotal"      => $this->Dt_aplikasi->count_all_versi($id),
            "recordsFiltered"   => $this->Dt_aplikasi->count_filtered_versi($id),
            "data"              => $versi,
        ];

        echo json_encode($output);
    }

}
