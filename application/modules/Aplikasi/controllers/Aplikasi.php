<?php

(defined('BASEPATH')) or exit('No direct script access allowed');

class Aplikasi extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        if ($this->session->jabatan != 'admin') {
            redirect('');
        }
        $this->load->model('Dt_aplikasi');
    }

    public function index()
    {
        redirect('Aplikasi/input');
    }

    public function input()
    {
        if ($this->form_validation->run() == FALSE) {
            $opd = $this->db->from('ref_opd')
                ->get()
                ->result();

            $lokasi = $this->db->from('ref_lokasi')
                ->get()
                ->result();

            $data = [
                'title'     => 'List Aplikasi',
                'navbar'    => true,
                'header'    => true,
                'padding'   => false,
                'opds'      => $opd,
                'lokasis'    => $lokasi
            ];

            $this->template->admin('V_listaplikasi', $data);
        } else {
            redirect('Aplikasi');
        }
    }

    public function editInfoApp()
    {
        $aplikasi_id = $this->input->post('id_edit');
        $update = [
            'no_asset' => $this->input->post('edit_no'),
            'hostname' => $this->input->post('edit_hostname'),
            'ipaddress' => $this->input->post('edit_ip'),
            'nm_internal' => $this->input->post('edit_nama')
        ];
        $this->db->where(['id' => $aplikasi_id])->update('aplikasi', $update);
        redirect('Aplikasi/detail/' . $aplikasi_id);
    }

    public function detail($id)
    {
        if ($this->form_validation->run() == FALSE) {
            $opd = $this->db->from('ref_opd')->get()->result();
            $lokasi = $this->db->from('ref_lokasi')->get()->result();
            $sertf = $this->db->from('ref_sertf')->get()->result();
            $sasaran = $this->db->from('ref_sasaran')->get()->result();
            $dasarhukum = $this->db->from('ref_dasarhukum')->get()->result();
            $kategoriakses = $this->db->from('ref_kategoriakses')->get()->result();
            $publik = $this->db->from('ref_layananpublik')->get()->result();
            $frekuensi = $this->db->from('ref_frekuensipemeliharaan')->get()->result();
            $evaluasi = $this->db->from('ref_evaluasi')->get()->result();
            $dokumen = $this->db->from('ref_dokumen')->get()->result();
            $pj = $this->db->from('ref_pj')->get()->result();
            $pjteknis = $this->db->from('ref_pjteknis')->get()->result();
            $jenisaplikasi = $this->db->from('ref_jenisaplikasi')->get()->result();
            $kematangan = $this->db->from('ref_tingkatkematangan')->get()->result();
            $pengaman = $this->db->from('ref_sistempengaman')->get()->result();
            $teknis = $this->db->from('ref_tenagateknis')->get()->result();
            $app = $this->Dt_aplikasi->get_detail_app($id);

            $data = [
                'title'     => 'Detail Aplikasi',
                'navbar'    => true,
                'header'    => true,
                'padding'   => false,
                'opds'      => $opd,
                'lokasis'   => $lokasi,
                'sertfs'    => $sertf,
                'sasarans'  => $sasaran,
                'dasarhukums' => $dasarhukum,
                'kategoriaksess' => $kategoriakses,
                'publiks'   => $publik,
                'frekuensis' => $frekuensi,
                'evaluasis' => $evaluasi,
                'dokumens'  => $dokumen,
                'pjs'       => $pj,
                'pjtekniss' => $pjteknis,
                'jenisaplikasis' => $jenisaplikasi,
                'kematangans' => $kematangan,
                'pengamans' => $pengaman,
                'tekniss'   => $teknis,
                'apps' => $app
            ];

            if (validation_errors()) {
                $this->session->set_flashdata('pelanggarBaruError', TRUE);
            }
            $this->template->admin('V_aplikasi', $data);
        } else {
            redirect('Aplikasi');
        }
    }

    public function edit_aplikasi()
    {

        $aplikasi_id = $this->input->post('id_h');

        $update = [
            'no_asset' => $this->input->post('edit_no'),
            'hostname' => $this->input->post('edit_hostname'),
            'ipaddress'        => $this->input->post('edit_ip'),
            'ref_opd_id'        => $this->input->post('edit_opd'),
            'ref_lokasi_id'        => $this->input->post('edit_lokasi'),
            'nm_internal'        => $this->input->post('edit_nama'),
            'nm_eksternal'        => $this->input->post('edit_nama_ex'),
            'deskripsi'       => $this->input->post('edit_deskripsi')
        ];

        $this->db->where(['id' => $aplikasi_id])
            ->update('aplikasi', $update);

        redirect('Aplikasi/input');
    }

    public function add_aplikasi()
    {
        $data = [
            'no_asset'  => $this->input->post('add_no'),
            'hostname'      => $this->input->post('add_hostname'),
            'ipaddress'     => $this->input->post('add_ip'),
            'nm_internal'   => $this->input->post('add_nama'),
            'nm_eksternal'  => $this->input->post('add_nama_ex'),
            'ref_opd_id'    => $this->input->post('add_opd'),
            'ref_lokasi_id' => $this->input->post('add_lokasi'),
            'deskripsi'     => $this->input->post('add_deskripsi'),
            '_active'       => 1,
        ];
        $this->db->insert('aplikasi', $data);

        redirect('Aplikasi/input');
    }

    public function editDetailApp()
    {
        $aplikasi_id = $this->input->post('id_h');
        $update = [
            // 'no_asset' => $this->input->post('no_asset'),
            // 'hostname' => $this->input->post('hostname'),
            // 'ipaddress' => $this->input->post('ipaddress'),
            // 'nm_internal' => $this->input->post('nm_internal'),
            'nm_eksternal' => $this->input->post('nm_eksternal'),
            'ref_lokasi_id' => $this->input->post('ref_lokasi_id'),
            'ref_opd_id' => $this->input->post('ref_opd_id'),
            'ref_lokasi_id' => $this->input->post('ref_lokasi_id'),
            'deskripsi' => $this->input->post('deskripsi'),
            'sertf_kelayakan_id' => $this->input->post('sertf_kelayakan_id'),
            'ket_sertf' => $this->input->post('ket_sertf'),
            'prkt_utama' => $this->input->post('prkt_utama'),
            'prkt_khusus' => $this->input->post('prkt_khusus'),
            'os' => $this->input->post('os'),
            'web_server' => $this->input->post('web_server'),
            'basis_data' => $this->input->post('basis_data'),
            'bhs_pemrograman' => $this->input->post('bhs_pemrograman'),
            'framework' => $this->input->post('framework'),
            'ref_sasaran_id' => $this->input->post('ref_sasaran_id'),
            'ref_dasarhukum_id' => $this->input->post('ref_dasarhukum_id'),
            'ref_kategoriakses_id' => $this->input->post('ref_kategoriakses_id'),
            'layanan_publik' => $this->input->post('layanan_publik'),
            'ref_frekuensipemeliharaan_id' => $this->input->post('ref_frekuensipemeliharaan_id'),
            'ref_evaluasi_id' => $this->input->post('ref_evaluasi_id'),
            'ref_dokumen_id' => $this->input->post('ref_dokumen_id'),
            'ref_pj_id' => $this->input->post('ref_pj_id'),
            'ref_pjteknis_id' => $this->input->post('ref_pjteknis_id'),
            'ref_jenisaplikasi_id' => $this->input->post('ref_jenisaplikasi_id'),
            'ref_tingkatkematangan_id' => $this->input->post('ref_tingkatkematangan_id'),
            'ref_sistempengamanan_id' => $this->input->post('ref_sistempengamanan_id'),
            'ref_tenagateknis_id' => $this->input->post('ref_tenagateknis_id'),
            'tgl_ba' => $this->input->post('tgl_ba'),
            'tempat_ba' => $this->input->post('tempat_ba'),
            'maintenance_ba' => $this->input->post('maintenance_ba'),
            'pengembangan_ba' => $this->input->post('pengembangan_ba'),
            'server_ba' => $this->input->post('server_ba'),
            'ssl_ba' => $this->input->post('ssl_ba'),
            'backup_ba' => $this->input->post('backup_ba'),
            'restore_ba' => $this->input->post('restore_ba')
        ];
        $this->db->where(['id' => $aplikasi_id])->update('aplikasi', $update);
        redirect('Aplikasi/detail/' . $aplikasi_id);
    }

    public function deactive()
    {
        $aplikasi_id = $this->uri->segment(3);

        $update = [
            '_active' => 0
        ];

        $this->db->where(['id' => $aplikasi_id])->update('aplikasi', $update);
        redirect('Aplikasi');
    }

    public function chunking_upload(){
        function verbose($ok=1,$info=""){
            // THROW A 400 ERROR ON FAILURE
            if ($ok==0) { http_response_code(400); }
            die(json_encode(["ok"=>$ok, "info"=>$info]));
          }
          
          // (B) INVALID UPLOAD
          if (empty($_FILES) || $_FILES['file']['error']) {
            verbose(0, "Failed to move uploaded file.");
          }
          
          // (C) UPLOAD DESTINATION
          // ! CHANGE FOLDER IF REQUIRED !
          $filePath = "./upload";
          if (!file_exists($filePath)) { 
            if (!mkdir($filePath, 0777, true)) {
              verbose(0, "Failed to create $filePath");
            }
          }
          $fileName = isset($_REQUEST["name"]) ? $_REQUEST["name"] : $_FILES["file"]["name"];
          $filePath = $filePath . DIRECTORY_SEPARATOR . $fileName;
          
          // (D) DEAL WITH CHUNKS
          $chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
          $chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;
          $out = @fopen("{$filePath}.part", $chunk == 0 ? "wb" : "ab");
          if ($out) {
            $in = @fopen($_FILES['file']['tmp_name'], "rb");
            if ($in) {
              while ($buff = fread($in, 4096)) { fwrite($out, $buff); }
            } else {
              verbose(0, "Failed to open input stream");
            }
            @fclose($in);
            @fclose($out);
            @unlink($_FILES['file']['tmp_name']);
          } else {
            verbose(0, "Failed to open output stream");
          }
          
          // (E) CHECK IF FILE HAS BEEN UPLOADED
          if (!$chunks || $chunk == $chunks - 1) {
            rename("{$filePath}.part", $filePath);
          }
          verbose(1, "Upload OK");
        //   $data = [
        //     'aplikasi_id'   => $this->input->post('versi_id'),
        //     'no_versi'      => $this->input->post('add_versi_no'),
        //     'deskripsi'     => $this->input->post('add_versi_deskripsi'),
        //     'tanggal'       => $this->input->post('add_versi_tgl'),
        //     'path_file'    => $this->input->post('add_versi_file'),
        //     '_active'       => 1
        // ];
        // echo "Update ke database";
        // die();
    }

    public function add_versi_aplikasi()
    {
        // $config['upload_path']          = './upload/';
        // $config['allowed_types']        = 'rar|zip';
        // $config['max_size']             = 102400;

        // $this->load->library('upload', $config);

        // if($this->input->post('add_versi_file')){
            // if (!$this->upload->do_upload('add_versi_file')) {
            //     $error = array('error' => $this->upload->display_errors());
            //     // $this->load->view('v_upload', $error);
            //     echo "Gagal Upload";
            //     die();
            // } else {
            //     $data = array('upload_data' => $this->upload->data());
            //     // $this->load->view('v_upload_sukses', $data);
            //     echo "Sukses Upload";
            //     die();
            // }
        //     echo "masuk pos";
        //     die();
        // }

        // if ($this->input->post('edit_fotoResized')) {
        //     $base64img  = $this->input->post('edit_fotoResized');
        //     if (preg_match('/^data:image\/(\w+);base64,/', $base64img, $type)) {
        //         $base64img = substr($base64img, strpos($base64img, ',') + 1);
        //         $type = strtolower($type[1]); // jpg, png, gif

        //         if (!in_array($type, ['jpg', 'jpeg', 'gif', 'png'])) {
        //             throw new \Exception('invalid image type');
        //         }
        //         $base64img = str_replace(' ', '+', $base64img);
        //         $base64img = base64_decode($base64img);

        //         if ($base64img === false) {
        //             throw new \Exception('base64_decode failed');
        //         }
        //     } else {
        //         throw new \Exception('did not match data URI with image data');
        //     }

        //     $img_name   = md5(uniqid(rand(), true)) . '.' . $type;
        //     file_put_contents(realpath('') . './upload/' . $img_name, $base64img);
        //     $data['foto'] = $img_name;
        // }

        $data = [
            'aplikasi_id'   => $this->input->post('versi_id'),
            'no_versi'      => $this->input->post('add_versi_no'),
            'deskripsi'     => $this->input->post('add_versi_deskripsi'),
            'tanggal'       => $this->input->post('add_versi_tgl'),
            'path_file'    => $this->input->post('add_versi_file'),
            '_active'       => 1
        ];
        echo "Update ke database";
        die();
        // $this->db->insert('versi', $data);

        redirect('Aplikasi/input');
    }

    public function add_berita_acara(){
        $data = [
            'aplikasi_id'   => $this->input->post('versi_id'),
            'no_versi'      => $this->input->post('add_versi_no'),
            'deskripsi'     => $this->input->post('add_versi_deskripsi'),
            'tanggal'       => $this->input->post('add_versi_tgl'),
            'path_file'    => $this->input->post('add_versi_file'),
            '_active'       => 1
        ];
        echo "Update ke database";
        die();
        // $this->db->insert('versi', $data);

        redirect('Aplikasi/input');
    }

    public function tanggal_indo($tanggal, $cetak_hari = false)
    {
        $hari = array(
            1 =>    'Senin',
            'Selasa',
            'Rabu',
            'Kamis',
            'Jumat',
            'Sabtu',
            'Minggu'
        );

        $bulan = array(
            1 =>   'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );
        $split       = explode('-', $tanggal);
        $tgl_indo = $split[2] . ' ' . $bulan[(int)$split[1]] . ' ' . $split[0];

        if ($cetak_hari) {
            $num = date('N', strtotime($tanggal));
            return $hari[$num] . ', ' . $tgl_indo;
        }
        return $tgl_indo;
    }

    public function print_ba($id)
    {
        $this->load->library('Pdfgenerator');
        $file_pdf = 'Berita Acara';
        $paper = 'F4';
        $orientation = 'portrait';
        $app = $this->Dt_aplikasi->get_ba($id);
        $tanggal = $this->Dt_aplikasi->get_tanggal_ba($id);
        $tgl_hari = $this->tanggal_indo($tanggal->tgl_ba, true);
        $tgl_id = $this->tanggal_indo($tanggal->tgl_ba);
        $data = [
            'apps' => $app,
            'tgl_haris' => $tgl_hari,
            'tgl_ids' => $tgl_id
        ];
        // $this->load->view('pdf.php', $data);
        $html = $this->load->view('pdf.php', $data, TRUE);
        $this->pdfgenerator->generate($html, $file_pdf, $paper, $orientation);
    }
}
