<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Dt_Neighbour extends CI_Model
{

    // start datatables
    var $column_order = array(null, 'o.nama', 'k.lokasi', 'kel.nama_kel', 'k.alamat', 'k.rw', 'k.rt', 'k.uraian'); //set column field database for datatable orderable
    var $column_search = array('o.nama', 'k.lokasi', 'k.alamat', 'kel.nama_kel', 'k.uraian'); //set column field database for datatable searchable
    var $order = array('id' => 'asc'); // default order 

    function __construct()
    {
        parent::__construct();
    }

    function get_datatables($id, $type)
    {
        $this->_get_datatables_query($id, $type);
        if (@$_POST['length'] != -1)
            $this->db->limit(@$_POST['length'], @$_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    private function _get_datatables_query($id, $type)
    {
        $kegiatan =  $this->db->from('kegiatan')
            ->where(['id' => $id, '_active' => 1])
            ->get()
            ->row();

        $in = array_intersect(explode(',', $kegiatan->{'neighbour_' . $type}), explode(',', $kegiatan->neighbour_uraian));
        // echo json_encode(array_values($in));
        // die();
        $this->db->select('o.nama, kel.nama_kel, k.*')
            ->from('kegiatan k')
            ->join('opd o', 'o.id = k.opd_id', 'left')
            ->join('ref_kelurahan kel', 'kel.id = k.kelurahan_id', 'left')
            ->where(['k._active' => 1]);

        if ($in) {
            $this->db->where_in('k.id', $in);
            $this->db->or_where(['k.id' => $kegiatan->id]);
        } else {
            $this->db->where(['k.id' => $kegiatan->id]);
        }

        // ->where('k.{'neighbour_' . $type} is not null')
        // ->where('k.{'neighbour_' . $type} !=', '');

        $i = 0;

        if (@$_POST['search']['value']) { // if datatable send POST for search
            foreach ($this->column_search as $item) { // loop column 
                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
                $i++;
            }
        }

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function count_filtered($id, $type)
    {
        $this->_get_datatables_query($id, $type);
        $query = $this->db->get();
        return $query->num_rows();
    }

    function count_all($id, $type)
    {
        $kegiatan =  $this->db->from('kegiatan')
            ->where(['id' => $id, '_active' => 1])
            ->get()
            ->row();

        $in = array_intersect(explode(',', $kegiatan->{'neighbour_' . $type}), explode(',', $kegiatan->neighbour_uraian));

        $this->db->from('kegiatan k');
        if ($in) {
            $this->db->where_in('k.id', $in);
            $this->db->or_where(['k.id' => $kegiatan->id]);
        } else {
            $this->db->where(['k.id' => $kegiatan->id]);
        }

        return $this->db->count_all_results();
    }
}
