<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Dt_aplikasi extends CI_Model
{

    // start datatables
    var $column_order = array(null, 'kec.nama_kec', 'kel.nama_kel', 'p.riwayat_perjalanan', 'p.konfirmasi', 'p.meninggal', 'p.sembuh'); //set column field database for datatable orderable
    var $column_search = array('k.nama_kec', 'kel.nama_kel', 'p.riwayat_perjalanan', 'p.konfirmasi', 'p.meninggal', 'p.sembuh'); //set column field database for datatable searchable
    var $order = array('id' => 'desc'); // default order 

    function __construct()
    {
        parent::__construct();
    }
 
    function get_datatables()
    {
        $this->_get_datatables_query();
        if (@$_POST['length'] != -1)
            $this->db->limit(@$_POST['length'], @$_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    private function _get_datatables_query()
    {
        $this->db->select('a.*, opd.nama as nama_opd')
            ->from('aplikasi a')
            ->join('ref_opd opd', 'opd.id = a.ref_opd_id', 'left');
        $this->db->where(['a._active' => 1]);
        $this->db->order_by('id','asc');
        $i = 0;
    }


    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    function count_all()
    {
        $this->db->from('aplikasi a')->where(['a._active'=>1]);
        return $this->db->count_all_results();
    }

    function get_detail_app($id){
        return $this->db->select('*')->from('aplikasi a')->where(['_active'=>1, 'id'=> $id])->get()->row();
        // return json_encode($app);
    }

    public function get_ba($id){
        return $this->db
        ->select('a.hostname, a.tempat_ba, opd.nama as nama_opd, maintenance.nama as nama_maintenance, pengembangan.nama as nama_pengembangan, server.nama as nama_server, ssl.nama as nama_ssl, backup.nama as nama_backup, restore.nama as nama_restore')
        ->from('aplikasi a')
        ->join('ref_opd opd','opd.id=a.ref_opd_id','left')
        ->join('ref_opd maintenance','maintenance.id=a.maintenance_ba','left')
        ->join('ref_opd pengembangan','pengembangan.id=a.pengembangan_ba','left')
        ->join('ref_opd server','server.id=a.server_ba','left')
        ->join('ref_opd ssl','ssl.id=a.ssl_Ba','left')
        ->join('ref_opd backup','backup.id=a.backup_ba','left')
        ->join('ref_opd restore','restore.id=a.restore_ba','left')
        ->where(['a.id' => $id,'a._active' => 1])->get()->row();

    }

    public function get_tanggal_ba($id){
        return $this->db->select('a.tgl_ba')->from('aplikasi a')->where(['a.id' => $id,'a._active' => 1])->get()->row();
    }

    function get_datatables_versi($id)
    {
        $this->_get_datatables_versi_query($id);
        if (@$_POST['length'] != -1)
            $this->db->limit(@$_POST['length'], @$_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    private function _get_datatables_versi_query($id)
    {
        $this->db->select('v.*')
            ->from('versi v');
            // ->join('ref_opd opd', 'opd.id = a.ref_opd_id', 'left');
        $this->db->where(['v._active' => 1, 'aplikasi_id' => $id]);
        $this->db->order_by('id','asc');
        $i = 0;
    }

    
    function count_filtered_versi($id)
    {
        $this->_get_datatables_versi_query($id);
        $query = $this->db->get();
        return $query->num_rows();
    }

    function count_all_versi($id)
    {
        $this->db->from('versi v')->where(['v._active'=>1, 'v.aplikasi_id'=> $id]);
        return $this->db->count_all_results();
    }

}
