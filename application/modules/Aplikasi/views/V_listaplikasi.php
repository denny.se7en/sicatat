<div class="card-body">
    <div class=" pb-3">
        <div style="float: right;" class="text-right">
            <button type="button" class="btn btn-block btn-primary pull-right" onclick="addAplikasi()">Tambah Data<span class="badge badge-light"></button>
        </div>
    </div>
    <table id="listaplikasi" class="table table-bordered table-striped" style="width: 100%;">
        <thead>
            <tr>
                <th>No.</th>
                <th>Nomor Asset</th>
                <th>Hostname</th>
                <th>IP Address</th>
                <th>Nama Internal</th>
                <th>OPD</th>
                <th>Action</th>
            </tr>
        </thead>
    </table>
</div>
<!-- MODAl Tambah Data Aplikasi-->
<div class="modal fade" id="modalAdd">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Data Aplikasi</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="formAdd" action="<?= base_url('Aplikasi/add_aplikasi') ?>" method="POST" enctype="multipart/form-data">
                <div class="modal-body" id="bodyModalAdd">
                    <div class="p-2">
                        <div class="form-group row">
                            <label for="add_no" class="col-sm-2 col-form-label">Nomor Asset</label>
                            <div class="col-sm-10">
                                <input type="text" id="add_no" name="add_no" placeholder="Kode Asset" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="add_hostname" class="col-sm-2 col-form-label">Hostname</label>
                            <div class="col-sm-10">
                                <input type="text" id="add_hostname" name="add_hostname" placeholder="Alamat URL Website" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="add_ip" class="col-sm-2 col-form-label">IP Address</label>
                            <div class="col-sm-10">
                                <input type="text" id="add_ip" name="add_ip" placeholder="Alamat IP Address" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="add_nama" class="col-sm-2 col-form-label">Nama Internal</label>
                            <div class="col-sm-10">
                                <input type="text" id="add_nama" name="add_nama" placeholder="Nama Aplikasi" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="add_nama" class="col-sm-2 col-form-label">Nama Eksternal</label>
                            <div class="col-sm-10">
                                <input type="text" id="add_nama_ex" name="add_nama_ex" placeholder="Nama Aplikasi" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="add_lokasi" class="col-sm-2 col-form-label">Lokasi</label>
                            <div class="col-sm-10">
                                <select class="form-control" name="add_lokasi" id="add_lokasi">
                                    <?php
                                    foreach ($lokasis as $lokasi) {
                                    ?>
                                        <option value="<?= $lokasi->id ?>"><?= $lokasi->nama ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="add_opd" class="col-sm-2 col-form-label">OPD</label>
                            <div class="col-sm-10">
                                <select class="form-control" name="add_opd" id="add_opd">
                                    <?php
                                    foreach ($opds as $opd) {
                                    ?>
                                        <option value="<?= $opd->id ?>"><?= $opd->nama ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="add_nama" class="col-sm-2 col-form-label">Deskripsi</label>
                            <div class="col-sm-10">
                                <textarea type="text" id="add_deskripsi" name="add_deskripsi" placeholder="Deskripsi Aplikasi" class="form-control"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" onclick="tambahAplikasi()" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!-- MODAl Edit Data Aplikasi-->
<div class="modal fade" id="modalEdit">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Edit Data Aplikasi</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="formEdit" action="<?= base_url('Aplikasi/edit_aplikasi') ?>" method="POST" enctype="multipart/form-data">
                <div class="modal-body" id="bodyModalEdit">
                    <div class="p-2">
                        <input id="id_h" name="id_h" type="hidden" readonly class="form-control-plaintext">
                        <div class="form-group row">
                            <label for="edit_no" class="col-sm-2 col-form-label">Nomor Asset</label>
                            <div class="col-sm-10">
                                <input type="text" id="edit_no" name="edit_no" placeholder="Kode Asset" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="edit_hostname" class="col-sm-2 col-form-label">Hostname</label>
                            <div class="col-sm-10">
                                <input type="text" id="edit_hostname" name="edit_hostname" placeholder="Alamat URL Website" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="edit_ip" class="col-sm-2 col-form-label">IP Address</label>
                            <div class="col-sm-10">
                                <input type="text" id="edit_ip" name="edit_ip" placeholder="Alamat IP Address" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="edit_nama" class="col-sm-2 col-form-label">Nama Internal</label>
                            <div class="col-sm-10">
                                <input type="text" id="edit_nama" name="edit_nama" placeholder="Nama Aplikasi" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="edit_nama_ex" class="col-sm-2 col-form-label">Nama Eksternal</label>
                            <div class="col-sm-10">
                                <input type="text" id="edit_nama_ex" name="edit_nama_ex" placeholder="Nama Aplikasi" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="edit_lokasi" class="col-sm-2 col-form-label">Lokasi</label>
                            <div class="col-sm-10">
                                <select class="form-control" name="edit_lokasi" id="edit_lokasi">
                                    <?php
                                    foreach ($lokasis as $lokasi) {
                                    ?>
                                        <option value="<?= $lokasi->id ?>"><?= $lokasi->nama ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="edit_opd" class="col-sm-2 col-form-label">OPD</label>
                            <div class="col-sm-10">
                                <select class="form-control" name="edit_opd" id="edit_opd">
                                    <?php
                                    foreach ($opds as $opd) {
                                    ?>
                                        <option value="<?= $opd->id ?>"><?= $opd->nama ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="edit_nama" class="col-sm-2 col-form-label">Deskripsi</label>
                            <div class="col-sm-10">
                                <textarea type="text" id="edit_deskripsi" name="edit_deskripsi" placeholder="Deskripsi Aplikasi" class="form-control"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" onclick="saveAplikasi()" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<script>
    let id = undefined

    $(function() {
        get_listaplikasi()
    })

    function get_listaplikasi() {
        $('#listaplikasi').DataTable({
            "destroy": true,
            "processing": true,
            "serverSide": true,
            "autowidth": true,
            "lengthMenu": [
                [10, 25, 50, -1],
                [10, 25, 50, "All"]
            ],
            dom: 'Bfrtip',
            buttons: ['pageLength',
                {
                    extend: 'excelHtml5',
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4]
                    }
                },
            ],
            "ajax": {
                'url': '<?= base_url('Aplikasi/api/get_ajax') ?>',
                'type': 'POST',
                'dataType': 'JSON',
                'data': function(d) {
                    return $.extend({}, d, {
                        "selected_opd": $('#list_opd').val()
                    });
                },
            },

            "columns": [{
                    "data": "no",
                    "defaultContent": "<i>Not set</i>"
                },
                {
                    "data": "no_asset",
                    "defaultContent": "<i>Not set</i>"
                },
                {
                    "data": "hostname",
                    "defaultContent": "<i>Not set</i>"
                },
                {
                    "data": "ipaddress",
                    "defaultContent": "<i>Not set</i>"
                },
                {
                    "data": "nm_internal",
                    "defaultContent": "<i>Not set</i>"
                },
                {
                    "data": "nama_opd",
                    "defaultContent": "<i>Not set</i>"
                },
                {
                    "data": null,
                    "render": function(data, type, row) {
                        return '<button type="button" class="btn btn-sm btn-block btn-info" onclick="detailApp('+data.id+')">Detail<span class="badge badge-light"></button>\
                                <button type="button" class="btn btn-sm btn-block btn-danger" onclick="deactiveAplikasi(' + data.id + ')">Hapus <span class="badge badge-light"></button>';
                    }
                },
            ],


            "order": [
                ['0', 'desc']
            ],

            "columnDefs": [{
                    "orderable": false,
                    "targets": [0, 1, 2, 3, 4]
                },
                {
                    "width": "20%",
                    "targets": -1
                },
                {
                    className: 'dt-body-center',
                    "targets": 4
                }
            ],
        });
    }

    function addAplikasi() {
        $('#modalAdd').modal('show')
    }

    function tambahAplikasi() {
        Swal.fire({
            title: 'Apakah Anda Yakin Memasukkan data?',
            text: "Silahkan periksa kembali data aplikasi, apakah data yang anda masukkan sudah benar?!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya',
            cancelButtonText: 'Batal'
        }).then((result) => {
            if (result.value) {
                $('#formAdd').submit()
            }
        })
    }

    function editAplikasi(id) {
        $.ajax({
            type: 'GET',
            url: '<?= base_url('Aplikasi/Api/get_aplikasi/') ?>' + id,
            dataType: 'JSON',
            success: function(response) {
                console.log(response)
                $("#id_h").val(response.id)
                $("#edit_no").val(response.no_asset)
                $("#edit_hostname").val(response.hostname)
                $("#edit_ip").val(response.ipaddress)
                $("#edit_opd").val(response.ref_opd_id)
                $("#edit_lokasi").val(response.ref_lokasi_id)
                $("#edit_nama").val(response.nm_internal)
                $("#edit_nama_ex").val(response.nm_eksternal)
                $("#edit_deskripsi").val(response.deskripsi)
                $('#modalEdit').modal('show')
            }
        })
    }

    function saveAplikasi() {
        Swal.fire({
            title: 'Apakah Anda Yakin Memasukkan data?',
            text: "Silahkan periksa kembali data aplikasi, apakah data yang anda masukkan sudah benar?!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya',
            cancelButtonText: 'Batal'
        }).then((result) => {
            if (result.value) {
                $('#formEdit').submit()
            }
        })
    }

    function deactiveAplikasi(id) {
        Swal.fire({
            title: 'Apakah Anda Yakin Akan Menghapus Data Aplikasi Ini?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya',
            cancelButtonText: 'Batal'
        }).then((result) => {
            if (result.value) {
                window.location.href = "<?= base_url('Aplikasi/deactive/') ?>" + id;
            }
        })
    }

    function detailApp(id){
        window.location.href = "<?= base_url('Aplikasi/detail/') ?>" + id;
    }
</script>