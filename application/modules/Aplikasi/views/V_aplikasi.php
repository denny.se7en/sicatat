<div class="card-body">
    <div class="app-main__outer">
        <div class="app-main__inner">
            <div class="col-12 d-flex flex-wrap px-3">
                <div class="col-md-12">
                    <!-- <form id="formInfoAplikasi" action="<?= base_url('Aplikasi/editInfoApp/') ?>" method="POST" enctype="multipart/form-data"> -->
                    <div class="main-card card">
                        <div class="card-body">
                            <input id="id_h" name="id_h" type="hidden" readonly class="form-control-plaintext" value="<?= $apps->id ?>">
                            <div class="form-group row">
                                <label for="no_asset" class="col-sm-2 col-form-label">Nomor Asset</label>
                                <div class="col-sm-10">
                                    <input type="text" id="no_asset" name="no_asset" placeholder="Kode Asset" class="form-control" value="<?= $apps->no_asset ?>" readonly>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="hostname" class="col-sm-2 col-form-label">Hostname</label>
                                <div class="col-sm-10">
                                    <input type="text" id="hostname" name="hostname" placeholder="Alamat URL Website" class="form-control" value="<?= $apps->hostname ?>" readonly>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="ipaddress" class="col-sm-2 col-form-label">IP Address</label>
                                <div class="col-sm-10">
                                    <input type="text" id="ipaddress" name="ipaddress" placeholder="Alamat IP Address" class="form-control" value="<?= $apps->ipaddress ?>" readonly>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="nm_internal" class="col-sm-2 col-form-label">Nama Internal</label>
                                <div class="col-sm-10">
                                    <input type="text" id="nm_internal" name="nm_internal" placeholder="Nama Internal Aplikasi" class="form-control" value="<?= $apps->nm_internal ?>" readonly>
                                </div>
                            </div>
                            <button type="button" onclick="editInfoApp(<?= $apps->id ?>)" class="mb-2 ml-3 btn btn-primary text-white mt-3 shadow">Edit Nama Aplikasi</button>
                        </div>
                    </div>
                    <!-- </form> -->
                </div>
            </div>
            <form id="formAplikasi" action="<?= base_url('Aplikasi/editDetailApp/') ?>" method="POST" enctype="multipart/form-data">
                <div class="mb-1 card">
                    <div class="card-header">
                        <ul class="nav nav-pills nav-fill">
                            <li class="nav-item"><a data-toggle="tab" href="#tab-eg7-1" class="nav-link show active">1<br>Deskripsi Sistem</a></li>
                            <li class="nav-item"><a data-toggle="tab" href="#tab-eg7-2" class="nav-link show">2<br>Perangkat Keras</a></li>
                            <li class="nav-item"><a data-toggle="tab" href="#tab-eg7-3" class="nav-link show">3<br>Kebijakan</a></li>
                            <li class="nav-item"><a data-toggle="tab" href="#tab-eg7-4" class="nav-link show">4<br>Tata Kelola</a></li>
                            <li class="nav-item"><a data-toggle="tab" href="#tab-eg7-5" class="nav-link show">5<br>Teknologi</a></li>
                            <li class="nav-item"><a data-toggle="tab" href="#tab-eg7-6" class="nav-link show">6<br>Berita Acara</a></li>
                            <li class="nav-item"><a data-toggle="tab" href="#tab-eg7-0" class="nav-link show">7<br>Upload File</a></li>
                        </ul>
                    </div>
                    <div class="card-body" style="margin-left: 10%;margin-right: 5%;">
                        <div class="tab-content">
                            <div class="tab-pane show active" id="tab-eg7-1" role="tabpanel">
                                <!-- ######### Deskripsi Sistem ########### -->
                                <div class="col-md-10">
                                    <div class="main-card card">
                                        <div class="card-body">
                                            <input id="id_h" name="id_h" type="hidden" readonly class="form-control-plaintext" value="<?= $apps->id ?>">
                                            <div class="form-group row">
                                                <label for="nm_eksternal" class="col-sm-2 col-form-label">Nama Eksternal</label>
                                                <div class="col-sm-10">
                                                    <input type="text" id="nm_eksternal" name="nm_eksternal" placeholder="Nama Eksternal Aplikasi" class="form-control" value="<?= $apps->nm_eksternal ?>">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="ref_lokasi_id" class="col-sm-2 col-form-label">Lokasi</label>
                                                <div class="col-sm-10">
                                                    <select class="form-control" name="ref_lokasi_id" id="ref_lokasi_id" value="<?= $apps->ref_lokasi_id ?>">
                                                        <option disabled selected value> -- Pilih Lokasi Server -- </option>
                                                        <?php
                                                        foreach ($lokasis as $lokasi) {
                                                            echo '<option' . ($lokasi->id == $apps->ref_lokasi_id ? ' selected="selected" ' : ' ') .  'value="' . $lokasi->id . '">' . $lokasi->nama . '</option>';
                                                        } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="ref_opd_id" class="col-sm-2 col-form-label">OPD</label>
                                                <div class="col-sm-10">
                                                    <select class="form-control select2" name="ref_opd_id" id="ref_opd_id" style="width: 100%;">
                                                        <option disabled selected value> -- Pilih OPD -- </option>
                                                        <?php
                                                        foreach ($opds as $opd) {
                                                            echo '<option' . ($opd->id == $apps->ref_opd_id ? ' selected="selected" ' : ' ') . 'value="' . $opd->id . '">' . $opd->nama . '</option>';
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="deskripsi" class="col-sm-2 col-form-label">Deskripsi</label>
                                                <div class="col-sm-10">
                                                    <textarea type="text" id="deskripsi" name="deskripsi" placeholder="Deskripsi Aplikasi" class="form-control"><?= $apps->deskripsi ?></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="sertf_kelayakan_id" class="col-sm-2 col-form-label">Sertifikat Kelayakan</label>
                                                <div class="col-sm-10">
                                                    <select class="form-control" name="sertf_kelayakan_id" id="sertf_kelayakan_id">
                                                        <option disabled selected value> -- Pilih Sertifikat -- </option>
                                                        <?php
                                                        foreach ($sertfs as $sertf) {
                                                            echo '<option' . ($sertf->id == $apps->ref_sertf_id ? ' selected="selected" ' : ' ') . 'value="' . $sertf->id . '">' . $sertf->nama . '</option>';
                                                        } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="ket_sertf" class="col-sm-2 col-form-label">Keterangan Sertifikat</label>
                                                <div class="col-sm-10">
                                                    <input type="text" id="ket_sertf" name="ket_sertf" placeholder="Keterangan Sertifikat" class="form-control" value="<?= $apps->ket_sertf ?>">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="ref_opd_id1" class="col-sm-2 col-form-label">Profile Penyelenggara</label>
                                                <div class="col-sm-10">
                                                    <input type="text" id="ref_opd_id1" name="ref_opd_id1" class="form-control" readonly>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- ########################### -->
                            </div>
                            <div class="tab-pane show" id="tab-eg7-2" role="tabpanel">
                                <!-- ########## Perangkat Keras ######### -->
                                <div class="col-md-10">
                                    <div class="main-card card">
                                        <div class="card-body">
                                            <div class="form-group row">
                                                <label for="prkt_utama" class="col-sm-2 col-form-label">Perangkat Utama</label>
                                                <div class="col-sm-10">
                                                    <input type="text" id="prkt_utama" name="prkt_utama" placeholder="Perangkat Utama" class="form-control" value="<?= $apps->prkt_utama ?>">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="info_data_center" class="col-sm-2 col-form-label">Informasi Data Center</label>
                                                <div class="col-sm-10">
                                                    <input type="text" id="info_data_center" name="info_data_center" placeholder="Informasi Data Center" class="form-control" value="<?= $apps->info_data_center ?>">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="prkt_khusus" class="col-sm-2 col-form-label">Perangkat Khusus</label>
                                                <div class="col-sm-10">
                                                    <input type="text" id="prkt_khusus" name="prkt_khusus" placeholder="Perangkat Khusus yang diperlukan" class="form-control" value="<?= $apps->prkt_khusus ?>">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="os" class="col-sm-2 col-form-label">Sistem Operasi</label>
                                                <div class="col-sm-10">
                                                    <input type="text" id="os" name="os" placeholder="Sistem Operasi" class="form-control" value="<?= $apps->os ?>">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="web_server" class="col-sm-2 col-form-label">Web server</label>
                                                <div class="col-sm-10">
                                                    <input type="text" id="web_server" name="web_server" placeholder="Web Server" class="form-control" value="<?= $apps->web_server ?>">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="basis_data" class="col-sm-2 col-form-label">Basis Data</label>
                                                <div class="col-sm-10">
                                                    <input type="text" id="basis_data" name="basis_data" placeholder="Basis Data" class="form-control" value="<?= $apps->basis_data ?>">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="bhs_pemrograman" class="col-sm-2 col-form-label">Bahasa Pemrograman</label>
                                                <div class="col-sm-10">
                                                    <input type="text" id="bhs_pemrograman" name="bhs_pemrograman" placeholder="Bahasa Pemrograman" class="form-control" value="<?= $apps->bhs_pemrograman ?>">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="framework" class="col-sm-2 col-form-label">Framework</label>
                                                <div class="col-sm-10">
                                                    <input type="text" id="framework" name="framework" placeholder="Framework" class="form-control" value="<?= $apps->framework ?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- ########################### -->
                            </div>
                            <div class="tab-pane show " id="tab-eg7-3" role="tabpanel">
                                <!-- ####### Kebijakan ########## -->
                                <div class="col-md-10">
                                    <div class="main-card card">
                                        <div class="card-body">
                                            <div class="form-group row">
                                                <label for="ref_sasaran_id" class="col-sm-2 col-form-label">Sasaran Pelayanan</label>
                                                <div class="col-sm-10">
                                                    <select class="form-control" name="ref_sasaran_id" id="ref_sasaran_id">
                                                        <option disabled selected value> -- Pilih Sasaran Pelayanan -- </option>
                                                        <?php
                                                        foreach ($sasarans as $sasaran) {
                                                            echo '<option' . ($sasaran->id == $apps->ref_sasaran_id ? ' selected="selected" ' : ' ') . ' value="' . $sasaran->id . '">' . $sasaran->nama . '</option>';
                                                        } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="ref_dasarhukum_id" class="col-sm-2 col-form-label">Dasar Hukum</label>
                                                <div class="col-sm-10">
                                                    <select class="form-control" name="ref_dasarhukum_id" id="ref_dasarhukum_id">
                                                        <option disabled selected value> -- Pilih Dasar Hukum -- </option>
                                                        <?php
                                                        foreach ($dasarhukums as $dasarhukum) {
                                                            echo '<option' . ($dasarhukum->id == $apps->ref_dasarhukum_id ? ' selected="selected" ' : ' ') . 'value="' . $dasarhukum->id . '">' . $dasarhukum->nama . '</option>';
                                                        } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="ref_kategoriakses_id" class="col-sm-2 col-form-label">Kategori Akses</label>
                                                <div class="col-sm-10">
                                                    <select class="form-control" name="ref_kategoriakses_id" id="ref_kategoriakses_id">
                                                        <option disabled selected value> -- Pilih Kategori Akses -- </option>
                                                        <?php
                                                        foreach ($kategoriaksess as $kategoriakses) {
                                                            echo '<option' . ($kategoriakses->id == $apps->ref_kategoriakses_id ? ' selected="selected" ' : ' ') . 'value="' . $kategoriakses->id . '">' . $kategoriakses->nama . '</option>';
                                                        } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="layanan_publik" class="col-sm-2 col-form-label">Kesediaan dipublikasikan melalui portal layanan publik</label>
                                                <div class="col-sm-10">
                                                    <select class="form-control" name="layanan_publik" id="layanan_publik">
                                                        <option disabled selected value> -- Pilih Kesediaan Publikasi -- </option>
                                                        <?php
                                                        foreach ($publiks as $publik) {
                                                            echo '<option' . ($publik->id == $apps->layanan_publik ? ' selected="selected" ' : ' ') . 'value="' . $publik->id . '">' . $publik->nama . '</option>';
                                                        } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="hostname1" class="col-sm-2 col-form-label">URL</label>
                                                <div class="col-sm-10">
                                                    <input type="text" id="hostname1" name="hostname1" placeholder="Alamat URL Website" class="form-control" value="<?= $apps->hostname ?>" readonly>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- ########################### -->
                            </div>
                            <div class="tab-pane show" id="tab-eg7-4" role="tabpanel">
                                <!-- ######### Tata Kelola ########### -->
                                <div class="col-md-10">
                                    <div class="main-card card">
                                        <div class="card-body">
                                            <div class="form-group row">
                                                <label for="ref_frekuensipemeliharaan_id" class="col-sm-2 col-form-label">Frekuensi Pemeliharaan</label>
                                                <div class="col-sm-10">
                                                    <select class="form-control" name="ref_frekuensipemeliharaan_id" id="ref_frekuensipemeliharaan_id">
                                                        <option disabled selected value> -- Pilih Frekuensi Pemeliharaan -- </option>
                                                        <?php
                                                        foreach ($frekuensis as $frekuensi) {
                                                            echo '<option' . ($frekuensi->id == $apps->ref_frekuensipemeliharaan_id ? ' selected="selected" ' : ' ') . 'value="' . $frekuensi->id . '">' . $frekuensi->nama . '</option>';
                                                        } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="ref_evaluasi_id" class="col-sm-2 col-form-label">Evaluasi Layanan</label>
                                                <div class="col-sm-10">
                                                    <select class="form-control" name="ref_evaluasi_id" id="ref_evaluasi_id">
                                                        <option disabled selected value> -- Pilih Evaluasi Layanan -- </option>
                                                        <?php
                                                        foreach ($evaluasis as $evaluasi) {
                                                            echo '<option' . ($evaluasi->id == $apps->ref_evaluasi_id ? ' selected="selected" ' : ' ') . 'value="' . $evaluasi->id . '">' . $evaluasi->nama . '</option>';
                                                        } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="ref_dokumen_id" class="col-sm-2 col-form-label">Adanya dokumen</label>
                                                <div class="col-sm-10">
                                                    <select class="form-control" name="ref_dokumen_id" id="ref_dokumen_id">
                                                        <option disabled selected value> -- Pilih Kelengkapan Dokumen -- </option>
                                                        <?php
                                                        foreach ($dokumens as $dokumen) {
                                                            echo '<option' . ($dokumen->id == $apps->ref_dokumen_id ? ' selected="selected" ' : ' ') . 'value="' . $dokumen->id . '">' . $dokumen->nama . '</option>';
                                                        } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <table id="dokumen_app" class="table table-bordered table-striped" style="width: 100%;">
                                                <thead>
                                                    <tr>
                                                        <th>No.</th>
                                                        <th>Jenis Dokumen</th>
                                                        <th>File Dokumen</th>
                                                        <th>Aksi</th>
                                                    </tr>
                                                </thead>
                                            </table>
                                            <div class="form-group row">
                                                <label for="ref_pj_id" class="col-sm-2 col-form-label">Penanggung Jawab</label>
                                                <div class="col-sm-10">
                                                    <select class="form-control" name="ref_pj_id" id="ref_pj_id">
                                                        <option disabled selected value> -- Pilih Penanggung Jawab -- </option>
                                                        <?php
                                                        foreach ($pjs as $pj) {
                                                            echo '<option' . ($pj->id == $apps->ref_pj_id ? ' selected="selected" ' : ' ') . 'value="' . $pj->id . '">' . $pj->nama . '</option>';
                                                        } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="ref_pjteknis_id" class="col-sm-2 col-form-label">Penanggung Jawab Teknis</label>
                                                <div class="col-sm-10">
                                                    <select class="form-control" name="ref_pjteknis_id" id="ref_pjteknis_id">
                                                        <option disabled selected value> -- Pilih Penanggung Jawab Teknis -- </option>
                                                        <?php
                                                        foreach ($pjtekniss as $pjteknis) {
                                                            echo '<option' . ($pjteknis->id == $apps->ref_pjteknis_id ? ' selected="selected" ' : ' ') . 'value="' . $pjteknis->id . '">' . $pjteknis->nama . '</option>';
                                                        } ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- ########################### -->
                            </div>
                            <div class="tab-pane show" id="tab-eg7-5" role="tabpanel">
                                <!-- ####### Teknologi ############ -->
                                <div class="col-md-10">
                                    <div class="main-card card">
                                        <div class="card-body">
                                            <div class="form-group row">
                                                <label for="ref_jenisaplikasi_id" class="col-sm-2 col-form-label">Kategori Jenis Aplikasi</label>
                                                <div class="col-sm-10">
                                                    <select class="form-control" name="ref_jenisaplikasi_id" id="ref_jenisaplikasi_id">
                                                        <option disabled selected value> -- Pilih Kategori Jenis Aplikasi -- </option>
                                                        <?php
                                                        foreach ($jenisaplikasis as $jenisaplikasi) {
                                                            echo '<option' . ($jenisaplikasi->id == $apps->ref_jenisaplikasi_id ? ' selected="selected" ' : ' ') . 'value="' . $jenisaplikasi->id . '">' . $jenisaplikasi->nama . '</option>';
                                                        } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="ref_tingkatkematangan_id" class="col-sm-2 col-form-label">Tingkat Kematangan</label>
                                                <div class="col-sm-10">
                                                    <select class="form-control" name="ref_tingkatkematangan_id" id="ref_tingkatkematangan_id">
                                                        <option disabled selected value> -- Pilih Tingkat Kematangan -- </option>
                                                        <?php
                                                        foreach ($kematangans as $kematangan) {
                                                            echo '<option' . ($kematangan->id == $apps->ref_tingkatkematangan_id ? ' selected="selected" ' : ' ') . 'value="' . $kematangan->id . '">' . $kematangan->nama . '</option>';
                                                        } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="ref_sistempengamanan_id" class="col-sm-2 col-form-label">Sistem Pengamanan</label>
                                                <div class="col-sm-10">
                                                    <select class="form-control" name="ref_sistempengamanan_id" id="ref_sistempengamanan_id">
                                                        <option disabled selected value> -- Pilih Sistem Pengamanan -- </option>
                                                        <?php
                                                        foreach ($pengamans as $pengaman) {
                                                            echo '<option' . ($pengaman->id == $apps->ref_sistempenganan_id ? ' selected="selected" ' : ' ') . 'value="' . $pengaman->id . '">' . $pengaman->nama . '</option>';
                                                        } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="ref_tenagateknis_id" class="col-sm-2 col-form-label">Tenaga Teknis</label>
                                                <div class="col-sm-10">
                                                    <select class="form-control" name="ref_tenagateknis_id" id="ref_tenagateknis_id">
                                                        <option disabled selected value> -- Pilih Tenaga Teknis -- </option>
                                                        <?php
                                                        foreach ($tekniss as $teknis) {
                                                            echo '<option' . ($teknis->id == $apps->ref_tenagateknis_id ? ' selected="selected" ' : ' ') . 'value="' . $teknis->id . '">' . $teknis->nama . '</option>';
                                                        } ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- ########################### -->
                            </div>
                            <div class="tab-pane show" id="tab-eg7-6" role="tabpanel">
                                <!-- ######### Berita Acara ############ -->
                                <div class="col-md-10">
                                    <div class="main-card card">
                                        <div class="card-body">
                                            <div class="form-group row">
                                                <label for="tgl_ba" class="col-sm-2 col-form-label">Tanggal Rapat</label>
                                                <div class="col-sm-10">
                                                    <input type="date" id="tgl_ba" name="tgl_ba" class="form-control" value="<?= $apps->tgl_ba ?>">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="tempat_ba" class="col-sm-2 col-form-label">Tempat Rapat</label>
                                                <div class="col-sm-10">
                                                    <input type="text" id="tempat_ba" name="tempat_ba" placeholder="Tempat Rapat Teknis" class="form-control" value="<?= $apps->tempat_ba ?>">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="maintenance_ba" class="col-sm-2 col-form-label">Maintenance Aplikasi</label>
                                                <div class="col-sm-10">
                                                    <select class="form-control select2" name="maintenance_ba" id="maintenance_ba" style="width: 100%;">
                                                        <option disabled selected value> -- Pilih OPD -- </option>
                                                        <?php
                                                        foreach ($opds as $opd) {
                                                            echo '<option' . ($opd->id == $apps->maintenance_ba ? ' selected="selected" ' : ' ') . 'value="' . $opd->id . '">' . $opd->nama . '</option>';
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="pengembangan_ba" class="col-sm-2 col-form-label">Pengembangan Aplikasi</label>
                                                <div class="col-sm-10">
                                                    <select class="form-control select2" name="pengembangan_ba" id="pengembangan_ba" style="width: 100%;">
                                                        <option disabled selected value> -- Pilih OPD -- </option>
                                                        <?php
                                                        foreach ($opds as $opd) {
                                                            echo '<option' . ($opd->id == $apps->pengembangan_ba ? ' selected="selected" ' : ' ') . 'value="' . $opd->id . '">' . $opd->nama . '</option>';
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="server_ba" class="col-sm-2 col-form-label">Pemeliharaan Server</label>
                                                <div class="col-sm-10">
                                                    <select class="form-control select2" name="server_ba" id="server_ba" style="width: 100%;">
                                                        <option disabled selected value> -- Pilih OPD -- </option>
                                                        <?php
                                                        foreach ($opds as $opd) {
                                                            echo '<option' . ($opd->id == $apps->server_ba ? ' selected="selected" ' : ' ') . 'value="' . $opd->id . '">' . $opd->nama . '</option>';
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="ssl_ba" class="col-sm-2 col-form-label">Keamanan(Pemasangan SSL)</label>
                                                <div class="col-sm-10">
                                                    <select class="form-control select2" name="ssl_ba" id="ssl_ba" style="width: 100%;">
                                                        <option disabled selected value> -- Pilih OPD -- </option>
                                                        <?php
                                                        foreach ($opds as $opd) {
                                                            echo '<option' . ($opd->id == $apps->ssl_ba ? ' selected="selected" ' : ' ') . 'value="' . $opd->id . '">' . $opd->nama . '</option>';
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="backup_ba" class="col-sm-2 col-form-label">Backup Data Aplikasi</label>
                                                <div class="col-sm-10">
                                                    <select class="form-control select2" name="backup_ba" id="backup_ba" style="width: 100%;">
                                                        <option disabled selected value> -- Pilih OPD -- </option>
                                                        <?php
                                                        foreach ($opds as $opd) {
                                                            echo '<option' . ($opd->id == $apps->backup_ba ? ' selected="selected" ' : ' ') . 'value="' . $opd->id . '">' . $opd->nama . '</option>';
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="restore_ba" class="col-sm-2 col-form-label">Restore Aplikasi</label>
                                                <div class="col-sm-10">
                                                    <select class="form-control select2" name="restore_ba" id="restore_ba" style="width: 100%;">
                                                        <option disabled selected value> -- Pilih OPD -- </option>
                                                        <?php
                                                        foreach ($opds as $opd) {
                                                            echo '<option' . ($opd->id == $apps->restore_ba ? ' selected="selected" ' : ' ') . 'value="' . $opd->id . '">' . $opd->nama . '</option>';
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <!-- <div class="form-group row">
                                                <label for="tempat_ba" class="col-sm-2 col-form-label">Nama Kadin Kominfo</label>
                                                <div class="col-sm-10">
                                                    <input type="text" id="tempat_ba" name="tempat_ba" placeholder="Tempat Rapat Teknis" class="form-control" value="<?= $apps->tempat_ba ?>">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="tempat_ba" class="col-sm-2 col-form-label">NIP Kadin Kominfo</label>
                                                <div class="col-sm-10">
                                                    <input type="text" id="tempat_ba" name="tempat_ba" placeholder="Tempat Rapat Teknis" class="form-control" value="<?= $apps->tempat_ba ?>">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="tempat_ba" class="col-sm-2 col-form-label">Nama Kepala Satker</label>
                                                <div class="col-sm-10">
                                                    <input type="text" id="tempat_ba" name="tempat_ba" placeholder="Tempat Rapat Teknis" class="form-control" value="<?= $apps->tempat_ba ?>">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="tempat_ba" class="col-sm-2 col-form-label">NIP Kepala Satker</label>
                                                <div class="col-sm-10">
                                                    <input type="text" id="tempat_ba" name="tempat_ba" placeholder="Tempat Rapat Teknis" class="form-control" value="<?= $apps->tempat_ba ?>">
                                                </div>
                                            </div> -->
                                            <button type="button" onclick="print_ba(<?= $apps->id ?>)" class="mb-2 ml-3 btn btn-primary text-white mt-3 shadow">Cetak Berita Acara</button>
                                            <button type="button" onclick="upload_ba()" class="mb-2 ml-3 btn btn-warning text-white mt-3 shadow">Upload Berita Acara</button>
                                        </div>
                                        <table id="list_ba" class="table table-bordered table-striped" style="width: 100%;">
                                            <thead>
                                                <tr>
                                                    <th>No.</th>
                                                    <!-- <th>No Versi</th> -->
                                                    <!-- <th>Deskripsi Update</th> -->
                                                    <th>Tanggal</th>
                                                    <th>Aksi</th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                                <!-- ########################### -->
                            </div>
                            <div class="tab-pane show" id="tab-eg7-0" role="tabpanel">
                                <!-- ######### Upload File ########## -->
                                <div class="col-md-10">
                                    <div class="main-card card">
                                        <div class="card-body">
                                            <button type="button" class="btn btn-block btn-primary pull-right" onclick="addVersi()">Tambah Versi<span class="badge badge-light"></button>
                                        </div>
                                        <table id="list_versi_app" class="table table-bordered table-striped" style="width: 100%;">
                                            <thead>
                                                <tr>
                                                    <th>No.</th>
                                                    <th>No Versi</th>
                                                    <th>Deskripsi Update</th>
                                                    <th>Tanggal</th>
                                                    <th>Aksi</th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>

                                <!-- ########################### -->
                            </div>
                            <button type="button" onclick="saveAplikasi()" class="mb-2 ml-3 btn btn-info text-white mt-3 shadow">Save Update</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- MODAl Tambah Data Versi Aplikasi-->
<div class="modal fade" id="modalAddVersi">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Data Versi Aplikasi</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <!-- <form> -->
            <form id="formAddVersi" action="<?= base_url('Aplikasi/add_versi_aplikasi') ?>" method="POST" enctype="multipart/form-data" -->
                <div class="modal-body" id="bodyModalAddVersi">
                    <div class="p-2">
                        <input id="versi_id" name="versi_id" type="hidden" readonly class="form-control-plaintext" value="<?= $apps->id ?>">
                        <div class="form-group row">
                            <label for="add_versi_no" class="col-sm-2 col-form-label">No Versi</label>
                            <div class="col-sm-10">
                                <input type="text" id="add_versi_no" name="add_versi_no" placeholder="1.0.0" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="add_versi_deskripsi" class="col-sm-2 col-form-label">Deskripsi Pembaruan</label>
                            <div class="col-sm-10">
                                <input type="text" id="add_versi_deskripsi" name="add_versi_deskripsi" placeholder="Deskripsi Pembaruan" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="add_versi_tgl" class="col-sm-2 col-form-label">Tanggal</label>
                            <div class="col-sm-10">
                                <input type="date" id="add_versi_tgl" name="add_versi_tgl" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="add_versi_file" class="col-sm-2 col-form-label">File</label>
                            <div class="col-sm-10">
                                <input type="file" id="add_versi_file" name="add_versi_file">
                            </div>
                            <div id="filelist">Your browser doesn't support HTML5 upload.</div>
                        </div>

                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" onclick="tambahVersi()" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!-- MODAl Tambah Data Berita Acara-->
<div class="modal fade" id="modalAddBa">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Data Berita Acara</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <!-- <form> -->
            <form id="formAddBA" action="<?= base_url('Aplikasi/add_berita_acara') ?>" method="POST" enctype="multipart/form-data" -->
                <div class="modal-body" id="bodyModalAddVersi">
                    <div class="p-2">
                        <input id="ba_id" name="ba_id" type="hidden" readonly class="form-control-plaintext" value="<?= $apps->id ?>">
                        <!-- <div class="form-group row">
                            <label for="add_versi_no" class="col-sm-2 col-form-label">No Versi</label>
                            <div class="col-sm-10">
                                <input type="text" id="add_versi_no" name="add_versi_no" placeholder="1.0.0" class="form-control">
                            </div>
                        </div> -->
                        <div class="form-group row">
                            <label for="add_ba_tgl" class="col-sm-2 col-form-label">Tanggal</label>
                            <div class="col-sm-10">
                                <input type="date" id="add_ba_tgl" name="add_ba_tgl" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="add_ba_file" class="col-sm-2 col-form-label">File</label>
                            <div class="col-sm-10">
                                <input type="file" id="add_ba_file" name="add_ba_file">
                            </div>
                            <!-- <div id="filelist">Your browser doesn't support HTML5 upload.</div> -->
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" onclick="tambahBa()" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!-- MODAl Tambah Data Penanggung Jawab-->
<div class="modal fade" id="modalAddPJ">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Data Penanggung Jawab</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form>
                <!-- id="formAdd" action="<?= base_url('Aplikasi/add_aplikasi') ?>" method="POST" enctype="multipart/form-data" -->
                <div class="modal-body" id="bodyModalAddPJ">
                    <div class="p-2">
                        <div class="form-group row">
                            <label for="add_pj_nama" class="col-sm-2 col-form-label">Nama Penanggung Jawab</label>
                            <div class="col-sm-10">
                                <input type="text" id="add_pj_nama" name="add_pj_nama" placeholder="Nama Penanggung Jawab" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="add_pj_nip" class="col-sm-2 col-form-label">NIP</label>
                            <div class="col-sm-10">
                                <input type="text" id="add_pj_nip" name="add_pj_nip" placeholder="NIP Penanggung Jawab" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="add_pj_opd" class="col-sm-2 col-form-label">Nama Satker</label>
                            <div class="col-sm-10">
                                <select class="form-control" name="add_pj_opd" id="add_pj_opd">
                                    <?php
                                    foreach ($opds as $opd) {
                                    ?>
                                        <option value="<?= $opd->id ?>"><?= $opd->nama ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="add_pj_alamat_opd" class="col-sm-2 col-form-label">Alamat Satker</label>
                            <div class="col-sm-10">
                                <input type="text" id="add_pj_alamat_opd" name="add_pj_alamat_opd" placeholder="Alamat Satuan Kerja" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="add_pj_provinsi" class="col-sm-2 col-form-label">Provinsi</label>
                            <div class="col-sm-10">
                                <input type="text" id="add_pj_provinsi" name="add_pj_provinsi" class="form-control" value="Jawa Timur" readonly>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="add_pj_kota" class="col-sm-2 col-form-label">Kota/Kabupaten</label>
                            <div class="col-sm-10">
                                <input type="text" id="add_pj_kota" name="add_pj_kota" class="form-control" value="Kota Kediri" readonly>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="add_pj_kodepos" class="col-sm-2 col-form-label">Kode POS</label>
                            <div class="col-sm-10">
                                <input type="number" id="add_pj_kodepos" name="add_pj_kodepos" placeholder="Kode Pos Satuan Kerja" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="add_pj_nohp" class="col-sm-2 col-form-label">No HP</label>
                            <div class="col-sm-10">
                                <input type="number" id="add_pj_nohp" name="add_pj_nohp" placeholder="No HP Penanggung Jawab" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="add_pj_email" class="col-sm-2 col-form-label">Email</label>
                            <div class="col-sm-10">
                                <input type="text" id="add_pj_email" name="add_pj_email" placeholder="Email Penanggung Jawab" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" onclick="tambahPJ()" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!-- MODAl Edit Data Aplikasi-->
<div class="modal fade" id="modalEdit">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Edit Data Aplikasi</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="formEditInfoApp" action="<?= base_url('Aplikasi/editInfoApp/') ?>" method="POST" enctype="multipart/form-data">
                <div class="modal-body" id="bodyModalEdit">
                    <div class="p-2">
                        <input id="id_edit" name="id_edit" type="hidden" readonly class="form-control-plaintext">
                        <div class="form-group row">
                            <label for="edit_no" class="col-sm-2 col-form-label">Nomor Asset</label>
                            <div class="col-sm-10">
                                <input type="text" id="edit_no" name="edit_no" placeholder="Kode Asset" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="edit_hostname" class="col-sm-2 col-form-label">Hostname</label>
                            <div class="col-sm-10">
                                <input type="text" id="edit_hostname" name="edit_hostname" placeholder="Alamat URL Website" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="edit_ip" class="col-sm-2 col-form-label">IP Address</label>
                            <div class="col-sm-10">
                                <input type="text" id="edit_ip" name="edit_ip" placeholder="Alamat IP Address" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="edit_nama" class="col-sm-2 col-form-label">Nama Internal</label>
                            <div class="col-sm-10">
                                <input type="text" id="edit_nama" name="edit_nama" placeholder="Nama Aplikasi" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" onclick="saveInfoApp()" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/plupload/3.1.2/plupload.full.min.js"></script>
<script>
    let id = undefined

    var uploader = new plupload.Uploader({
            runtimes: 'html5,html4',
            browse_button: 'add_versi_file',
            url: '<?= base_url() ?>/Aplikasi/chunking_upload',
            chunk_size: '10mb',
            filters: {
                //   max_file_size: '150mb',
                mime_types: [{
                    title: "Compressed files",
                    extensions: "rar,zip"
                }]
            },
            init: {
                PostInit: function() {
                    document.getElementById('filelist').innerHTML = '';
                },
                FilesAdded: function(up, files) {
                    plupload.each(files, function(file) {
                        document.getElementById('filelist').innerHTML += `<div id="${file.id}">${file.name} (${plupload.formatSize(file.size)}) <strong></strong></div>`;
                    });
                    // uploader.start();
                },
                UploadProgress: function(up, file) {
                    document.querySelector(`#${file.id} strong`).innerHTML = `<span>${file.percent}%</span>`;
                },
                Error: function(up, err) {
                    console.log(err);
                }
            }
        });

    $(document).ready(function() {
        uploader.init();
        $("#ref_opd_id").select2({
            theme: 'bootstrap4',
            tags: true
        })
        $("#maintenance_ba").select2({
            theme: 'bootstrap4',
            tags: true
        })
        $("#pengembangan_ba").select2({
            theme: 'bootstrap4',
            tags: true
        })
        $("#server_ba").select2({
            theme: 'bootstrap4',
            tags: true
        })
        $("#ssl_ba").select2({
            theme: 'bootstrap4',
            tags: true
        })
        $("#backup_ba").select2({
            theme: 'bootstrap4',
            tags: true
        })
        $("#restore_ba").select2({
            theme: 'bootstrap4',
            tags: true
        })
        $('#ref_opd_id').change(function() {
            $('#ref_opd_id1').val($('#ref_opd_id :selected').text())
        })
        $('#hostname').change(function() {
            $('#hostname1').val($('#hostname').val())
        })
        get_listVersi()
        get_listBa()
    })

    $('#ref_dokumen_id').change(function() {
        if (this.value == 2 || this.value == 3 || this.value == 4) {
            $('#dokumen_app').show()
        } else {
            $('#dokumen_app').hide()
        }
    })


    function addVersi() {
        $('#modalAddVersi').modal('show')
    }

    function upload_ba(){
        $('#modalAddBa').modal('show')
    }

    function tambahVersi() {
        Swal.fire({
            title: 'Apakah Anda Yakin?',
            text: "Silahkan periksa kembali data versi aplikasi, apakah data yang anda masukkan sudah benar?!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya',
            cancelButtonText: 'Batal'
        }).then((result) => {
            if (result.value) {
                uploader.start();
                $('#formAddVersi').submit()
            }
        })
    }

    function tambahBa() {
        Swal.fire({
            title: 'Apakah Anda Yakin?',
            text: "Silahkan periksa kembali data versi aplikasi, apakah data yang anda masukkan sudah benar?!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya',
            cancelButtonText: 'Batal'
        }).then((result) => {
            if (result.value) {
                uploader.start();
                $('#formAddBA').submit()
            }
        })
    }

    function get_listBa() {
        $('#list_ba').DataTable({
            "destroy": true,
            "processing": true,
            "serverSide": true,
            "autowidth": true,
            "lengthMenu": [
                [10, 25, 50, -1],
                [10, 25, 50, "All"]
            ],
            dom: 'Bfrtip',
            buttons: ['pageLength',
                {
                    extend: 'excelHtml5',
                    exportOptions: {
                        columns: [0, 1, 2]
                    }
                },
            ],
            "ajax": {
                'url': '<?= base_url('Aplikasi/api/get_ba') ?>',
                'type': 'POST',
                'dataType': 'JSON',
                'data': function(d) {
                    // return $.extend({}, d, {
                    //     "selected_opd": $('#list_opd').val()
                    // });
                },
            },

            "columns": [{
                    "data": "no",
                    "defaultContent": "<i>Not set</i>"
                },
                // {
                //     "data": "no_versi",
                //     "defaultContent": "<i>Not set</i>"
                // },
                // {
                //     "data": "deskripsi",
                //     "defaultContent": "<i>Not set</i>"
                // },
                {
                    "data": "tanggal",
                    "defaultContent": "<i>Not set</i>"
                },
                {
                    "data": null,
                    "render": function(data, type, row) {
                        return '<button type="button" class="btn btn-sm btn-block btn-info" onclick="detailApp(' + data.id + ')">Download<span class="badge badge-light"></button>\
                                <button type="button" class="btn btn-sm btn-block btn-info" onclick="detailApp(' + data.id + ')">Edit<span class="badge badge-light"></button>\
                                <button type="button" class="btn btn-sm btn-block btn-danger" onclick="deactiveAplikasi(' + data.id + ')">Hapus <span class="badge badge-light"></button>';
                    }
                },
            ],


            "order": [
                ['0', 'desc']
            ],

            "columnDefs": [{
                    "orderable": false,
                    "targets": [0, 1, 2, 3]
                },
                {
                    "width": "20%",
                    "targets": -1
                },
                {
                    className: 'dt-body-center',
                    "targets": 3
                }
            ],
        });
    }

    function get_listVersi() {
        $('#list_versi_app').DataTable({
            "destroy": true,
            "processing": true,
            "serverSide": true,
            "autowidth": true,
            "lengthMenu": [
                [10, 25, 50, -1],
                [10, 25, 50, "All"]
            ],
            dom: 'Bfrtip',
            buttons: ['pageLength',
                {
                    extend: 'excelHtml5',
                    exportOptions: {
                        columns: [0, 1, 2]
                    }
                },
            ],
            "ajax": {
                'url': '<?= base_url('Aplikasi/api/get_versi') ?>',
                'type': 'POST',
                'dataType': 'JSON',
                'data': function(d) {
                    // return $.extend({}, d, {
                    //     "selected_opd": $('#list_opd').val()
                    // });
                },
            },

            "columns": [{
                    "data": "no",
                    "defaultContent": "<i>Not set</i>"
                },
                {
                    "data": "no_versi",
                    "defaultContent": "<i>Not set</i>"
                },
                {
                    "data": "deskripsi",
                    "defaultContent": "<i>Not set</i>"
                },
                {
                    "data": "tanggal",
                    "defaultContent": "<i>Not set</i>"
                },
                {
                    "data": null,
                    "render": function(data, type, row) {
                        return '<button type="button" class="btn btn-sm btn-block btn-info" onclick="detailApp(' + data.id + ')">Download<span class="badge badge-light"></button>\
                                <button type="button" class="btn btn-sm btn-block btn-info" onclick="detailApp(' + data.id + ')">Edit<span class="badge badge-light"></button>\
                                <button type="button" class="btn btn-sm btn-block btn-danger" onclick="deactiveAplikasi(' + data.id + ')">Hapus <span class="badge badge-light"></button>';
                    }
                },
            ],


            "order": [
                ['0', 'desc']
            ],

            "columnDefs": [{
                    "orderable": false,
                    "targets": [0, 1, 2, 3]
                },
                {
                    "width": "20%",
                    "targets": -1
                },
                {
                    className: 'dt-body-center',
                    "targets": 3
                }
            ],
        });
    }

    function get_listaplikasi() {
        $('#versi_app').DataTable({
            "destroy": true,
            "processing": true,
            "serverSide": true,
            "autowidth": true,
            "lengthMenu": [
                [10, 25, 50, -1],
                [10, 25, 50, "All"]
            ],
            dom: 'Bfrtip',
            buttons: ['pageLength',
                {
                    extend: 'excelHtml5',
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4]
                    }
                },
            ],
            "ajax": {
                'url': '<?= base_url('Aplikasi/Api/get_ajax') ?>',
                'type': 'POST',
                'dataType': 'JSON',
                'data': function(d) {
                    return $.extend({}, d, {
                        "selected_opd": $('#list_opd').val()
                    });
                },
            },

            "columns": [{
                    "data": "no",
                    "defaultContent": "<i>Not set</i>"
                },
                {
                    "data": "no_asset",
                    "defaultContent": "<i>Not set</i>"
                },
                {
                    "data": "hostname",
                    "defaultContent": "<i>Not set</i>"
                },
                {
                    "data": "ipaddress",
                    "defaultContent": "<i>Not set</i>"
                },
                {
                    "data": "nm_internal",
                    "defaultContent": "<i>Not set</i>"
                },
                {
                    "data": "nama_opd",
                    "defaultContent": "<i>Not set</i>"
                },
                {
                    "data": null,
                    "render": function(data, type, row) {
                        return '<button type="button" class="btn btn-sm btn-block btn-info" onclick="detailApp(' + data.id + ')">Detail<span class="badge badge-light"></button>\
                                <button type="button" class="btn btn-sm btn-block btn-danger" onclick="deactiveAplikasi(' + data.id + ')">Hapus <span class="badge badge-light"></button>';
                    }
                },
            ],


            "order": [
                ['0', 'desc']
            ],

            "columnDefs": [{
                    "orderable": false,
                    "targets": [0, 1, 2, 3, 4]
                },
                {
                    "width": "20%",
                    "targets": -1
                },
                {
                    className: 'dt-body-center',
                    "targets": 4
                }
            ],
        });
    }

    function print_ba(id) {
        window.location.href = "<?= base_url('Aplikasi/print_ba/') ?>" + id;
    }

    function saveAplikasi() {
        Swal.fire({
            title: 'Apakah Anda Yakin?',
            text: "Silahkan periksa kembali data aplikasi, apakah data yang anda masukkan sudah benar?!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya',
            cancelButtonText: 'Batal'
        }).then((result) => {
            if (result.value) {
                $('#formAplikasi').submit()
            }
        })
    }

    function editInfoApp(id) {
        $.ajax({
            type: 'GET',
            url: '<?= base_url('Aplikasi/Api/get_aplikasi/') ?>' + id,
            dataType: 'JSON',
            success: function(response) {
                console.log(response)
                $("#id_edit").val(response.id)
                $("#edit_no").val(response.no_asset)
                $("#edit_hostname").val(response.hostname)
                $("#edit_ip").val(response.ipaddress)
                $("#edit_nama").val(response.nm_internal)
                $('#modalEdit').modal('show')
            }
        })
    }

    function saveInfoApp() {
        Swal.fire({
            title: 'Apakah Anda Yakin?',
            text: "Silahkan periksa kembali data aplikasi, apakah data yang anda masukkan sudah benar?!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya',
            cancelButtonText: 'Batal'
        }).then((result) => {
            if (result.value) {
                $('#formEditInfoApp').submit()
            }
        })
    }
</script>