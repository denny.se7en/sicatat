<div class="d-flex flex-column">
    <div class="" style="min-height: 200px;">
        <div class="d-flex flex-fill align-items-center justify-content-around bd-highlight">
            <div id="gambar1">
                <div class="d-flex flex-column align-items-center bd-highlight">
                    <span class="badge badge-pill badge-danger">0%</span>
                    <div class="p-2 bd-highlight">
                        <img id="myImg1" src="<?= isset($kegiatan->progress0) ? base_url('upload/' . $kegiatan->progress0->img) : base_url('upload/img.jpg') ?>" style="height:300px;width:300px" class="img-thumbnail">
                        <div class="d-flex flex-column align-items-center justify-content-center">
                            <div style="font-weight:bold;">Serapan</div>
                            <div style="font-weight:bold;"><?= "Rp. " . number_format((isset($kegiatan->progress0) ? $kegiatan->progress0->serapan : 0), 0, ',', '.') ?></div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="gambar2">
                <div class="d-flex flex-column align-items-center bd-highlight">
                    <span class="badge badge-pill badge-warning">50%</span>
                    <div class="p-2 bd-highlight">
                        <img id="myImg1" src="<?= isset($kegiatan->progress50) ? base_url('upload/' . $kegiatan->progress50->img) : base_url('upload/img.jpg') ?>" style="height:300px;width:300px" class="img-thumbnail">
                        <div class="d-flex flex-column align-items-center justify-content-center">
                            <div style="font-weight:bold;">Serapan</div>
                            <div style="font-weight:bold;"><?= "Rp. " . number_format((isset($kegiatan->progress50) ? $kegiatan->progress50->serapan : 0), 0, ',', '.') ?></div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="gambar3">
                <div class="d-flex flex-column align-items-center bd-highlight">
                    <span class="badge badge-pill badge-success">100%</span>
                    <div class="p-2 bd-highlight">
                        <img id="myImg1" src="<?= isset($kegiatan->progress100) ? base_url('upload/' . $kegiatan->progress100->img) : base_url('upload/img.jpg') ?>" style="height:300px;width:300px" class="img-thumbnail">
                        <div class="d-flex flex-column align-items-center justify-content-center">
                            <div style="font-weight:bold;">Serapan</div>
                            <div style="font-weight:bold;"><?= "Rp. " . number_format((isset($kegiatan->progress100) ? $kegiatan->progress100->serapan : 0), 0, ',', '.') ?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="px-3" style="min-height: 200px;">
        <div style="font-weight:bold;color:blue;">Lokasi Kegiatan</div>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th scope="col">Alamat</th>
                    <th scope="col">Kelurahan</th>
                    <th scope="col">RW</th>
                    <th scope="col">RT</th>
                    <th scope="col">Latitude</th>
                    <th scope="col">Longitude</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><?= $kegiatan->alamat ?></td>
                    <td><?= $kegiatan->kelurahan ?></td>
                    <td><?= $kegiatan->rw ?></td>
                    <td><?= $kegiatan->rt ?></td>
                    <td><?= $kegiatan->lat ?></td>
                    <td><?= $kegiatan->lon ?></td>
                </tr>
            </tbody>
        </table>

        <!-- URAIAN KEBUTUHAN -->
        <div style="font-weight:bold;color:blue;">Uraian Kebutuhan</div>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th scope="col">Alat</th>
                    <th scope="col">%</th>
                    <th scope="col">Bahan</th>
                    <th scope="col">%</th>
                    <th scope="col">Tenaga Kerja</th>
                    <th scope="col">%</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><?= "Rp. " . number_format($kegiatan->biaya_alat, 0, ',', '.') ?></td>
                    <td><?= $kegiatan->biaya_jumlah_rkb ? round($kegiatan->biaya_alat / $kegiatan->biaya_jumlah_rkb * 100, 2) : 0 ?>%</td>
                    <td><?= "Rp. " . number_format($kegiatan->biaya_bahan, 0, ',', '.') ?></td>
                    <td><?= $kegiatan->biaya_jumlah_rkb ? round($kegiatan->biaya_bahan / $kegiatan->biaya_jumlah_rkb * 100, 2) : 0 ?>%</td>
                    <td><?= "Rp. " . number_format($kegiatan->biaya_tenaga, 0, ',', '.') ?></td>
                    <td><?= $kegiatan->biaya_jumlah_rkb ? round($kegiatan->biaya_tenaga / $kegiatan->biaya_jumlah_rkb * 100, 2) : 0 ?>%</td>
                </tr>
            </tbody>
        </table>

        <!-- JUMLAH -->
        <div style="font-weight:bold;color:blue;">Jumlah</div>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th scope="col">Jumlah RKB</th>
                    <th scope="col">Biaya Pengelolaan Kegiatan</th>
                    <th scope="col">Jumlah Total</th>
                    <th scope="col">Serapan</th>
                    <th scope="col">SILPA</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><?= "Rp. " . number_format($kegiatan->biaya_jumlah_rkb, 0, ',', '.') ?></td>
                    <td><?= "Rp. " . number_format($kegiatan->biaya_pengelolaan, 0, ',', '.') ?></td>
                    <td><?= "Rp. " . number_format($kegiatan->biaya_total, 0, ',', '.') ?></td>
                    <?php if ($kegiatan->progress100) { ?>
                        <td style="font-weight:bold;color:blue"><?= "Rp. " . number_format((isset($kegiatan->progress100) ? $kegiatan->progress100->serapan : 0), 0, ',', '.') ?></td>
                        <td style="font-weight:bold;color:blue"><?= "Rp. " . number_format(($kegiatan->biaya_total - (isset($kegiatan->progress100) ? $kegiatan->progress100->serapan : 0)), 0, ',', '.') ?></td>
                    <?php } else if ($kegiatan->progress50) { ?>
                        <td style="font-weight:bold;color:blue"><?= "Rp. " . number_format((isset($kegiatan->progress50) ? $kegiatan->progress50->serapan : 0), 0, ',', '.') ?></td>
                        <td style="font-weight:bold;color:blue"><?= "Rp. " . number_format(($kegiatan->biaya_total - (isset($kegiatan->progress50) ? $kegiatan->progress50->serapan : 0)), 0, ',', '.') ?></td>
                    <?php } else if ($kegiatan->progress0) { ?>
                        <td style="font-weight:bold;color:blue"><?= "Rp. " . number_format((isset($kegiatan->progress0) ? $kegiatan->progress0->serapan : 0), 0, ',', '.') ?></td>
                        <td style="font-weight:bold;color:blue"><?= "Rp. " . number_format(($kegiatan->biaya_total - (isset($kegiatan->progress0) ? $kegiatan->progress0->serapan : 0)), 0, ',', '.') ?></td>
                    <?php } else { ?>
                        <td style="font-weight:bold;color:blue"><?= "Rp. 0" ?></td>
                        <td style="font-weight:bold;color:blue"><?= "Rp. " . number_format($kegiatan->biaya_total, 0, ',', '.') ?></td>
                    <?php } ?>

                </tr>
            </tbody>
        </table>
    </div>
</div>

<script>
    function tampilkanPreview(userfile, idpreview) {
        var gb = userfile.files;
        for (var i = 0; i < gb.length; i++) {
            var gbPreview = gb[i];
            var imageType = /image.*/;
            var preview = document.getElementById(idpreview);
            var reader = new FileReader();
            if (gbPreview.type.match(imageType)) {
                //jika tipe data sesuai
                preview.file = gbPreview;
                reader.onload = (function(element) {
                    return function(e) {
                        element.src = e.target.result;
                    };
                })(preview);
                //membaca data URL gambar
                reader.readAsDataURL(gbPreview);
            } else {
                //jika tipe data tidak sesuai
                alert("Tipe file tidak sesuai. Gambar harus bertipe .png, .gif atau .jpg.");
            }
        }
    }

    $('#serapan').keyup(function() {
        let val = this.value.toString().replace(/\D+/g, "")
        $('#serapan_h').val(val)
        $('#serapan').val(val.replace(/\B(?=(\d{3})+(?!\d))/g, "."))
    })
</script>