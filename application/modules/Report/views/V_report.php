

<div class="card-body">
    <div class=" pb-3">
        <select class="form-control" name="list_opd" id="list_opd">
            <option value="0">Semua OPD</option>
            <?php
            foreach ($opds as $opd) {
            ?>
                <option value="<?= $opd->id ?>"><?= $opd->nama?></option>
            <?php } ?>
        </select>
    </div>
    <table id="reportaplikasi" class="table table-bordered table-striped" style="width: 100%;">
        <thead>
            <tr>
                <th>No.</th>
                <th>Nomor Asset</th>
                <th>Hostname</th>
                <th>Nama Internal</th>
                <th>IP Address</th>
                <th>OPD</th>
                <th>OS</th>
                <th>Bahasa Pemrograman</th>
                <th>Dokumen</th>
                <th>Jenis Aplikasi</th>
                <th>Tingkat Kematangan</th>
                <th>Sistem Pengaman</th>
            </tr>
        </thead>
    </table>
</div>

<script>
    let id = undefined

    $(function() {
        get_aplikasi()
    })

    function get_aplikasi() {
        $('#reportaplikasi').DataTable({
            "destroy": true,
            "processing": true,
            "serverSide": true,
            "scrollX": true,
            "lengthMenu": [
                [10, 25, 50, -1],
                [10, 25, 50, "All"]
            ],
            dom: 'Bfrtip',
            buttons: ['pageLength',
                {
                    extend: 'excelHtml5',
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
                    }
                },
            ],
            "ajax": {
                'url': '<?= base_url('Report/Api/get_ajax') ?>',
                'type': 'POST',
                'dataType': 'JSON',
                'data': function(d) {
                    return $.extend({}, d, {
                        "selected_opd": $('#list_opd').val()
                    });
                },
            },

            "columns": [{
                    "data": "no",
                    "defaultContent": "<i>Not set</i>"
                },
                {
                    "data": "no_asset",
                    "defaultContent": "<i>Not set</i>"
                },
                {
                    "data": "hostname",
                    "defaultContent": "<i>Not set</i>"
                },
                {
                    "data": "nm_internal",
                    "defaultContent": "<i>Not set</i>"
                },
                {
                    "data": "ipaddress",
                    "defaultContent": "<i>Not set</i>"
                },
                {
                    "data": "o_nama",
                    "defaultContent": "<i>Not set</i>"
                },
                {
                    "data": "os",
                    "defaultContent": "<i>Not set</i>",
                },
                {
                    "data": "bhs_pemrograman",
                    "defaultContent": "<i>Not set</i>",
                },
                {
                    "data": "dok_nama",
                    "defaultContent": "<i>Not set</i>",
                },
                {
                    "data": "jns_nama",
                    "defaultContent": "<i>Not set</i>",
                },
                {
                    "data": "tkk_nama",
                    "defaultContent": "<i>Not set</i>",
                },
                {
                    "data": "sp_nama",
                    "defaultContent": "<i>Not set</i>",
                },
            ],


            "order": [
                ['1', 'asc']
            ],

            "columnDefs": [{
                    "orderable": false,
                    "targets": [0, -1]
                }
            ],
        });
    }

    $('#list_opd').change(function(e) {
        $('#reportaplikasi').DataTable().draw();
    })
</script>