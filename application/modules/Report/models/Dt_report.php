<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Dt_Report extends CI_Model
{

    // start datatables
    var $column_order = array(null, 'o.nama', 'k.lokasi', 'kel.nama_kel', 'k.alamat', 'k.rw', 'k.rt', 'k.uraian', 'p.progress', 'p.serapan'); //set column field database for datatable orderable
    var $column_search = array('o.nama', 'k.lokasi', 'k.alamat', 'kel.nama_kel', 'k.uraian', 'p.progress', 'p.serapan'); //set column field database for datatable searchable
    var $order = array('id' => 'asc'); // default order 

    function __construct()
    {
        parent::__construct();
    }

    function get_datatables()
    {
        $this->_get_datatables_query();
        if (@$_POST['length'] != -1)
            $this->db->limit(@$_POST['length'], @$_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    private function _get_datatables_query()
    {
        $this->db->select('a.*, o.nama as o_nama, dok.nama as dok_nama, jns.nama as jns_nama, tkk.nama as tkk_nama, sp.nama as sp_nama')
            ->from('aplikasi a')
            ->join('ref_opd o', 'o.id = a.ref_opd_id', 'left')
            ->join('ref_dokumen dok', 'dok.id = a.ref_dokumen_id', 'left')
            ->join('ref_jenisaplikasi jns', 'jns.id = a.ref_jenisaplikasi_id', 'left')
            ->join('ref_tingkatkematangan tkk', 'tkk.id = a.ref_tingkatkematangan_id', 'left')
            ->join('ref_sistempengaman sp', 'sp.id = a.ref_sistempengamanan_id', 'left');
            
            
        if ($this->input->post('selected_opd')) {
            $this->db->where(['a.ref_opd_id' => $this->input->post('selected_opd')]);
        }
        $this->db->where(['a._active' => 1]);

        $i = 0;

        if (@$_POST['search']['value']) { // if datatable send POST for search
            foreach ($this->column_search as $item) { // loop column 
                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
                $i++;
            }
        }

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    function count_all()
    {
        $this->db->from('aplikasi');
        return $this->db->count_all_results();
    }
}
