<?php

(defined('BASEPATH')) or exit('No direct script access allowed');

class Report extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        if ($this->session->jabatan != 'admin') {
            redirect('');
        }
    }

    public function index()
    {
        redirect('Report/input');
    }

    public function input()
    {
        // $this->form_validation->set_rules('nama', 'Nama Pelanggar', 'required', ['required' => 'Nama pelanggar wajib diisi.']);
        // $this->form_validation->set_rules('kelurahan', 'Kelurahan', 'required', ['required' => 'Kelurahan wajib diisi.']);
        // $this->form_validation->set_rules('alamat', 'alamat', 'required', ['required' => 'Alamat wajib diisi.']);
        // $this->form_validation->set_rules('ttl', 'ttl', 'required', ['required' => 'Tempat, Tgl. lahir wajib diisi.']);
        // $this->form_validation->set_rules('fotoPelanggar', 'Foto Pelanggar', 'required', ['required' => 'Foto Pelanggar wajib ada.']);
        $opd = $this->db->from('ref_opd opd')->where(['opd._active' => 1])->get()->result();;

        if ($this->form_validation->run() == FALSE) {

            $data = [
                'title'         => 'Report Aplikasi',
                'navbar'        => true,
                'header'        => true,
                'padding'       => false,
                'opds'          => $opd,
            ];

            // if (validation_errors()) {
            //     $this->session->set_flashdata('pelanggarBaruError', TRUE);
            // }

            $this->template->admin('V_report', $data);
        } else {
            redirect('Report');
        }
    }


}
