<?php

(defined('BASEPATH')) or exit('No direct script access allowed');

class Api extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        if ($this->session->jabatan != 'admin') {
            redirect('');
        }

        $this->load->model(['Dt_report']);
    }

    public function get_ajax()
    {
        $aplikasis   = $this->Dt_report->get_datatables();
        $data   = [];
        $no     = $this->input->post('start');

        foreach ($aplikasis as $item) {
            $no++;
            $item->no = $no . '. ';
        }

        $output = [
            "draw"              => $this->input->post('draw'),
            "recordsTotal"      => $this->Dt_report->count_all(),
            "recordsFiltered"   => $this->Dt_report->count_filtered(),
            "data"              => $aplikasis,
        ];

        echo json_encode($output);
    }

    // public function get_neighbour($id)
    // {

    //     $neighbours   = $this->dt_neighbour->get_datatables($id);
    //     $data   = [];
    //     $no     = $this->input->post('start');

    //     foreach ($neighbours as $item) {
    //         $no++;
    //         $item->no = $no . '. ';
    //     }

    //     $output = [
    //         "draw"              => $this->input->post('draw'),
    //         "recordsTotal"      => $this->dt_neighbour->count_all($id),
    //         "recordsFiltered"   => $this->dt_neighbour->count_filtered($id),
    //         "data"              => $neighbours,
    //     ];

    //     echo json_encode($output);
    // }

    // public function deactive($id)
    // {
    //     $this->db->where(['id' => $id])
    //         ->update('kegiatan', ['_active' => 0]);
    //     $this->_find_neighbour();
    //     echo $this->db->affected_rows();
    // }

    // private function _find_neighbour()
    // {
    //     $this->db->update('kegiatan', ['neighbour' => null]);
        
    //     $kegiatans1 = $this->db->from('kegiatan')
    //         ->where(['_active' => 1])
    //         ->get()
    //         ->result();

    //     $kegiatans2 = $this->db->from('kegiatan')
    //         ->where(['_active' => 1])
    //         ->get()
    //         ->result();

    //     foreach ($kegiatans1 as $kegiatan1) {

    //         if ($kegiatan1->neighbour) {
    //             $neighbour1 = explode(',', $kegiatan1->neighbour);
    //         } else {
    //             $neighbour1 = [];
    //         }

    //         foreach ($kegiatans2 as $kegiatan2) {
    //             if ($kegiatan2->id == $kegiatan1->id) {
    //                 continue;
    //             }

    //             if (in_array($kegiatan2->id, $neighbour1)) {
    //                 continue;
    //             }

    //             $distance = $this->_get_distance($kegiatan1->lat, $kegiatan1->lon, $kegiatan2->lat, $kegiatan2->lon);
    //             $distance = $distance * 1000;

    //             if ($distance < 500) {
    //                 $neighbour1[] = $kegiatan2->id;
    //             }
    //         }
    //         $this->db->where(['id' => $kegiatan1->id])->update('kegiatan', ['neighbour' => implode(',', $neighbour1)]);
    //     }
    // }

    // private function _get_distance($latitude1, $longitude1, $latitude2, $longitude2)
    // {
    //     $earth_radius = 6371;

    //     $dLat = deg2rad($latitude2 - $latitude1);
    //     $dLon = deg2rad($longitude2 - $longitude1);

    //     $a = sin($dLat / 2) * sin($dLat / 2) + cos(deg2rad($latitude1)) * cos(deg2rad($latitude2)) * sin($dLon / 2) * sin($dLon / 2);
    //     $c = 2 * asin(sqrt($a));
    //     $d = $earth_radius * $c;

    //     return $d;
    // }

}
