<?php

(defined('BASEPATH')) or exit('No direct script access allowed');

class Dashboard extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        // if ($this->session->jabatan != 'admin') {
        //     redirect('');
        // }
    }

    public function index()
    {
        redirect('Dashboard/input');
    }

    public function input()
    {

        if ($this->form_validation->run() == FALSE) {

            $data = [
                'title'     => 'Dashboard data aplikasi',
                'navbar'    => true,
                'header'    => true,
                'padding'   => false
            ];

            if (validation_errors()) {
                $this->session->set_flashdata('pelanggarBaruError', TRUE);
            }
            $this->template->admin('V_dashboard', $data);
        } else {
            redirect('Dashboard');
        }
    }

    public function grafik()
    {

        if ($this->form_validation->run() == FALSE) {

            $data = [
                'title'     => 'Dashboard data aplikasi',
                'navbar'    => true,
                'header'    => true,
                'padding'   => false
            ];

            if (validation_errors()) {
                $this->session->set_flashdata('pelanggarBaruError', TRUE);
            }
            $this->template->fullscreen('V_dashboard', $data);
        } else {
            redirect('Dashboard');
        }
    }

    public function edit_kegiatan()
    {
        $kegiatan_id = $this->input->post('kegiatan_id');

        $kegiatan_awal = $this->db->from('persebaran')
            ->where(['id' => $kegiatan_id])
            ->get()
            ->row();

        $kegiatan_awal = json_encode($kegiatan_awal);

        $update = [
            'kelurahan_id'  => $this->input->post('kelurahan'),
            'real_konfirmasi' => $this->input->post('real_konfirmasi'),
            'real_meninggal' => $this->input->post('real_meninggal'),
            'riwayat_perjalanan'        => $this->input->post('alamat'),
            'konfirmasi'        => $this->input->post('uraian'),
            'suspect'        => $this->input->post('alat_h'),
            'sembuh'        => $this->input->post('bahan_h'),
            'meninggal'        => $this->input->post('tenaga_h'),
            'tanggal'       =>$this->input->post('tanggal')
        ];

        $this->db->where(['id' => $kegiatan_id])
            ->update('persebaran', $update);
        redirect('Dashboard/input');
    }

    public function add_kegiatan()
    {
        $kel = $this->db->from('ref_kelurahan')
            ->where(['id' => $this->input->post('add_kelurahan')])
            ->get()
            ->row();
            $data = [
            'kelurahan_id'  => $this->input->post('add_kelurahan'),
            '_riwayat_perjalanan'        => $this->input->post('add_alamat'),
            '_konfirmasi'        => $this->input->post('add_uraian'),
            '_suspect'        => $this->input->post('add_alat_h'),
            '_sembuh'        => $this->input->post('add_bahan_h'),
            '_meninggal'        => $this->input->post('add_tenaga_h'),
            'real_konfirmasi'        => $this->input->post('add_real_konfirmasi'),
            'real_meninggal'        => $this->input->post('add_real_meninggal'),
            'tanggal'       => $this->input->post('add_tanggal'),
        ];
        $this->db->insert('persebaran', $data);
        redirect('Dashboard/input');
    }

    public function deactive()
    {
        $kegiatan_id = $this->uri->segment(3);
        $update = [
            '_isactive' => 0
        ];

        $this->db->where(['id' => $kegiatan_id])->update('persebaran',$update);

        redirect('Dashboard');
    }
}
