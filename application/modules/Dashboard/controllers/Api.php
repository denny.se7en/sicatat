<?php

(defined('BASEPATH')) or exit('No direct script access allowed');

class Api extends MY_Controller
{

    public function __construct()
    {
        // parent::__construct();
        // if ($this->session->jabatan != 'admin') {
        //     redirect('');
        // }

        $this->load->model(['dt_rekap', 'dt_neighbour', 'colour']);
    }

    public function get_ajax_ssd()
    {
        $kegiatans   = $this->dt_rekap->get_datatables_ssd();
        $data   = [];
        $no     = $this->input->post('start');

        foreach ($kegiatans as $index => $item) {
            $no++;
            $item->no = $no . '. ';
        }
        $kegiatans = array_values($kegiatans);
        $output = [
            "draw"              => $this->input->post('draw'),
            "recordsTotal"      => $this->dt_rekap->count_all_sbl(),
            "recordsFiltered"   => $this->dt_rekap->count_filtered_sbl(),
            "data"              => $kegiatans,
        ];
        echo json_encode($output);
    }

    public function get_peropd()
    {
        $data = $this->db->query("SELECT
        GROUP_CONCAT(nama_opd) AS labels,
        GROUP_CONCAT(datas) AS data,
        MAX(datas) AS maxdata
        FROM
        (
            select
                opd.nama as nama_opd,
                count(a.id) as datas
            from
                aplikasi a
                left join ref_opd opd on opd.id = a.ref_opd_id
            where
                a._active = 1
            group by
                opd.nama
            order by
                datas desc
        ) AS temp1")->row();
        $datasets     = [];
        $temp                   = new stdClass();
        $temp->label            = "Jumlah Aplikasi per OPD";
        $temp->data             = explode(',', $data->data);
        $temp->borderColor      = "green";
        $temp->backgroundColor  = $this->colour->backgroundColorGreen;

        array_push($datasets, $temp);

        $options = new stdClass();

        $response = [
            'labels'    => explode(',', $data->labels),
            'datasets'  => $datasets,
            'options'   => $options
        ];

        echo json_encode($response);
    }

    public function get_pertingkatkematangan()
    {
        $data = $this->db->query("SELECT
        GROUP_CONCAT(nama_tk) AS labels,
        GROUP_CONCAT(datas) AS data,
        MAX(datas) AS maxdata
        FROM
        (
            select
                tk.nama as nama_tk,
                coalesce(case when a._active =1 then COUNT(a.id) END,0) AS datas
            from
                ref_tingkatkematangan tk
                left join aplikasi a ON a.ref_tingkatkematangan_id = tk.id
            group BY
                tk.nama
            order by
                datas desc
        ) AS temp1")->row();
        $datasets     = [];
        $temp                   = new stdClass();
        $temp->label            = "Jumlah Aplikasi per Tingkat Kematangan";
        $temp->data             = explode(',', $data->data);
        $temp->borderColor      = "white";
        $temp->backgroundColor  = $this->colour->backgroundColorGreen;

        array_push($datasets, $temp);

        $options = new stdClass();

        $response = [
            'labels'    => explode(',', $data->labels),
            'datasets'  => $datasets,
            'options'   => $options
        ];

        echo json_encode($response);
    }

    public function get_perlokasi()
    {
        $data = $this->db->query("SELECT
        GROUP_CONCAT(nama_lokasi) AS labels,
        GROUP_CONCAT(datas) AS data,
        MAX(datas) AS maxdata
        FROM
        (
            select
                lok.nama as nama_lokasi,
                coalesce(case when a._active=1 then count(a.id) END,0) as datas
            from
            	ref_lokasi lok
            	LEFT JOIN aplikasi a ON a.ref_lokasi_id=lok.id
            group by
                lok.nama
            order by
                datas desc
        ) AS temp1")->row();
        $datasets     = [];
        $temp                   = new stdClass();
        $temp->label            = "Jumlah Aplikasi per Lokasi";
        $temp->data             = explode(',', $data->data);
        $temp->borderColor      = "green";
        $temp->backgroundColor  = $this->colour->backgroundColorGreen;

        array_push($datasets, $temp);

        $options = new stdClass();

        $response = [
            'labels'    => explode(',', $data->labels),
            'datasets'  => $datasets,
            'options'   => $options
        ];

        echo json_encode($response);
    }

    public function get_perdasarhukum()
    {
        $data = $this->db->query("SELECT
        GROUP_CONCAT(nama_dh) AS labels,
        GROUP_CONCAT(datas) AS data,
        MAX(datas) AS maxdata
        FROM
        (
            select
                dsh.nama as nama_dh,
                coalesce(case when a._active =1 then count(a.id) END,0) as datas
            from
                ref_dasarhukum dsh
                LEFT JOIN aplikasi a  on a.ref_dasarhukum_id = dsh.id
            group by
                dsh.nama
            order by
                datas desc
        ) AS temp1")->row();
        $datasets     = [];
        $temp                   = new stdClass();
        $temp->label            = "Jumlah Aplikasi per Dasar Hukum";
        $temp->data             = explode(',', $data->data);
        $temp->borderColor      = "green";
        $temp->backgroundColor  = $this->colour->backgroundColorGreen;

        array_push($datasets, $temp);

        $options = new stdClass();

        $response = [
            'labels'    => explode(',', $data->labels),
            'datasets'  => $datasets,
            'options'   => $options
        ];

        echo json_encode($response);
    }

    public function get_persistempengamanan()
    {
        $data = $this->db->query("SELECT
        GROUP_CONCAT(nama_sp) AS labels,
        GROUP_CONCAT(datas) AS data,
        MAX(datas) AS maxdata
        FROM
        (
            select
                sp.nama as nama_sp,
                coalesce(case when a._active = 1 then count(a.id) END ,0) as datas
            from
                ref_sistempengaman sp
                left JOIN aplikasi a on a.ref_sistempengamanan_id = sp.id
            group by
                sp.nama
            order by
                datas desc
        ) AS temp1")->row();
        $datasets     = [];
        $temp                   = new stdClass();
        $temp->label            = "Jumlah Aplikasi per Sistem Pengamanan";
        $temp->data             = explode(',', $data->data);
        $temp->borderColor      = "green";
        $temp->backgroundColor  = $this->colour->backgroundColorGreen;

        array_push($datasets, $temp);

        $options = new stdClass();

        $response = [
            'labels'    => explode(',', $data->labels),
            'datasets'  => $datasets,
            'options'   => $options
        ];

        echo json_encode($response);
    }

    public function get_perdokumen()
    {
        $data = $this->db->query("SELECT
        GROUP_CONCAT(nama_dok) AS labels,
        GROUP_CONCAT(datas) AS data,
        MAX(datas) AS maxdata
        FROM
        (
            select
                dok.nama as nama_dok,
                coalesce(case when a._active = 1 then COUNT(a.id) END ,0) as datas
            from
                ref_dokumen dok
                left join aplikasi a on a.ref_dokumen_id = dok.id
            group by
                dok.nama
            order by
                datas desc
        ) AS temp1")->row();
        $datasets     = [];
        $temp                   = new stdClass();
        $temp->label            = "Jumlah Aplikasi per Dokumen";
        $temp->data             = explode(',', $data->data);
        $temp->borderColor      = "green";
        $temp->backgroundColor  = $this->colour->backgroundColorGreen;

        array_push($datasets, $temp);

        $options = new stdClass();

        $response = [
            'labels'    => explode(',', $data->labels),
            'datasets'  => $datasets,
            'options'   => $options
        ];

        echo json_encode($response);
    }
}
