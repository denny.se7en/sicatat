<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Dt_Rekap extends CI_Model
{

    // start datatables
    var $column_order = array(null, 'kec.nama_kec', 'kel.nama_kel', 'p.riwayat_perjalanan', 'p.konfirmasi', 'p.meninggal', 'p.sembuh',null); //set column field database for datatable orderable
    var $column_search = array('k.nama_kec', 'kel.nama_kel', 'p.riwayat_perjalanan', 'p.konfirmasi', 'p.meninggal', 'p.sembuh',null); //set column field database for datatable searchable
    var $order = array('id' => 'desc'); // default order 

    function __construct()
    {
        parent::__construct();
    }
 
    function get_datatables()
    {
        $this->_get_datatables_query();
        if (@$_POST['length'] != -1)
            $this->db->limit(@$_POST['length'], @$_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    private function _get_datatables_query()
    {
        $this->db->select('kec.nama_kec, kel.nama_kel, p.*')
            ->from('persebaran p')
            ->join('ref_kelurahan kel', 'p.kelurahan_id = kel.id', 'left')
            ->join('ref_kecamatan kec', 'kel.no_kec = kec.id', 'left');
        if ($this->input->post('selected_opd')) {
            $this->db->where(['kel.id' => $this->input->post('selected_opd')]);
        }
        $this->db->order_by('tanggal','desc');
        $i = 0;
    }

    function get_datatables_ssd()
    {
        $this->_get_datatables_query_ssd();
        if (@$_POST['length'] != -1)
            $this->db->limit(@$_POST['length'], @$_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
    private function _get_datatables_query_ssd()
    {   

        $tgl_1 = date("Y-m-d", strtotime(str_replace('/', '-', $this->input->post('first_date'))));
        $tgl_2 = date("Y-m-d", strtotime(str_replace('/', '-', $this->input->post('second_date'))));
        $tgl_diff1 = date("Y-m-d",strtotime($this->input->post("first_date")." -7 day"));
        $tgl_diff2 = date("Y-m-d",strtotime($this->input->post("second_date")." -7 day"));

        $this->db->select("nama_kel,
        coalesce(sum(case when tanggal between '".$tgl_1."' and '".$tgl_2."' then _konfirmasi end),0) as konfirmasi,
        coalesce(sum(case when tanggal between '".$tgl_1."' and '".$tgl_2."' then _sembuh end),0) as sembuh,
        coalesce(sum(case when tanggal between '".$tgl_1."' and '".$tgl_2."' then _meninggal end),0) as meninggal,
        coalesce(sum(case when tanggal between '".$tgl_1."' and '".$tgl_2."' then _riwayat_perjalanan end),0) as riwayat_perjalanan,
        coalesce(sum(case when tanggal between '".$tgl_1."' and '".$tgl_2."' then _suspect end),0) as suspect,
        coalesce(coalesce(sum(case when tanggal between '".$tgl_1."' and '".$tgl_2."' then _konfirmasi end),0) / if(coalesce(sum(case when tanggal between '".$tgl_diff1."' and '".$tgl_diff2."' then _konfirmasi end),1)<1,1,coalesce(sum(case when tanggal between '".$tgl_diff1."' and '".$tgl_diff2."' then _konfirmasi end),1)),0) * 100 as kasus_baru")
        ->from('persebaran p')
        ->join('ref_kelurahan kel', 'p.kelurahan_id = kel.id', 'left');
        if ($this->input->post('selected_opd')) {
            $this->db->where(['kel.id' => $this->input->post('selected_opd')]);
        }
        $this->db->where(['_isactive' => 1, 'kel.id is not null' => null]);
        $this->db->group_by("kel.nama_kel");

        $i = 0;
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    function count_all()
    {
        $this->db->from('persebaran');
        return $this->db->count_all_results();
    }

    function count_filtered_sbl()
    {
        $this->_get_datatables_query_ssd();
        $query = $this->db->get();
        return $query->num_rows();
    }

    function count_all_sbl()
    {
        $this->db->select('kel.nama_kel, sum(_riwayat_perjalanan) as riwayat_perjalanan, sum(_suspect) as suspect, sum(_konfirmasi) as konfirmasi, sum(_sembuh) as sembuh, sum(_meninggal) as meninggal')
            ->from('persebaran p')
            ->join('ref_kelurahan kel', 'p.kelurahan_id = kel.id', 'left');
            // ->join('ref_kecamatan kec', 'kel.no_kec = kec.id', 'left');
        if ($this->input->post('selected_opd')) {
            $this->db->where(['kel.id' => $this->input->post('selected_opd')]);
        }
        $this->db->where(['_isactive' => 1, 'p.kelurahan_id is not null' => null]);
        $this->db->group_by("kel.nama_kel");
        return $this->db->count_all_results();
    }

}
