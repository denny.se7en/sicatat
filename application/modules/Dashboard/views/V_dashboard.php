<script src="<?php echo base_url() ?>/assets/chart/Chart.js"></script>
<!-- <div class="card-body"> -->

<!-- ROW 1 -->
<div class="d-flex flex-wrap">
    <!-- <div class="col-12 d-flex"> -->
    <div class="col-12 col-md-6 d-flex flex-column">

        <!-- <div class="col-3"> -->
        <!-- <div class="d-flex flex-column" style="border: 3px solid ' . $kec->color . ';border-radius:10px;font-weight:bold;">
                    <div class="d-flex align-items-center justify-content-center py-2" style="font-size:2rem;">96</div>
                    <div class="d-flex justify-content-center p-1">
                        <div class="d-flex justify-content-center flex-fill py-2 elevation-1" style="text-align:center;background-color:purple;color:white;border-radius:10px;">Aplikasi</div>
                    </div>
                </div> -->
        <!-- </div> -->
        <div class="d-flex card p-2" style="background-color:white;height:800px;">
            <div class="my-card-title">Aplikasi berdasarkan OPD</div>
            <div class="d-flex flex-fill" style="height:300px">
                <canvas id="grafik_peropd"></canvas>
            </div>
        </div>
    </div>
    <div class="col-12 col-md-6 d-flex flex-column">
        <div class="d-flex card p-2" style="height:800px;">
            <div class="my-card-title">Aplikasi berdasarkan Tingkat Kematangan</div>
            <div class="d-flex flex-fill" style="height:300px">
                <canvas id="grafik_pertingkatkematangan"></canvas>
            </div>
        </div>
    </div>
</div>

<!-- ROW 2 -->
<div class="d-flex flex-wrap">
    <div class="col-12 col-md-6 d-flex flex-column">
        <div class="d-flex card p-2" style="height:800px;">
            <div class="my-card-title">Aplikasi berdasarkan Lokasi</div>
            <div class="d-flex flex-fill" style="height:300px">
                <canvas id="grafik_perlokasi"></canvas>
            </div>
        </div>
    </div>
    <div class="col-12 col-md-6 d-flex flex-column">
        <div class="d-flex card p-2" style="height:800px;">
            <div class="my-card-title">Aplikasi berdasarkan Dasar Hukum</div>
            <div class="d-flex flex-fill" style="height:300px">
                <canvas id="grafik_perdasarhukum"></canvas>
            </div>
        </div>
    </div>
</div>


<div class="d-flex flex-wrap">
    <div class="col-12 col-md-6 d-flex flex-column">
        <div class="d-flex card p-2" style="min-height:800px;">
            <div class="my-card-title">Aplikasi berdasarkan Sistem Pengamanan</div>
            <div class="d-flex flex-fill" style="height:300px">
                <canvas id="grafik_persistempengamanan"></canvas>
            </div>
        </div>
    </div>
    <div class="col-12 col-md-6 d-flex flex-column">
        <div class="d-flex card p-2" style="min-height:800px;">
            <div class="my-card-title">Aplikasi berdasarkan Kelengkapan Dokumen</div>
            <div class="d-flex flex-fill" style="height:300px">
                <canvas id="grafik_perdokumen"></canvas>
            </div>
        </div>
    </div>
</div>


<script>
    let id = undefined

    $(function() {
        grafik_peropd()
        grafik_pertingkatkematangan()
        grafik_perlokasi()
        grafik_perdasarhukum()
        grafik_persistempengamanan()
        grafik_perdokumen()
    })

    $('#btn-filter').click(function() { //button filter event click

    })

    function grafik_peropd() {
        $.ajax({
            type: "GET",
            url: '<?= base_url('Dashboard/Api/get_peropd') ?>',
            success: function(response) {
                horizontalBarDiagram('grafik_peropd', response, 1000)
            },
            dataType: 'JSON'
        });
    }

    function grafik_pertingkatkematangan() {
        $.ajax({
            type: "GET",
            url: '<?= base_url('Dashboard/Api/get_pertingkatkematangan') ?>',
            success: function(response) {
                barDiagram('grafik_pertingkatkematangan', response, 400)
            },
            dataType: 'JSON'
        });
    }

    function grafik_perlokasi() {
        $.ajax({
            type: "GET",
            url: '<?= base_url('Dashboard/Api/get_perlokasi') ?>',
            success: function(response) {
                doughnutDiagram('grafik_perlokasi', response, 2)
            },
            dataType: 'JSON'
        });
    }

    function grafik_perdasarhukum() {
        $.ajax({
            type: "GET",
            url: '<?= base_url('Dashboard/Api/get_perdasarhukum') ?>',
            success: function(response) {
                barDiagram('grafik_perdasarhukum', response, 400)
            },
            dataType: 'JSON'
        });
    }

    function grafik_persistempengamanan() {
        $.ajax({
            type: "GET",
            url: '<?= base_url('Dashboard/Api/get_persistempengamanan') ?>',
            success: function(response) {
                horizontalBarDiagram('grafik_persistempengamanan', response, 1000)
            },
            dataType: 'JSON'
        });
    }

    function grafik_perdokumen() {
        $.ajax({
            type: "GET",
            url: '<?= base_url('Dashboard/Api/get_perdokumen') ?>',
            success: function(response) {
                horizontalBarDiagram('grafik_perdokumen', response, 1000)
            },
            dataType: 'JSON'
        });
    }

</script>