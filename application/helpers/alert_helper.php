<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('alertMsg')) {
    function alertMsg()
    {
        $CI = get_instance();
        return $CI->session->flashdata('alertMsg');
    }
}

if (!function_exists('setAlert')) {
    function setAlert($type, $message)
    {
        $CI = get_instance();

        $CI->session->set_flashdata(
            'alertMsg',
            '<div class="alert alert-' . $type . '" role="alert">
                ' . ucfirst($message) . '
            </div>'
        );
    }
}
