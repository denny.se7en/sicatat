-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               10.4.17-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for sicatat
CREATE DATABASE IF NOT EXISTS `u1543793_sicatat` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `u1543793_sicatat`;

-- Dumping structure for table sicatat.menu
CREATE TABLE IF NOT EXISTS `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent` int(11) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `code` varchar(245) CHARACTER SET latin1 DEFAULT NULL,
  `name` varchar(245) CHARACTER SET latin1 DEFAULT NULL,
  `url` varchar(245) CHARACTER SET latin1 DEFAULT NULL,
  `icon_type` varchar(245) CHARACTER SET latin1 DEFAULT NULL,
  `icon_keyword` varchar(245) CHARACTER SET latin1 DEFAULT NULL,
  `badge_type` varchar(245) CHARACTER SET latin1 DEFAULT NULL,
  `badge_keyword` varchar(245) CHARACTER SET latin1 DEFAULT NULL,
  `_active` tinyint(4) NOT NULL DEFAULT 1,
  `_created_at` timestamp NULL DEFAULT current_timestamp(),
  `_created_by` int(11) DEFAULT NULL,
  `_updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `_updated_by` int(11) DEFAULT NULL,
  `_deleted_at` timestamp NULL DEFAULT NULL,
  `_deleted_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table sicatat.menu: ~4 rows (approximately)
/*!40000 ALTER TABLE `menu` DISABLE KEYS */;
INSERT INTO `menu` (`id`, `parent`, `order`, `code`, `name`, `url`, `icon_type`, `icon_keyword`, `badge_type`, `badge_keyword`, `_active`, `_created_at`, `_created_by`, `_updated_at`, `_updated_by`, `_deleted_at`, `_deleted_by`) VALUES
	(1, 0, 1, '001', NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-02-01 13:57:37', NULL, '2021-02-01 13:57:44', NULL, NULL, NULL),
	(2, 1, 2, '001:002', 'Daftar list aplikasi', 'aplikasi', 'fa', 'fas fa-list', NULL, NULL, 1, '2021-02-01 13:58:09', NULL, '2021-09-28 15:02:23', NULL, NULL, NULL),
	(3, 1, 1, '001:001', 'Dashboard', 'dashboard', 'fa', 'fas fa-tachometer-alt', NULL, NULL, 1, '2021-09-28 14:49:33', NULL, '2021-09-28 14:50:54', NULL, NULL, NULL),
	(4, 1, 3, '001:003', 'Report', 'report', 'fa', 'fas fa-file-alt', NULL, NULL, 1, '2021-09-28 14:52:35', NULL, '2021-09-28 14:52:35', NULL, NULL, NULL);
/*!40000 ALTER TABLE `menu` ENABLE KEYS */;

-- Dumping structure for table sicatat.pengguna
CREATE TABLE IF NOT EXISTS `pengguna` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) DEFAULT NULL,
  `reg_id` int(11) DEFAULT NULL,
  `username` varchar(255) CHARACTER SET latin1 NOT NULL,
  `password` varchar(255) CHARACTER SET latin1 NOT NULL,
  `nama` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `no_hp` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `_active` tinyint(4) NOT NULL DEFAULT 1,
  `_created_at` timestamp NULL DEFAULT current_timestamp(),
  `_created_by` int(11) DEFAULT NULL,
  `_updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `_updated_by` int(11) DEFAULT NULL,
  `_deleted_at` timestamp NULL DEFAULT NULL,
  `_deleted_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table sicatat.pengguna: ~2 rows (approximately)
/*!40000 ALTER TABLE `pengguna` DISABLE KEYS */;
INSERT INTO `pengguna` (`id`, `role_id`, `reg_id`, `username`, `password`, `nama`, `no_hp`, `_active`, `_created_at`, `_created_by`, `_updated_at`, `_updated_by`, `_deleted_at`, `_deleted_by`) VALUES
	(1, 1, 1, 'admin', '$2y$11$T1kwFvfgOKMlQ7lzLxOkMeXCnB9/k.KUSmoFQKlWO42k9blkm08iK', 'admin', '085854445232', 1, '2021-02-01 13:55:26', NULL, '2021-02-02 10:59:04', NULL, NULL, NULL),
	(52, 1, 1, 'commandcenter', '$2y$11$T1kwFvfgOKMlQ7lzLxOkMeXCnB9/k.KUSmoFQKlWO42k9blkm08iK', 'commandcenter', NULL, 1, '2021-02-19 13:08:32', NULL, '2021-02-19 13:10:59', NULL, NULL, NULL);
/*!40000 ALTER TABLE `pengguna` ENABLE KEYS */;

-- Dumping structure for table sicatat.ref_dokumen
CREATE TABLE IF NOT EXISTS `ref_dokumen` (
  `ref_dokumen_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(150) DEFAULT NULL,
  `skor` tinyint(4) DEFAULT NULL,
  `_active` tinyint(4) NOT NULL DEFAULT 1,
  `_created_at` timestamp NULL DEFAULT current_timestamp(),
  `_created_by` int(11) DEFAULT NULL,
  `_updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `_updated_by` int(11) DEFAULT NULL,
  `_deleted_at` timestamp NULL DEFAULT NULL,
  `_deleted_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`ref_dokumen_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table sicatat.ref_dokumen: ~4 rows (approximately)
/*!40000 ALTER TABLE `ref_dokumen` DISABLE KEYS */;
INSERT INTO `ref_dokumen` (`ref_dokumen_id`, `nama`, `skor`, `_active`, `_created_at`, `_created_by`, `_updated_at`, `_updated_by`, `_deleted_at`, `_deleted_by`) VALUES
	(1, 'Tidak ada petunjuk penggunaan aplikasi dan dokumentasi teknis', 1, 1, '2021-09-28 15:31:29', NULL, '2021-09-28 15:31:29', NULL, NULL, NULL),
	(2, 'Terdapat petunjuk penggunaan aplikasi', 3, 1, '2021-09-28 15:31:48', NULL, '2021-09-28 15:31:48', NULL, NULL, NULL),
	(3, 'Terdapat dokumen teknis', 4, 1, '2021-09-28 15:32:08', NULL, '2021-09-28 15:32:08', NULL, NULL, NULL),
	(4, 'Terdapat petunjuk penggunaan aplikasi dan dokumentasi teknis', 5, 1, '2021-09-28 15:32:31', NULL, '2021-09-28 15:32:31', NULL, NULL, NULL);
/*!40000 ALTER TABLE `ref_dokumen` ENABLE KEYS */;

-- Dumping structure for table sicatat.ref_evaluasi
CREATE TABLE IF NOT EXISTS `ref_evaluasi` (
  `ref_evaluasi_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) DEFAULT NULL,
  `skor` tinyint(4) DEFAULT NULL,
  `_active` tinyint(4) NOT NULL DEFAULT 1,
  `_created_at` timestamp NULL DEFAULT current_timestamp(),
  `_created_by` int(11) DEFAULT NULL,
  `_updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `_updated_by` int(11) DEFAULT NULL,
  `_deleted_at` timestamp NULL DEFAULT NULL,
  `_deleted_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`ref_evaluasi_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table sicatat.ref_evaluasi: ~3 rows (approximately)
/*!40000 ALTER TABLE `ref_evaluasi` DISABLE KEYS */;
INSERT INTO `ref_evaluasi` (`ref_evaluasi_id`, `nama`, `skor`, `_active`, `_created_at`, `_created_by`, `_updated_at`, `_updated_by`, `_deleted_at`, `_deleted_by`) VALUES
	(1, 'Jarang / TIdak pernah dilakukan evaluasi layanan s', 1, 1, '2021-09-28 15:21:30', NULL, '2021-09-28 15:21:30', NULL, NULL, NULL),
	(2, 'Dilakukan evaluasi layanan secara terencana oleh m', 3, 1, '2021-09-28 15:22:24', NULL, '2021-09-28 15:22:24', NULL, NULL, NULL),
	(3, 'DIlakukan evaluasi layanan secara terencana oleh m', 5, 1, '2021-09-28 15:23:07', NULL, '2021-09-28 15:23:07', NULL, NULL, NULL);
/*!40000 ALTER TABLE `ref_evaluasi` ENABLE KEYS */;

-- Dumping structure for table sicatat.ref_frekuensipemeliharaan
CREATE TABLE IF NOT EXISTS `ref_frekuensipemeliharaan` (
  `ref_frekuensipemeliharaan_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(150) DEFAULT NULL,
  `skor` tinyint(4) DEFAULT NULL,
  `_active` tinyint(4) NOT NULL DEFAULT 1,
  `_created_at` timestamp NULL DEFAULT current_timestamp(),
  `_created_by` int(11) DEFAULT NULL,
  `_updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `_updated_by` int(11) DEFAULT NULL,
  `_deleted_at` timestamp NULL DEFAULT NULL,
  `_deleted_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`ref_frekuensipemeliharaan_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table sicatat.ref_frekuensipemeliharaan: ~3 rows (approximately)
/*!40000 ALTER TABLE `ref_frekuensipemeliharaan` DISABLE KEYS */;
INSERT INTO `ref_frekuensipemeliharaan` (`ref_frekuensipemeliharaan_id`, `nama`, `skor`, `_active`, `_created_at`, `_created_by`, `_updated_at`, `_updated_by`, `_deleted_at`, `_deleted_by`) VALUES
	(1, 'Jarang / Tidak pernah terjadi ada pemeliharaan yang terencana', 1, 1, '2021-09-28 15:17:58', NULL, '2021-09-28 15:17:58', NULL, NULL, NULL),
	(2, 'Pemeliharaan 1 kali dalam setahun', 3, 1, '2021-09-28 15:18:23', NULL, '2021-09-28 15:18:23', NULL, NULL, NULL),
	(3, 'Pemeliharaan lebih dari 1 kali dalam setahun (dalam bulan)', 5, 1, '2021-09-28 15:18:50', NULL, '2021-09-28 15:18:50', NULL, NULL, NULL);
/*!40000 ALTER TABLE `ref_frekuensipemeliharaan` ENABLE KEYS */;

-- Dumping structure for table sicatat.ref_jenisaplikasi
CREATE TABLE IF NOT EXISTS `ref_jenisaplikasi` (
  `ref_jenisaplikasi_id` int(10) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) DEFAULT NULL,
  `skor` tinyint(4) DEFAULT NULL,
  `_active` tinyint(4) DEFAULT 1,
  `_created_at` timestamp NULL DEFAULT current_timestamp(),
  `_created_by` int(11) DEFAULT NULL,
  `_updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `_updated_by` int(11) DEFAULT NULL,
  `_deleted_at` timestamp NULL DEFAULT NULL,
  `_deleted_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`ref_jenisaplikasi_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table sicatat.ref_jenisaplikasi: ~2 rows (approximately)
/*!40000 ALTER TABLE `ref_jenisaplikasi` DISABLE KEYS */;
INSERT INTO `ref_jenisaplikasi` (`ref_jenisaplikasi_id`, `nama`, `skor`, `_active`, `_created_at`, `_created_by`, `_updated_at`, `_updated_by`, `_deleted_at`, `_deleted_by`) VALUES
	(1, 'Khusu', 3, 1, '2021-09-22 21:15:16', 0, '2021-09-22 21:15:32', NULL, NULL, NULL),
	(2, 'Umum', 5, 1, '2021-09-22 21:16:06', 0, '2021-09-22 21:16:11', NULL, NULL, NULL);
/*!40000 ALTER TABLE `ref_jenisaplikasi` ENABLE KEYS */;

-- Dumping structure for table sicatat.ref_sistempengaman
CREATE TABLE IF NOT EXISTS `ref_sistempengaman` (
  `ref_sistempengaman_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) DEFAULT NULL,
  `skor` tinyint(4) DEFAULT NULL,
  `_active` tinyint(4) NOT NULL DEFAULT 1,
  `_created_at` timestamp NULL DEFAULT current_timestamp(),
  `_created_by` int(11) DEFAULT NULL,
  `_updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `_updated_by` int(11) DEFAULT NULL,
  `_deleted_at` timestamp NULL DEFAULT NULL,
  `_deleted_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`ref_sistempengaman_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table sicatat.ref_sistempengaman: ~3 rows (approximately)
/*!40000 ALTER TABLE `ref_sistempengaman` DISABLE KEYS */;
INSERT INTO `ref_sistempengaman` (`ref_sistempengaman_id`, `nama`, `skor`, `_active`, `_created_at`, `_created_by`, `_updated_at`, `_updated_by`, `_deleted_at`, `_deleted_by`) VALUES
	(1, 'Tidak terdapat sistem pengaman pada aplikasi', 1, 1, '2021-09-22 21:34:23', NULL, '2021-09-22 21:34:23', NULL, NULL, NULL),
	(2, 'Terdapat sistem pengaman data dengan menggunakan sertifikat digital yang disediakan oleh dinkominfo', 3, 1, '2021-09-22 21:35:25', NULL, '2021-09-22 21:35:25', NULL, NULL, NULL),
	(3, 'Terdapat sistem pengaman data dengan menggunakan sertifikat digital yang disediakan oleh dinkominfo dan sistem pengaman tingkat lanjut lainnya yang disesuaikan dengan kebutuhan', 5, 1, '2021-09-22 21:36:17', NULL, '2021-09-22 21:36:17', NULL, NULL, NULL);
/*!40000 ALTER TABLE `ref_sistempengaman` ENABLE KEYS */;

-- Dumping structure for table sicatat.ref_tenagateknis
CREATE TABLE IF NOT EXISTS `ref_tenagateknis` (
  `ref_tenagateknis_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) DEFAULT NULL,
  `skor` tinyint(4) DEFAULT NULL,
  `_active` tinyint(4) NOT NULL DEFAULT 1,
  `_created_at` timestamp NULL DEFAULT current_timestamp(),
  `_created_by` int(11) DEFAULT NULL,
  `_updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `_updated_by` int(11) DEFAULT NULL,
  `_deleted_at` timestamp NULL DEFAULT NULL,
  `_deleted_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`ref_tenagateknis_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table sicatat.ref_tenagateknis: ~4 rows (approximately)
/*!40000 ALTER TABLE `ref_tenagateknis` DISABLE KEYS */;
INSERT INTO `ref_tenagateknis` (`ref_tenagateknis_id`, `nama`, `skor`, `_active`, `_created_at`, `_created_by`, `_updated_at`, `_updated_by`, `_deleted_at`, `_deleted_by`) VALUES
	(1, 'Tidak ada', 1, 1, '2021-09-22 21:43:19', NULL, '2021-09-22 21:43:19', NULL, NULL, NULL),
	(2, 'Satu orang dari internal', 2, 1, '2021-09-22 21:43:39', NULL, '2021-09-22 21:43:39', NULL, NULL, NULL),
	(3, 'Satu orang dari penyedia', 3, 1, '2021-09-22 21:43:54', NULL, '2021-09-22 21:43:54', NULL, NULL, NULL),
	(4, 'Lebih dari satu orang dari internal', 4, 1, '2021-09-22 21:44:13', NULL, '2021-09-22 21:44:13', NULL, NULL, NULL),
	(5, 'Lebih dari satu orang gabungan dari internal dan p', 5, 1, '2021-09-22 21:44:37', NULL, '2021-09-22 21:44:37', NULL, NULL, NULL);
/*!40000 ALTER TABLE `ref_tenagateknis` ENABLE KEYS */;

-- Dumping structure for table sicatat.ref_tingkatkematangan
CREATE TABLE IF NOT EXISTS `ref_tingkatkematangan` (
  `ref_tingkatkematangan_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) DEFAULT NULL,
  `indikator` varchar(255) DEFAULT NULL,
  `skor` tinyint(4) DEFAULT NULL,
  `_active` tinyint(4) DEFAULT 1,
  `_created_at` timestamp NULL DEFAULT current_timestamp(),
  `_created_by` int(11) DEFAULT NULL,
  `_updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `_updated_by` int(11) DEFAULT NULL,
  `_deleted_at` timestamp NULL DEFAULT NULL,
  `_deleted_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`ref_tingkatkematangan_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table sicatat.ref_tingkatkematangan: ~4 rows (approximately)
/*!40000 ALTER TABLE `ref_tingkatkematangan` DISABLE KEYS */;
INSERT INTO `ref_tingkatkematangan` (`ref_tingkatkematangan_id`, `nama`, `indikator`, `skor`, `_active`, `_created_at`, `_created_by`, `_updated_at`, `_updated_by`, `_deleted_at`, `_deleted_by`) VALUES
	(1, '1 - Informasi', 'Layanan SPBE diberikan dalam bentuk informasi satu arah', 1, 1, '2021-09-22 21:18:53', NULL, '2021-09-22 21:18:53', NULL, NULL, NULL),
	(2, '2 - Interaksi', 'Kriteria Tingkat informasi telah terpenuhi. Layanan SPBE diberikan dalam bentuk interaksi dua arah', 2, 1, '2021-09-22 21:19:58', NULL, '2021-09-22 21:19:58', NULL, NULL, NULL),
	(3, '3 - Transaksi', 'Kriteria tingkat interaksi telah terpenuhi. layanan SPBE diberikan melalui satu kesatuan transaksi operasi dengan menggunakan beberapa sumber daya SPBE', 3, 1, '2021-09-22 21:21:29', NULL, '2021-09-22 21:21:29', NULL, NULL, NULL),
	(4, '4 - Kolaborasi', 'Kriteria tingkat transaksi telah terpenuhi. Layanan SPBE diberikan melalui integrasi/kolaborasi dengan layanan SPBE lain.', 4, 1, '2021-09-22 21:22:31', NULL, '2021-09-22 21:22:31', NULL, NULL, NULL),
	(5, '5 - Optimum', 'Kriteria tingkat kolaborasi telah terpenuhi. Layanan SPBE telah dilakukan perbaikan dan peningkatan kualitas menyesuaikan perubahan kebutuhan di lingkungan internal dan eksternal.', 5, 1, '2021-09-22 21:23:45', NULL, '2021-09-22 21:23:45', NULL, NULL, NULL);
/*!40000 ALTER TABLE `ref_tingkatkematangan` ENABLE KEYS */;

-- Dumping structure for table sicatat.role
CREATE TABLE IF NOT EXISTS `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `_active` tinyint(4) NOT NULL DEFAULT 1,
  `_created_at` timestamp NULL DEFAULT current_timestamp(),
  `_created_by` int(11) DEFAULT NULL,
  `_updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `_updated_by` int(11) DEFAULT NULL,
  `_deleted_at` timestamp NULL DEFAULT NULL,
  `_deleted_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table sicatat.role: ~0 rows (approximately)
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` (`id`, `name`, `_active`, `_created_at`, `_created_by`, `_updated_at`, `_updated_by`, `_deleted_at`, `_deleted_by`) VALUES
	(1, 'admin', 1, '2021-02-01 13:56:08', NULL, '2021-02-01 13:56:08', NULL, NULL, NULL);
/*!40000 ALTER TABLE `role` ENABLE KEYS */;

-- Dumping structure for table sicatat.role_x_menu
CREATE TABLE IF NOT EXISTS `role_x_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `_active` tinyint(4) NOT NULL DEFAULT 1,
  `_created_at` timestamp NULL DEFAULT current_timestamp(),
  `_created_by` int(11) DEFAULT NULL,
  `_updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `_updated_by` int(11) DEFAULT NULL,
  `_deleted_at` timestamp NULL DEFAULT NULL,
  `_deleted_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table sicatat.role_x_menu: ~5 rows (approximately)
/*!40000 ALTER TABLE `role_x_menu` DISABLE KEYS */;
INSERT INTO `role_x_menu` (`id`, `role_id`, `menu_id`, `_active`, `_created_at`, `_created_by`, `_updated_at`, `_updated_by`, `_deleted_at`, `_deleted_by`) VALUES
	(1, 1, 4, 1, '2021-02-01 14:00:13', NULL, '2021-02-05 16:08:42', NULL, NULL, NULL),
	(2, 2, 1, 0, '2021-02-01 20:19:36', NULL, '2021-02-05 07:32:17', NULL, NULL, NULL),
	(4, 2, 2, 0, '2021-02-01 21:37:32', NULL, '2021-02-05 07:32:17', NULL, NULL, NULL),
	(5, 1, 5, 1, '2021-02-02 08:14:31', NULL, '2021-02-05 09:02:29', NULL, NULL, NULL),
	(6, 1, 6, 1, '2021-02-08 08:43:18', NULL, '2021-02-08 08:43:19', NULL, NULL, NULL);
/*!40000 ALTER TABLE `role_x_menu` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
