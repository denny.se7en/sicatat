-- --------------------------------------------------------
-- Host:                         45.143.81.1
-- Server version:               10.3.30-MariaDB-cll-lve - MariaDB Server
-- Server OS:                    Linux
-- HeidiSQL Version:             11.3.0.6345
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for u1543793_sicatat
CREATE DATABASE IF NOT EXISTS `sicatatk_sicatat` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `sicatatk_sicatat`;

-- Dumping structure for table u1543793_sicatat.aplikasi
CREATE TABLE IF NOT EXISTS `aplikasi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `no_asset` varchar(50) DEFAULT NULL,
  `hostname` varchar(100) DEFAULT NULL,
  `ipaddress` varchar(50) DEFAULT NULL,
  `ref_opd_id` int(11) DEFAULT NULL,
  `ref_lokasi_id` int(11) DEFAULT NULL,
  `nm_internal` varchar(200) DEFAULT NULL,
  `nm_eksternal` varchar(200) DEFAULT NULL,
  `deskripsi` text DEFAULT NULL,
  `sertf_kelayakan_id` tinyint(4) DEFAULT NULL,
  `ket_sertf` varchar(200) DEFAULT NULL,
  `prkt_utama` text DEFAULT NULL,
  `info_data_center` text DEFAULT NULL,
  `prkt_khusus` text DEFAULT NULL,
  `os` text DEFAULT NULL,
  `web_server` text DEFAULT NULL,
  `basis_data` text DEFAULT NULL,
  `bhs_pemrograman` text DEFAULT NULL,
  `framework` text DEFAULT NULL,
  `ref_sasaran_id` int(11) DEFAULT NULL,
  `ref_dasarhukum_id` int(11) DEFAULT NULL,
  `ref_kategoriakses_id` int(11) DEFAULT NULL,
  `layanan_publik` int(11) DEFAULT NULL,
  `ref_frekuensipemeliharaan_id` int(11) DEFAULT NULL,
  `ref_evaluasi_id` int(11) DEFAULT NULL,
  `ref_dokumen_id` int(11) DEFAULT NULL,
  `ref_pj_id` int(11) DEFAULT NULL,
  `ref_pjteknis_id` int(11) DEFAULT NULL,
  `ref_jenisaplikasi_id` int(11) DEFAULT NULL,
  `ref_tingkatkematangan_id` int(11) DEFAULT NULL,
  `ref_sistempengamanan_id` int(11) DEFAULT NULL,
  `ref_tenagateknis_id` int(11) DEFAULT NULL,
  `tgl_ba` date DEFAULT NULL,
  `tempat_ba` varchar(200) DEFAULT NULL,
  `maintenance_ba` int(11) DEFAULT NULL,
  `pengembangan_ba` int(11) DEFAULT NULL,
  `server_ba` int(11) DEFAULT NULL,
  `ssl_ba` int(11) DEFAULT NULL,
  `backup_ba` int(11) DEFAULT NULL,
  `restore_ba` int(11) DEFAULT NULL,
  `_active` tinyint(4) NOT NULL DEFAULT 1,
  `_created_at` timestamp NULL DEFAULT current_timestamp(),
  `_created_by` int(11) DEFAULT NULL,
  `_updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `_updated_by` int(11) DEFAULT NULL,
  `_deleted_at` timestamp NULL DEFAULT NULL,
  `_deleted_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=97 DEFAULT CHARSET=latin1;

-- Dumping data for table u1543793_sicatat.aplikasi: ~96 rows (approximately)
/*!40000 ALTER TABLE `aplikasi` DISABLE KEYS */;
INSERT INTO `aplikasi` (`id`, `no_asset`, `hostname`, `ipaddress`, `ref_opd_id`, `ref_lokasi_id`, `nm_internal`, `nm_eksternal`, `deskripsi`, `sertf_kelayakan_id`, `ket_sertf`, `prkt_utama`, `info_data_center`, `prkt_khusus`, `os`, `web_server`, `basis_data`, `bhs_pemrograman`, `framework`, `ref_sasaran_id`, `ref_dasarhukum_id`, `ref_kategoriakses_id`, `layanan_publik`, `ref_frekuensipemeliharaan_id`, `ref_evaluasi_id`, `ref_dokumen_id`, `ref_pj_id`, `ref_pjteknis_id`, `ref_jenisaplikasi_id`, `ref_tingkatkematangan_id`, `ref_sistempengamanan_id`, `ref_tenagateknis_id`, `tgl_ba`, `tempat_ba`, `maintenance_ba`, `pengembangan_ba`, `server_ba`, `ssl_ba`, `backup_ba`, `restore_ba`, `_active`, `_created_at`, `_created_by`, `_updated_at`, `_updated_by`, `_deleted_at`, `_deleted_by`) VALUES
	(1, 'SW-KMINFO-002', 'barometer.diskominfo.kedirikota.go.id', '103.78.106.20', 93, 2, 'Radio Baromater FM', 'Radio Barometer FM', 'Radio milik pemerintahan Kota Kediri yang dikelola Diskominfo Kota Kediri', NULL, '', 'Server', NULL, 'Tidak Ada', 'Linux Debian 4.9', 'Apache 2.4.25', '-', '-', 'Native HTML', 4, 4, 2, 1, 1, 2, 1, 3, 3, NULL, NULL, NULL, NULL, '2021-10-01', 'Ruang Rapat Diskominfo', 91, 92, 93, 94, 95, 96, 1, '2021-10-11 03:52:41', NULL, '2021-10-25 01:46:33', NULL, NULL, NULL),
	(2, 'SW-KMINFO-001', 'corona.kedirikota.go.id/bantuancorona', '103.78.106.53', 93, 2, 'Aplikasi Bantuan Corona', 'Aplikasi Bantuan Corona', 'SI untuk data penyaluran bantuan-bantuan terdampak covid19 di Kota Kediri', NULL, '', '', NULL, '', '', '', '', '', '', NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-10-12 08:00:50', NULL, '2021-10-18 03:39:02', NULL, NULL, NULL),
	(3, 'SW-KMINFO-003', 'cc.kedirikota.go.id.', '103.78.106.54', 93, 2, 'Website media center kota kediri', 'Website media center kota kediri', 'Website kegawat daruratan kota kediri', 1, '', 'Server ', 'Dinas Kominfo', 'Tidak Ada', 'Windows Server 2012', 'Apache 2.4.41', 'MySQL Mariadb 15.1', 'php 7.3', 'CI 2.2.6', 4, 4, 2, 1, 2, 3, 1, 3, 3, 1, 4, 2, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-10-20 12:23:17', NULL, '2021-10-20 12:23:17', NULL, NULL, NULL),
	(4, 'SW-KMINFO-004', 'cekbansos.kedirikota.go.id.', '103.78.106.54', 93, 2, 'Aplikasi cek bansos', 'Aplikasi cek bansos', 'Sistem Informasi untuk pengecekan penerima bantuan sosial berdasarkan NIK yang terintegrasi dengan datawarehouse dispendukcapil', 1, '', 'Server ', 'Dinas Kominfo', 'Tidak Ada', 'Windows Server 2012', 'Apache 2.4.41', 'MySQL Mariadb 15.1', 'php 7.3', 'CI 3.1.11', 4, 4, 2, 1, 2, 3, 1, 3, 3, 1, 4, 2, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-10-20 12:23:17', NULL, '2021-10-20 12:23:17', NULL, NULL, NULL),
	(5, 'SW-KMINFO-005', 'corona.kedirikota.go.id.', '103.78.106.53', 93, 2, 'Website corona', 'Website corona', 'Website informasi seputar update corona di kota kediri', 2, '', 'Server ', 'Dinas Kominfo', 'Tidak Ada', 'Windows 7 Ultimate', 'Apache 2.4.23', 'MySQL 5.7.32', 'php 7.0.13', 'wordpress 5.5.3', 4, 1, 2, 1, 2, 2, 1, 3, 3, 1, 1, 2, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-10-20 12:23:17', NULL, '2021-10-20 12:23:17', NULL, NULL, NULL),
	(6, 'SW-KMINFO-006', 'diskominfo.kedirikota.go.id.', '124.40.255.186', 93, 1, 'Website dinas kominfo kota kediri', 'Website dinas kominfo kota kediri', 'Webiste Informasi dinas kominfo kota kediri', 2, '', 'Server ', 'Diluar Data center diskominfo', 'Tidak Ada', 'Cloud Linux (Centos 7)', 'Apache 2.4.46', 'MySQL 5.7.32', 'php 5.5', 'CI 3.1.5', 4, 4, 2, 1, 3, 2, 1, 3, 3, 1, 2, 2, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-10-20 12:23:17', NULL, '2021-10-20 12:23:17', NULL, NULL, NULL),
	(7, 'SW-KMINFO-007', 'esuket.kedirikota.go.id.', '103.78.106.11', 93, 2, 'Aplikasi e-suket', 'Aplikasi e-suket', 'Sistem Informasi untuk pembuatan surat keterangan layanan kependudukan di kota kediri', 1, '', 'Server ', 'Dinas Kominfo', 'Tidak Ada', 'Windows 10 Pro', 'Apache 2.4.37', 'MySQL Mariadb 15.1', 'php 7.3', 'CI 3.1.6', 4, 4, 2, 1, 3, 2, 4, 3, 3, 1, 4, 2, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-10-20 12:23:17', NULL, '2021-10-20 12:23:17', NULL, NULL, NULL),
	(8, 'SW-KMINFO-008', 'esurat.kedirikota.go.id.', '103.78.106.18', 93, 2, 'Aplikasi e-surat', 'Aplikasi e-surat', 'Aplikasi pengelolaan surat menyurat secara elektronik untuk internal birokrasi kota kediri', 2, '', 'Server ', 'Dinas Kominfo', 'Satu (Scanner)', 'Windows Server 2012', 'Apache 2.4.41', 'MySQL Mariadb 15.1', 'php 5.6', 'CI 2.2.6', 2, 4, 2, 1, 3, 2, 4, 3, 3, 1, 3, 2, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-10-20 12:23:17', NULL, '2021-10-20 12:23:17', NULL, NULL, NULL),
	(9, 'SW-KMINFO-009', 'ganet.kedirikota.go.id.', '103.78.106.21', 93, 2, 'siganet app', 'siganet app', 'Sistem Informasi gangguan jaringan kota kediri', 2, '', 'Server ', 'Dinas Kominfo', 'Tidak Ada', 'Windows Server 2012', 'Apache 2.4.29', 'MySQL Mariadb 15.1', 'php 5.5', 'CI 3.1.8', 2, 4, 2, 2, 2, 2, 1, 3, 3, 1, 3, 2, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-10-20 12:23:17', NULL, '2021-10-20 12:23:17', NULL, NULL, NULL),
	(10, 'SW-KMINFO-010', 'infocukai.kedirikota.go.id.', '124.40.255.186', 93, 1, 'website info cukai', 'website info cukai', 'Website resmi tentang dana cukai kota kediri', 2, '', 'Server ', 'Diluar Data center diskominfo', 'Tidak Ada', 'Cloud Linux (Centos 7)', 'Apache 2.4.46', 'MySQL 5.7.32', 'php 5.5', 'CI 3.0.0', 4, 1, 2, 1, 2, 2, 1, 3, 3, 1, 1, 2, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-10-20 12:23:17', NULL, '2021-10-20 12:23:17', NULL, NULL, NULL),
	(11, 'SW-KMINFO-011', 'observasicorona.kedirikota.go.id.', '124.40.255.186', 93, 1, 'Aplikasi observasi korona', 'Aplikasi observasi korona', 'Sistem Informasi untuk pendataan dan pemantauan warga kota kediri terkait pandemi covid yang mudik', 1, '', 'Server ', 'Diluar Data center diskominfo', 'Tidak Ada', 'Cloud Linux (Centos 7)', 'Apache 2.4.46', 'MySQL 5.7.32', 'php 5.5', 'CI 3.1.11', 4, 4, 2, 1, 2, 2, 1, 3, 3, 1, 4, 2, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-10-20 12:23:17', NULL, '2021-10-20 12:23:17', NULL, NULL, NULL),
	(12, 'SW-KMINFO-012', 'padatkarya.kedirikota.go.id.', '103.78.106.54', 93, 2, 'Aplikasi padat karya', 'Aplikasi padat karya', 'Sistem Informasi untuk monitoring progress kegiatan padat karya di kota kediri', 2, '', 'Server ', 'Dinas Kominfo', 'Tidak Ada', 'Windows Server 2012', 'Apache 2.4.41', 'MySQL Mariadb 15.1', 'php 7.3', 'CI 3.1.11', 2, 4, 2, 2, 2, 3, 1, 3, 3, 1, 3, 2, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-10-20 12:23:17', NULL, '2021-10-20 12:23:17', NULL, NULL, NULL),
	(13, 'SW-KMINFO-013', 'pecut.kedirikota.go.id.', '103.78.106.52', 93, 2, 'Aplikasi pecut', 'Aplikasi pecut', 'Aplikasi untuk centrrelize authentication services (CAS) untuk seluruh layanan publik yang menggunakan NIK sebagai login', 1, '', 'Server ', 'Dinas Kominfo', 'Tidak Ada', 'Windows 10 Pro', 'Apache 2.4.41', 'MySQL Mariadb 15.1', 'php 7.3', 'CI 3.1.11', 4, 4, 2, 1, 3, 2, 1, 3, 3, 1, 4, 2, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-10-20 12:23:17', NULL, '2021-10-20 12:23:17', NULL, NULL, NULL),
	(14, 'SW-KMINFO-014', 'ppid.kedirikota.go.id.', '124.40.255.186', 93, 1, 'website PPID', 'website PPID', 'Website penjabat penglola informasi dan komunikasi', 2, '', 'Server ', 'Diluar Data center diskominfo', 'Tidak Ada', 'Cloud Linux (Centos 7)', 'Apache 2.4.46', 'MySQL 5.7.32', 'php 5.5', 'CI 2.1.4', 4, 4, 2, 1, 2, 2, 1, 3, 3, 1, 2, 2, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-10-20 12:23:17', NULL, '2021-10-20 12:23:17', NULL, NULL, NULL),
	(15, 'SW-KMINFO-015', 'sekawan.diskominfo.kedirikota.go.id.', '124.40.255.186', 93, 1, 'Aplikasi sekawan', 'Aplikasi sekawan', 'Sistim Informasi kepegawaian non PNS dan pendataan siswa/mahasiswa magang diskominfo kota kediri', 2, '', 'Server ', 'Diluar Data center diskominfo', 'Tidak Ada', 'Cloud Linux (Centos 7)', 'Apache 2.4.46', 'MySQL 5.7.32', 'php 5.5', 'native PHP', 1, 4, 2, 2, 2, 2, 3, 3, 3, 1, 2, 2, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-10-20 12:23:17', NULL, '2021-10-20 12:23:17', NULL, NULL, NULL),
	(16, 'SW-KMINFO-016', 'sie.kedirikota.go.id.', '103.78.106.5', 93, 2, 'website sistim informasi eksekutif kota kediri', 'website sistim informasi eksekutif kota kediri', 'Website dashboard eksekutif kota kediri, menampilkan dashboard layanan kota kediri', 1, '', 'Server ', 'Dinas Kominfo', 'Tidak Ada', 'Windows Server 2012', 'Apache 2.4.37', 'MySQL Mariadb 15.1', 'php 5.6', 'CI 3.1.9', 4, 4, 2, 2, 2, 2, 4, 3, 3, 1, 4, 1, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-10-20 12:23:17', NULL, '2021-10-20 12:23:17', NULL, NULL, NULL),
	(17, 'SW-KMINFO-017', 'smartcity.kedirikota.go.id.', '124.40.255.186', 93, 1, 'Website smart city', 'Website smart city', 'Website informasi seputar smart city kota kediri', 2, '', 'Server ', 'Diluar Data center diskominfo', 'Tidak Ada', 'Cloud Linux (Centos 7)', 'Apache 2.4.46', 'MySQL 5.7.32', 'php 5.5', 'wordpress 5.5.3', 4, 4, 2, 1, 2, 2, 1, 3, 3, 1, 1, 2, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-10-20 12:23:17', NULL, '2021-10-20 12:23:17', NULL, NULL, NULL),
	(18, 'SW-KMINFO-018', 'surga.kedirikota.go.id.', '103.78.106.10', 93, 2, 'website aduan surga', 'website aduan surga', 'Website aduan warga kota kediri dengan memasukkan nomor SMS dalam perkembangannya dikendalikan secara operasional oleh satpol PP', 1, '', 'Server ', 'Dinas Kominfo', 'Tidak Ada', 'ubuntu 14.04.1', 'Apache 2.4.7', 'MySQL Mariadb 15.1', 'php 5.5.9', 'CI 2.2.6', 4, 4, 2, 1, 2, 2, 4, 3, 3, 2, 4, 2, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-10-20 12:23:17', NULL, '2021-10-20 12:23:17', NULL, NULL, NULL),
	(19, 'SW-KMINFO-019', 'tilangcovid.kedirikota.go.id.', '103.78.106.54', 93, 2, 'aplikasi tilang covis', 'aplikasi tilang covis', 'Aplikasi pendataan tilang untuk warga kota kediri terhadap pelanggaran protokol kesehatan covid19', 1, '', 'Server ', 'Dinas Kominfo', 'Tidak Ada', 'Windows Server 2012', 'Apache 2.4.41', 'MySQL Mariadb 15.1', 'php 7.3', 'CI 3.1.11', 1, 4, 2, 1, 2, 2, 1, 3, 3, 1, 4, 2, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-10-20 12:23:17', NULL, '2021-10-20 12:23:17', NULL, NULL, NULL),
	(20, 'SW-KELDER-001', 'kel-dermo.kedirikota.go.id.', '124.40.255.186', 66, 1, 'website kelurahahn dermo', 'website kelurahan', 'website kelurahahn dermo', 2, '', 'Server ', 'Dinas Kominfo', 'Tidak Ada', 'Cloud Linux (Centos 7)', 'Apache 2.4.46', 'MySQL 5.7.32', 'php 5.5', 'wordpress 5.5.3', 4, 1, 2, 1, 3, 3, 1, 3, 2, 1, 3, 2, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-10-20 12:23:17', NULL, '2021-10-20 12:23:17', NULL, NULL, NULL),
	(21, 'SW-KELPAK-001', 'kel-pakelan.kedirikota.go.id.', '124.40.255.186', 53, 1, 'website kelurahan', 'website kelurahan', 'website kelurahan', 2, '', 'Server ', 'Dinas Kominfo', 'Tidak Ada', 'Cloud Linux (Centos 7)', 'Apache 2.4.46', 'MySQL 5.7.32', 'php 5.5', 'Tidak ada', 4, 1, 2, 1, 3, 2, 1, 3, 2, 1, 4, 3, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-10-20 12:23:17', NULL, '2021-10-20 12:23:17', NULL, NULL, NULL),
	(22, 'SW-KELLIR-001', 'kel-lirboyo.kedirikota.go.id.', '124.40.255.186', 68, 1, 'website kelurahan', 'website kelurahan', 'website kelurahan', 2, '', 'Server ', 'Dinas Kominfo', 'Tidak Ada', 'Cloud Linux (Centos 7)', 'Apache 2.4.46', 'MySQL 5.7.32', 'php 5.5', 'Tidak ada', 4, 1, 2, 1, 1, 1, 1, 3, 2, 1, 1, 1, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-10-20 12:23:17', NULL, '2021-10-20 12:23:17', NULL, NULL, NULL),
	(23, 'SW-KELBAND-001', 'kel-bandarkidul.kedirikota.go.id.', '124.40.255.186', 61, 1, 'website kelurahan', 'website kelurahan', 'website kelurahan', 2, '', 'Server ', 'Dinas Kominfo', 'Tidak Ada', 'Cloud Linux (Centos 7)', 'Apache 2.4.46', 'MySQL 5.7.3', 'php 5.5', 'Tidak ada', 4, 1, 2, 1, 1, 1, 1, 3, 2, 1, 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-10-20 12:23:17', NULL, '2021-10-20 12:23:17', NULL, NULL, NULL),
	(24, 'SW-KELNGAM-001', 'Kel-ngampel.kedirikota.go.id.', '124.40.255.186', 71, 1, 'website kelurahan', 'website kelurahan', 'website kelurahan', 2, '', 'Server ', 'Dinas Kominfo', 'Tidak Ada', 'Cloud Linux (Centos 7)', 'Apache 2.4.46', 'MySQL 5.7.3', 'php 5.5', 'Tidak ada', 4, 1, 2, 1, 1, 1, 1, 3, 2, 1, 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-10-20 12:23:17', NULL, '2021-10-20 12:23:17', NULL, NULL, NULL),
	(25, 'SW-KELBUJ-001', 'kel-bujel.kedirikota.go.id.', '124.40.255.186', 64, 1, 'website kelurahan', 'website kelurahan', 'website kelurahan', 2, '', '', 'LDP (Lintas Data Prima)', 'Tidak Ada', 'Cloud Linux (Centos 7)', 'Apache 2.4.46', 'MySQL 5.7.3', 'php 5.5', 'Tidak ada', 4, 1, 2, 1, 1, 1, 1, 3, 2, 1, 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-10-20 12:23:17', NULL, '2021-10-20 12:23:17', NULL, NULL, NULL),
	(26, 'SW-KELPOJ-001', 'kel-pojok.kedirikota.go.id.', '124.40.255.186', 72, 1, 'website kelurahan', 'website kelurahan', 'website kelurahan', 2, '', '', 'LDP (Lintas Data Prima)', 'Tidak Ada', 'Cloud Linux (Centos 7)', 'Apache 2.4.46', 'MySQL 5.7.3', 'php 5.5', 'Tidak ada', 4, 4, 2, 1, 1, 1, 1, 3, 2, 1, 2, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-10-20 12:23:17', NULL, '2021-10-20 12:23:17', NULL, NULL, NULL),
	(27, 'SW-KECKOT-001', 'kec-kota.kedirikota.go.id.', '124.40.255.186', 42, 1, 'website kecamatan', 'website kecamatan', 'website kecamatan', 2, '', 'Server ', 'Dinas Kominfo', 'Tidak Ada', 'Cloud Linux (Centos 7)', 'Apache 2.4.46', 'MySQL 5.7.3', 'php 7.3', 'wordpress', 4, 4, 2, 1, 1, 1, 1, 3, 2, 1, 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-10-20 12:23:17', NULL, '2021-10-20 12:23:17', NULL, NULL, NULL),
	(28, 'SW-KELGAYM-001', 'kel-gayam.kedirikota.go.id.', '124.40.255.186', 67, 1, 'website kelurahan', 'website kelurahan', 'website kelurahan', 2, '', '', 'LDP (Lintas Data Prima)', 'Tidak Ada', 'Windows Server 2012', 'Apache 2.4.46', 'MySQL 5.7.3', 'php 7.3', 'wordpress', 4, 1, 2, 1, 1, 1, 1, 3, 2, 1, 1, 1, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-10-20 12:23:17', NULL, '2021-10-20 12:23:17', NULL, NULL, NULL),
	(29, 'SW-KELREJO-001', 'kel-rejomulyo.kedirikota.go.id.', '124.40.255.186', 55, 1, 'website kelurahan', 'website kelurahan', 'website kelurahan', 2, '', 'Server ', 'Dinas Kominfo', 'Tidak Ada', 'Cloud Linux (Centos 7)', 'Apache 2.4.46', 'MySQL 5.7.3', 'php 5.5', 'Tidak ada', 4, 4, 2, 1, 1, 1, 1, 3, 2, 1, 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-10-20 12:23:17', NULL, '2021-10-20 12:23:17', NULL, NULL, NULL),
	(30, 'SW-KELPKUN-001', 'kel-pakunden.kedirikota.go.id.', '124.40.255.186', 77, 1, 'website kelurahan', 'website kelurahan', 'website kelurahan', 2, '', 'Server ', 'Dinas Kominfo', 'Tidak Ada', 'Cloud Linux (Centos 7)', 'Apache 2.4.46', 'MySQL 5.7.3', 'php 5.5', 'Wordpress  V.5.18', 4, 4, 2, 1, 2, 1, 1, 3, 2, 1, 2, 1, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-10-20 12:23:17', NULL, '2021-10-20 12:23:17', NULL, NULL, NULL),
	(31, 'SW-KELSEM-001', 'kel-semampir.kedirikota.go.id.', '124.40.255.186', 57, 1, 'website kelurahan', 'website kelurahan', 'website kelurahan', 2, '', '', 'LDP (Lintas Data Prima)', 'Tidak Ada', 'Cloud Linux (Centos 7)', 'Apache 2.4.46', 'MySQL 5.7.3', 'php 5.5', 'Wordpress 5.0.11', 4, 1, 2, 1, 1, 1, 1, 3, 2, 1, 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-10-20 12:23:17', NULL, '2021-10-20 12:23:17', NULL, NULL, NULL),
	(32, 'SW-KELSGED-001', 'kel-setonogedong.kedirikota.go.id.', '124.40.255.186', 58, 1, 'website kelurahan', 'website kelurahan', 'website kelurahan', 2, '', 'Server ', 'Dinas Kominfo', 'Tidak Ada', 'Cloud Linux (Centos 7)', 'Apache 2.4.46', 'MySQL 5.7.3', 'php 5.5', 'Tidak ada', 1, 4, 2, 1, 2, 1, 1, 3, 2, 1, 3, 2, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-10-20 12:23:17', NULL, '2021-10-20 12:23:17', NULL, NULL, NULL),
	(33, 'SW-KELSPAND-001', 'kel-setonopande.kedirikota.go.id.', '124.40.255.186', 59, 1, 'website kelurahan', 'website kelurahan', 'website kelurahan', 2, '', '', 'LDP (Lintas Data Prima)', 'Tidak Ada', 'Cloud Linux (Centos 7)', 'Apache 2.4.46', 'MySQL 5.7.3', 'php 5.5', 'Wordpress - 4 . 8 . 15', 4, 4, 2, 1, 1, 1, 1, 3, 3, 1, 1, 1, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-10-20 12:23:17', NULL, '2021-10-20 12:23:17', NULL, NULL, NULL),
	(34, 'SW-KELBANAR-001', 'kel-banaran.kedirikota.go.id.', '124.40.255.186', 79, 1, 'website kelurahan', 'website kelurahan', 'website kelurahan', 2, '', '', 'LDP (Lintas Data Prima)', 'Tidak Ada', 'Cloud Linux (Centos 7)', 'Apache 2.4.46', 'MySQL 5.7.3', 'php 5.5', ' wordpress  5.5.3', 4, 4, 2, 1, 2, 2, 1, 3, 2, 1, 1, 1, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-10-20 12:23:17', NULL, '2021-10-20 12:23:17', NULL, NULL, NULL),
	(35, 'SW-KELSUKO-001', 'kel-sukorame.kedirikota.go.id.', '124.40.255.186', 73, 1, 'website kelurahan', 'website kelurahan', 'website kelurahan', 2, '', '', 'LDP (Lintas Data Prima)', 'Tidak Ada', 'Cloud Linux (Centos 7)', 'Apache 2.4.46', 'MySQL 5.7.3', 'php 7.3', 'AJAX Framework', 4, 1, 2, 1, 2, 1, 1, 3, 3, 1, 1, 3, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-10-20 12:23:17', NULL, '2021-10-20 12:23:17', NULL, NULL, NULL),
	(36, 'SW-KELJAM-001', 'kel-jamsaren.kedirikota.go.id.', '124.40.255.186', 84, 1, 'website kelurahan', 'website kelurahan', 'website kelurahan', 2, '', 'Server ', 'Dinas Kominfo', 'Tidak Ada', 'Cloud Linux (Centos 7)', 'Apache 2.4.46', 'MySQL 5.7.3', 'php 5.5', 'Tidak ada', 4, 1, 2, 1, 1, 1, 1, 3, 2, 1, 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-10-20 12:23:17', NULL, '2021-10-20 12:23:17', NULL, NULL, NULL),
	(37, 'SW-KELKETAM-001', 'kel-ketami.kedirikota.go.id.', '124.40.255.186', 85, 1, 'website kelurahan', 'website kelurahan', 'website kelurahan', 2, '', '', 'LDP (Lintas Data Prima)', 'Tidak Ada', 'Cloud Linux (Centos 7)', 'Apache 2.4.46', 'MySQL 5.7.3', 'php 5.5', 'CI', 4, 1, 2, 1, 1, 1, 1, 3, 2, 1, 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-10-20 12:23:17', NULL, '2021-10-20 12:23:17', NULL, NULL, NULL),
	(38, 'SW-KECPES-001', 'kec-pesantren.kedirikota.go.id.', '124.40.255.186', 75, 1, 'website kecamatan', 'website kecamatan', 'website kecamatan', 2, '', '', 'LDP (Lintas Data Prima)', 'Tidak Ada', 'Cloud Linux (Centos 7)', 'Apache 2.4.46', 'MySQL 5.7.3', 'php 5.5', 'CI', 4, 1, 2, 1, 1, 1, 1, 3, 2, 1, 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-10-20 12:23:17', NULL, '2021-10-20 12:23:17', NULL, NULL, NULL),
	(39, 'SW-BHUKUM-001', 'jdih.kedirikota.go.id.', '124.40.255.186', 2, 1, 'JDIH Kota Kediri (Jaringan Dokumentasi dan Informasi Hukum)', 'JDIH Kota Kediri', 'JDIH adalah sistem informasi publik yang di peruntukan untuk seluruh masyarakat di indonesia terutama Kota Kediri untuk mengetahui atau mengunduh produk hukum daerah berupa peraturan daerah dan peraturan walikota', 1, '', 'Server ', 'Dinas Kominfo', 'Ada (kamera, scanner)', 'Cloud Linux (Centos 7)', 'Apache 2.4.46', 'MySQL 5.7.3', 'php 5.5', 'CI  3', 4, 4, 2, 1, 2, 2, 3, 2, 2, 2, 4, 3, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-10-20 12:23:17', NULL, '2021-10-20 12:23:17', NULL, NULL, NULL),
	(40, 'SW-BAGORG-001', 'ekinerja.kedirikota.go.id.', '103.78.106.14', 8, 2, 'E-Kinerja', 'E-Kinerja', 'E-Kinerja adalah aplikasi yang menyediakan informasi mengenai kedisplinan kerja pegaeai dan kinerja pegawai secara tepat waktu dan secara bersamaan memberikan motivasi berupa adanya kinerja pegawai.', 2, '', 'Server ', 'Dinas Kominfo', 'Tidak Ada', 'Linux Ubuntu 19-04', 'Apache 2.4.38', 'MySQL 5.7.28', 'php 5.7.28', 'Angular + Slim 3.1', 2, 4, 2, 1, 2, 2, 4, 3, 3, 2, 4, 2, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-10-20 12:23:17', NULL, '2021-10-20 12:23:17', NULL, NULL, NULL),
	(41, 'SW-BORGAN-002', 'sakip.kedirikota.go.id.', '124.40.255.186', 8, 1, 'ekinerja', 'sakip kota kediri', 'Mengupload data – data laporan kinerja (perencanaan, pengukuran, pelaporan, capaian, pengawasan) OPD di lingkungan Pemerintah Kota Kediri dan masyarakat bisa melihat, membaca dan mendownload', 2, '', 'Server ', 'LDP (Lintas Data Prima)', 'Tidak Ada', 'Cloud Linux (Centos 7)', 'Apache 2.4.46', 'MySQL 5.7.3', 'php 5.5', 'Wordpress  V.5.1.8', 0, 4, 2, 1, 2, 2, 1, 3, 2, 2, 2, 2, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-10-20 12:23:17', NULL, '2021-10-20 12:23:17', NULL, NULL, NULL),
	(42, 'SW-BADPEM-001', 'simepp.kedirikota.go.id.', '103.78.106.25', 4, 2, 'SIMEPP ', 'SIMEPP ', 'Aplikasi yang berisi data penganggaran, realisasi keuangan, pelaksanaan/realisasi kegiatan, Rencana Umum Pengadaan dan Perkembangan Pengadaan', 2, '', 'Server ', 'Dinas Kominfo', 'Tidak Ada', 'Linux Debian 9.5', 'Apache 2.4.25', 'MySQL 10.1.26', 'php 7.0.30', 'Laravel', 0, 4, 2, 1, 2, 2, 4, 3, 2, 2, 4, 2, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-10-20 12:23:17', NULL, '2021-10-20 12:23:17', NULL, NULL, NULL),
	(43, 'SW-BAPEMR-001', 'bagianpemerintahan.kedirikota.go.id.', '124.40.255.186', 1, 1, 'website bagian pemerintahan', 'website bagian pemerintahan', 'Website tentang informasi kegiatan urusan pemberdayaan masyarakat dan penunjang urusan pemerintahan', 2, '', 'Server ', 'Dinas Kominfo', 'Tidak Ada', 'Cloud Linux (Centos 7)', 'Apache 2.4.46', 'MySQL 5.7.3', 'php 5.5', '', 1, 1, 2, 1, 1, 1, 1, 3, 2, 1, 1, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-10-20 12:23:17', NULL, '2021-10-20 12:23:17', NULL, NULL, NULL),
	(44, 'SW-BAPJB-001', 'bpbj.kedirikota.go.id ', '124.40.255.186', 6, 1, 'Bpbj.kedirikota.go.id', 'Bpbj.kedirikota.go.id', 'Website opd berisi konten-konten even berita', 2, '', 'Server ', 'Dinas Kominfo', 'Tidak Ada', 'Cloud Linux (Centos 7)', 'Apache 2.4.46', 'MySQL 5.7.3', 'php 5.5', 'Wordpress ', 4, 1, 2, 1, 1, 1, 1, 3, 2, 1, 1, 1, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-10-20 12:23:17', NULL, '2021-10-20 12:23:17', NULL, NULL, NULL),
	(45, 'SW-BAPJB-002', 'bpbj.kedirikota.go.id/kotaksaran/', '124.40.255.186', 6, 1, 'Bpbj.kedirikota.go.id/kotaksaran', 'Bpbj.kedirikota.go.id/kotaksaran', 'Kotak Saran untu menampung kritik/saran', 2, '', 'Server ', 'Dinas Kominfo', 'Tidak Ada', 'Cloud Linux (Centos 7)', 'Apache 2.4.46', 'MySQL 5.7.3', 'php 5.5', '', 4, 1, 2, 1, 1, 2, 1, 1, 2, 1, 1, 2, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-10-20 12:23:17', NULL, '2021-10-20 12:23:17', NULL, NULL, NULL),
	(46, 'SW-BAPJB-003', 'lpse.kedirikota.go.id.', '36.66.195.237', 6, 2, 'lpse.kedirikota.go.id.', 'lpse.kedirikota.go.id.', 'SPSE merupakan aplikasi e-procurement yang dikembangkan oleh LKPP untuk diterapkan oleh instansi-instansi pemerintah di seluruh Indonesia. Instansi pemerintah di Indonesia sangat beraneka ragam begitu pula dengan anggaran yang mereka miliki. Ada instansi daerah yang memiliki anggaran lebih dari 7 trilyun dan ada pula yang hanya puluhan hingga ratusan miliar saja per tahun. Kondisi ini menjadi pertimbangan LKPP dalam mengembangkan sistem e-procurement SPSE.', 1, '', 'Server', 'Dinas Kominfo', 'Tidak Ada', 'Debian 8', 'Apache 2.4', 'PostgreSQL 10', 'Java 8', 'Spring', 0, 0, 2, 1, 2, 2, 3, 3, 3, 2, 4, 3, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-10-20 12:23:17', NULL, '2021-10-20 12:23:17', NULL, NULL, NULL),
	(47, 'SW-BAPJB-004', 'report-pbj.kedirikota.go.id.', '103.78.106.42', 6, 2, 'Report-pbj.kedirikota.go.id', 'simponi', 'Sistem untuk memonitoring pengadaan barang jasa tender/non tender', 2, '', 'Server', 'Dinas Kominfo', 'Tidak Ada', 'Windows 7 ', 'Apache 2.4', 'PostgreSQL', 'php 7 ', 'CI', 4, 0, 2, 1, 3, 3, 3, 3, 2, 1, 1, 1, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-10-20 12:23:17', NULL, '2021-10-20 12:23:17', NULL, NULL, NULL),
	(48, 'SW-RENLIT-001', 'simppd.kedirikota.go.id.', '103.78.106.27', 13, 2, 'SIPD', 'SIPD', 'Sistem Informasi Pemerintahan Daerah yang selanjutnya disingkat SIPD adalah pengelolaan informasi pembangunan daerah, informasi keuangan daerah, dan informasi Pemerintahan Daerah lainnya yang saling\nterhubung untuk dimanfaatkan dalam penyelenggaraan pembangunan daerah ', 2, '', 'Server ', 'Dinas Kominfo', 'Tidak Ada', 'Linux', 'Apache', 'MySQL', 'PHP', 'CI', 0, 4, 2, 1, 2, 2, 0, 3, 3, 2, 4, 3, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-10-20 12:23:17', NULL, '2021-10-20 12:23:17', NULL, NULL, NULL),
	(49, 'SW-RENLIT-002', 'tkpk.kedirikota.go.id/sinbad/', '103.78.106.27', 13, 2, 'sinbad', 'sinbad', 'SINBAD merupakan sistem informasi berbasis web based yang berisikan data mikro / Data Terpadu Kesejahteraan Sosial (DTKS) masyarakat Kota Kediri dengan  tingkat kesejahteraan 0-60 persen terendah di Indonesia yang meliputi data by name by address, data demografi, sosial dan ekonomi.', 1, '', 'Server ', 'Dinas Kominfo', 'Tidak Ada', 'Linux', 'Apache', 'MySQL', 'PHP', 'CI', 4, 4, 2, 1, 3, 3, 4, 3, 2, 1, 4, 2, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-10-20 12:23:17', NULL, '2021-10-20 12:23:17', NULL, NULL, NULL),
	(50, 'SW-RENLIT-003', 'csr.kedirikota.go.id.', '124.40.255.186', 13, 1, 'Website CSR Kota Kediri', 'Website CSR Kota Kediri', 'Website Corporate Social Responsibility (CSR)  Kota Kediri merupakan sistem informasi berbasis web based Yang berfungsi sebagai media informasi dan komunikasi Badan Perencanaan Pembangunan, Penelitian dan Pengembangan Daerah Kota Kediri selaku Ketua Tim Fasilitasi Pendayagunaan Program Tanggung Jawab Sosial Perusahaan (CSR) di Kota Kediri kepada public terkait program dan kegiatan CSR yang ada di Kota Kediri. Selain itu  website ini menjadi media interaktif antara pemerintah dengan Forum CSR dan lembaga atau instansi lain ( para pelaku CSR). ', 2, '', 'Server ', 'Dinas Kominfo', 'Tidak Ada', 'Linux', 'Apache', 'MySQL', 'PHP', 'CI', 4, 4, 2, 1, 3, 1, 0, 3, 2, 1, 3, 2, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-10-20 12:23:17', NULL, '2021-10-20 12:23:17', NULL, NULL, NULL),
	(51, 'SW-BAPBD-001', 'bpbd.kedirikota.go.id.', '124.40.255.186', 91, 1, 'Website BPBD Kota Kediri', 'Website BPBD Kota Kediri', 'Webiste BPBD kota kediri untuk mememenuhi fungsi komunikasi dan penyediaan informasi kepada publik dalam penanggulangan bencana serta sebagai sarana penyebaran informasi potensi bencana kepada masyarkat. Target pekerjaan website berisi informasi, foto, dan video terkait kebencanaan baik alam non alam lalu informasi materi terkait manajemen bencana, serta informasi manajemen bencana untuk relawan.', 2, '', 'Server', 'Lintas Data Prima', 'Tidak ada', 'Cloud Linux (Centos 7)', 'Apache 2.4.46', 'MySQL 5.7', 'php 7.0', 'CI 3.1.11', 4, 4, 2, 1, 3, 1, 3, 3, 2, 1, 1, 2, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-10-20 12:23:17', NULL, '2021-10-20 12:23:17', NULL, NULL, NULL),
	(52, 'SW-DISBUN-001', 'disbudparpora.kedirikota.go.id.', '124.40.255.186', 38, 0, 'Disbudparpora kota kediri', 'kediritourism.net', 'Aplikasi berbasis Web dimana isi konten web seputar kegiatan, event, dan informasi dari Dinas\nCatatan : Domain di suspend, diganti dengan kediritourism.net', 2, '', '', 'Jagoan Hosting', 'Tidak ada', 'Linux Centos 7', 'Apache', 'MySQL', 'PHP 7', 'CI', 4, 1, 2, 1, 2, 1, 3, 3, 2, 1, 1, 3, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-10-20 12:23:17', NULL, '2021-10-20 12:23:17', NULL, NULL, NULL),
	(53, 'SW-BKPPD-001', 'simpeg.kedirikota.go.id/arsipdigital', '103.78.106.26', 14, 2, 'Aplikasi Arsip Digital', 'Aplikasi Simpeg', 'Aplikasi pendukung simpeg (kegiatan pembangunan dan pengembangan Sisem Informasi Kepegawaian) ,untuk pengunggahan data', 2, '', 'PC', 'Dinas Kominfo', 'Tidak ada', 'Windows 10', 'Apache', 'MySQL 10.1.1', 'PHP 5.4', 'CI 2.1.3', 2, 1, 2, 1, 2, 2, 1, 3, 1, 2, 4, 2, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-10-20 12:23:17', NULL, '2021-10-20 12:23:17', NULL, NULL, NULL),
	(54, 'SW-BKPPD-002', 'epresensi.kedirikota.go.id.', '103.78.106.3', 14, 2, 'Aplikasi e-presensi', 'Aplikasi e-presensi', 'Untuk informasi proses penanganan kasus-kasus pelanggaran disiplin pegawai pemerintah kota kediri', 2, '', 'Server ', 'Dinas Kominfo', 'Ada (biometri)', 'Windows Server', 'Apache', 'MySQL', 'PHP', 'Symfony 1.4', 2, 4, 2, 1, 2, 1, 0, 3, 2, 1, 4, 2, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-10-20 12:23:17', NULL, '2021-10-20 12:23:17', NULL, NULL, NULL),
	(55, 'SW-BKPPD-003', 'simpeg.kedirikota.go.id/kepangkatan', '103.78.106.26', 14, 2, 'Aplikasi kepangkatan ', 'Aplikasi Simpeg', 'Aplikasi pendukung simpeg (kegiatan pembangunan dan pengembangan Sisem Informasi Kepegawaian) ,untuk pengajuan kenaikan pangkat, kenaikan gaji berkala, pensiun, mutasi', 2, '', 'PC', 'Dinas Kominfo', 'Tidak ada', 'Windows 10', 'Apache', 'MySQL 10.1.1', 'PHP 5.4', 'CI 2.1.3', 2, 4, 2, 1, 2, 2, 1, 3, 1, 2, 4, 3, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-10-20 12:23:17', NULL, '2021-10-20 12:23:17', NULL, NULL, NULL),
	(56, 'SW-BKPPD-004', 'simpeg.kedirikota.go.id.', '103.78.106.26', 14, 2, 'Simpeg', 'Simpeg', 'Aplikasi Sistem Informasi Kepegawaian (SIMPEG) Badan Kepegawaian, Pendidikan, dan Pelatihan Daerah Kota Kediri merupakan sebuah perangkat lunak yang membantu dalam  proses pengolahan data kepegawaian, memudahkan dalam melakukan fungsi analisis dan pengawasan kepegawaian ', 2, '', 'PC', 'Dinas Kominfo', 'Tidak ada', 'Windows 10', 'Apache', 'MySQL 10.1.1', 'PHP 5.4', 'CI 2.1.3', 4, 4, 2, 1, 2, 3, 4, 3, 2, 2, 4, 2, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-10-20 12:23:17', NULL, '2021-10-20 12:23:17', NULL, NULL, NULL),
	(57, 'SW-BKPPD-005', 'simpeg.kedirikota.go.id/skp', '103.78.106.26', 14, 2, 'Aplikasi SKP', 'Aplikasi Sasaran Kinerja Pegawai', 'Aplikasi untuk meilai sasaran kinerja pegawai. Memenudahkan pengunggahan P2KP.', 2, '', 'PC', 'Dinas Kominfo', 'Tidak ada', 'Windows 10', 'Apache', 'MySQL 10.1.1', 'PHP 5.4', 'CI 2.1.3', 2, 0, 2, 1, 2, 1, 1, 3, 2, 1, 2, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-10-20 12:23:17', NULL, '2021-10-20 12:23:17', NULL, NULL, NULL),
	(58, 'SW-BKPPD-006', 'bkd.kedirikota.go.id.', '124.40.255.186', 14, 1, 'Aplikasi website', 'Aplikasi website', 'Untuk informasi publik berita terkait kepegawaian di Pemerintah Kota Kediri. ', 2, '', 'Server ', 'Dinas Kominfo', 'Tidak ada', 'Cloud Linux (Centos 7)', 'Apache 2.4.46', 'MySQL ', 'php 6.7', 'Wordpress', 4, 4, 2, 1, 2, 3, 1, 2, 2, 1, 2, 2, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-10-20 12:23:17', NULL, '2021-10-20 12:23:17', NULL, NULL, NULL),
	(59, 'SW-BPPKAD-001', 'aset.bppkad.kedirikota.go.id.', '103.78.106.6', 92, 2, 'Aset.bppkad.kedirikota.go.id', 'Aset.bppkad.kedirikota.go.id', 'Portal aplikasi Bidang Pengelolaan Aset, Badan Pendapatan,Pengelolaan Keuangan dan Aset Daerah Kota Kediri ', 2, '', 'Server ', 'Dinas Kominfo', 'Tidak ada', 'Windows Server 2012 R2', 'Apache 2.4', 'MySQL 5.0.11', 'PHP 5.6', 'Ci 3', 1, 1, 2, 2, 3, 2, 4, 3, 2, 1, 4, 2, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-10-20 12:23:17', NULL, '2021-10-20 12:23:17', NULL, NULL, NULL),
	(60, 'SW-BPPKAD-002', 'tkd.bppkad.kedirikota.go.id.', '103.78.106.6', 92, 2, 'tkd.bppkad.kedirikota.go.id', 'tkd.bppkad.kedirikota.go.id', 'Aplikasi untuk lelang Tanah Pertanian Eks Tanah Kas Desa Kota Kediri', 2, '', 'Server ', 'Dinas Kominfo', 'Tidak ada', 'Windows Server 2012 R2', 'Apache 2.4', 'SQL Server 2008', 'PHP 5.6', 'Ci 3', 1, 4, 2, 2, 3, 3, 0, 3, 2, 1, 3, 2, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-10-20 12:23:17', NULL, '2021-10-20 12:23:17', NULL, NULL, NULL),
	(61, 'SW-BPPKAD-003', 'Bppkad.kedirikota.go.id.', '124.40.255.186', 92, 1, 'bppkad.kedirikota.go.id', 'bppkad.kedirikota.go.id', 'Website resmi Badan Pendapatan, Pengelolaan Keuangan dan Aset Daerah Kota Kediri (informasi kegiatan-kegiatan yang ad id BPPKAD Kediri yang diambil dari bidang-bidang)', 2, '', 'Server ', 'Dinas Kominfo', 'Tidak ada', 'Cloud Linux (Centos 7)', 'Apache 2.4.46', 'MySQL ', 'php 6.7', 'CI 3.1.10', 4, 4, 2, 1, 1, 1, 1, 3, 2, 1, 1, 2, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-10-20 12:23:17', NULL, '2021-10-20 12:23:17', NULL, NULL, NULL),
	(62, 'SW-BPPKAD-004', 'sms.bppkad.kedirikota.go.id.', '103.78.106.6', 92, 2, 'sms.bppkad.kedirikota.go.id (sistem monitoring SPJ)', 'sms.bppkad.kedirikota.go.id (sistem monitoring SPJ)', 'Portal aplikasi Bidang Anggaran pada skpd Badan Pendapatan,Pengelola Keuangan dan Aset Daerah Kota Kediri untuk memantau SPJ yang dikoreksi di  bagian anggaran sudah selesai atau belum.', 2, '', 'Server ', 'Dinas Kominfo', 'Tidak ada', 'Windows Server 2008 r2', 'Apache 2.4', 'SQL Server 2008', 'PHP 7.4', 'CI 3.1.11', 2, 1, 2, 2, 2, 2, 1, 2, 2, 2, 4, 2, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-10-20 12:23:17', NULL, '2021-10-20 12:23:17', NULL, NULL, NULL),
	(63, 'SW-BPPKAD-005', 'ebphtb.bppkad.kedirikota.go.id.', '124.40.255.186', 92, 1, 'ebphtb.bppkad.kedirikota.go.id', 'ebphtb.bppkad.kedirikota.go.id', 'Aplikasi pelaporan pajak yang digunakan oleh masyarakat (wajib pajak) untuk melaporkan dan membayarkan  BPHTB atas perolehan hak atas tanah dan bangunan', 1, '', 'Server ', 'BPPKAD', 'Tidak ada', 'Windows Server 2012 R2', 'Apache 2.0 (XAMPP 5.6.3)', 'Postgresql 9.3.2', 'PHP 5.6.3', 'Zend Framework 2', 4, 4, 2, 1, 3, 2, 0, 3, 2, 1, 4, 1, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-10-20 12:23:17', NULL, '2021-10-20 12:23:17', NULL, NULL, NULL),
	(64, 'SW-BPPKAD-006', 'esptpd.bppkad.kedirikota.go.id.', '182.253.31.74', 92, 2, 'Esptpd (elektronik surat pemberitahuan pajak daerah)', 'Esptpd (elektronik surat pemberitahuan pajak daerah)', 'Aplikasi pelaporan pajak yang digunakan oleh masyarakat (wajib pajak) untuk melaporkan dan membayarkan  pajak usahanya di kota kediri, seperti pajak hotel, pajak restoran, pajak parkir dan hiburan', 1, '', 'Server ', 'BPPKAD', 'Tidak ada', 'Windows Server 2012 R2', 'Apache 2.0 (XAMPP 5.6.3)', 'Postgresql 9.3.2', 'php 5.6.3', 'Zend Framework 2', 4, 4, 2, 1, 3, 2, 4, 3, 2, 1, 3, 2, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-10-20 12:23:17', NULL, '2021-10-20 12:23:17', NULL, NULL, NULL),
	(65, 'SW-BPPKAD-007', 'simpatda.bppkad.kedirikota.go.id.', '182.253.31.74', 92, 2, 'Simpatda (Sistim informasi pendapatan daerah)', 'Simpatda (Sistim informasi pendapatan daerah)', 'Aplikasi yang digunakan untuk pengelolaan data pajak daerah dan retibusi daerah di kota kediri, seperti pajak hotel, pajak restoran, pajak penerangan jalan, pajak reklame, pajak air tanah, pajak parkir,  pajak hiburan dan retribusi jasa usaha', 1, '', 'Server ', 'BPPKAD', 'Tidak ada', 'Windows Server 2012 R2', 'Apache 2.0 (XAMPP 5.6.3)', 'Postgresql 9.3.2', 'PHP 5.6.3', 'Zend Framework 2', 1, 4, 2, 2, 3, 2, 0, 3, 2, 1, 3, 1, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-10-20 12:23:17', NULL, '2021-10-20 12:23:17', NULL, NULL, NULL),
	(66, 'SW-BPPKAD-008', 'ws-api.bppkad.kedirikota.go.id.', '182.253.31.74', 92, 2, 'Webservice', 'ws-api', 'Sebagai jembatan aplikasi DPMPTSP dengan  Bank Jatim, BNI', 1, '', 'Server ', 'BPPKAD', 'Tidak ada', 'Windows Server 2012 R2', 'Apache 2.0 (XAMPP 5.6.3)', 'Postgresql 9.3.2 dan MySQL 5.6.21', 'PHP 5.6.3', 'CI 3', 2, 4, 2, 2, 3, 2, 1, 3, 2, 1, 4, 1, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-10-20 12:23:17', NULL, '2021-10-20 12:23:17', NULL, NULL, NULL),
	(67, 'SW-DINKES-001', 'simpus.dinkes.kedirikota.go.id.', '103.78.106.28', 21, 2, 'Simpustronik', 'Simpustronik', 'Website resmi Dinas Kesehatan, yang menampil beberapa informasi seputar Dinas Kesehatan dan Puskesmas se-Kota Kediri yang diperuntukkan untuk rekan-rekan di Puskesmas dan umum, \nyang menyatukan poli-poli di Puskesmas, dan basis datanya di Dinkes, terkait kegiatan yang dilakukan di Poli', 1, '', 'Server ', 'Dinas Kominfo', 'Tidak Ada', 'Linux', 'Apache', 'MySQL', 'PHP', 'Tidak ada', 1, 0, 2, 1, 3, 2, 3, 3, 2, 1, 4, 2, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-10-20 12:23:17', NULL, '2021-10-20 12:23:17', NULL, NULL, NULL),
	(68, 'SW-DINKES-002', 'dinkes.kedirikota.go.id.', '124.40.255.186', 21, 1, 'Website Dinkes', 'Website Dinkes', 'Website resmi Dinas Kesehatan, yang menampil beberapa \ninformasi seputar Dinas Kesehatan dan Puskesmas se-Kota Kediri', 2, '', 'Server ', 'Dinas Kominfo', 'Tidak Ada', 'Linux', 'Apache', 'MySQL', 'PHP', 'Tidak ada', 1, 0, 2, 1, 2, 1, 3, 3, 2, 1, 1, 2, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-10-20 12:23:17', NULL, '2021-10-20 12:23:17', NULL, NULL, NULL),
	(69, 'SW-DCAPIL-001', 'penduduk-layanan.kedirikota.go.id/sakti/', '182.253.31.27', 34, 2, 'Aplikasi SAKTI (System Adminitrasi Kependudukan berbasis Teknologi Informasi)', 'Aplikasi SAKTI (System Adminitrasi Kependudukan berbasis Teknologi Informasi)', 'Aplikasi Web base yang melayani permohonan \npendaftaran dokumen kependudukan dan pencatatan sipil', 2, '', 'Server ', 'Dinas kependudukan dan Pencatatan Sipil', 'Ekios-K', 'Multi platform (Windows atau Linux)', 'Apache  2.2', 'MySQL', 'php  5.4', 'CI 2.2', 4, 0, 1, 1, 2, 3, 3, 3, 2, 1, 4, 3, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-10-20 12:23:17', NULL, '2021-10-20 12:23:17', NULL, NULL, NULL),
	(70, 'SW-DCAPIL-002', 'penduduk-layanan.kedirikota.go.id/cek_nik/', '182.253.31.27', 34, 2, 'Aplikasi cek_nik', 'Aplikasi cek_nik', 'Aplikasi Web base yang melayani Informasi Status Data Kependudukan masyarakat Kota Kediri. (webservice pendukung layanan siak)', 2, '', 'Server ', 'Dinas kependudukan dan Pencatatan Sipil', 'Tidak Ada', 'Multi platform (Windows atau Linux)', 'Apache  2.2', 'MySQL', 'php  5.4', 'CI 2.2', 4, 0, 1, 1, 1, 2, 3, 3, 2, 1, 2, 2, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-10-20 12:23:17', NULL, '2021-10-20 12:23:17', NULL, NULL, NULL),
	(71, 'SW-DCAPIL-003', 'penduduk-layanan.kedirikota.go.id/pengaduan/', '182.253.31.27', 34, 2, 'Aplikasi pangaduan', 'Aplikasi pangaduan', 'Aplikasi Web base yang melayani update konsolidasi pusat data kependudukan untuk pelayanan public lainnya seperti Perbankan, Kesehatan, Perpajakan, dll….\n(Laporan dari masyarakat tentang permasalahan kependudukan).', 2, '', 'Server ', 'Dinas kependudukan dan Pencatatan Sipil', 'Tidak Ada', 'Multi platform (Windows atau Linux)', 'Apache  2.2', 'MySQL', 'php  5.4', 'CI 2.2', 4, 0, 1, 1, 1, 2, 3, 3, 2, 1, 2, 2, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-10-20 12:23:17', NULL, '2021-10-20 12:23:17', NULL, NULL, NULL),
	(72, 'SW-DCAPIL-004', 'penduduk-layanan.kedirikota.go.id/e-bip/', '182.253.31.27', 34, 2, 'Aplikasi e-bip (buku induk penduduk) ', 'Aplikasi e-bip (buku induk penduduk) ', 'Aplikasi Web base yang melayani eksekutif dan internal pemerintah kota kediri untuk data agregat kependudukan.', 2, '', 'Server ', 'Dinas kependudukan dan Pencatatan Sipil', 'Tidak Ada', 'Multi platform (Windows atau Linux)', 'Apache  2.2', 'MySQL', 'php  5.4', 'CI 2.2', 4, 0, 1, 2, 2, 3, 3, 3, 2, 1, 4, 1, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-10-20 12:23:17', NULL, '2021-10-20 12:23:17', NULL, NULL, NULL),
	(73, 'SW-DLHKP-001', 'dlhkp.kedirikota.go.id.', '124.40.255.186', 17, 1, 'Website DLHKP', 'Website DLHKP', 'Website informasi ke masyarakat tentang kegiatan OPD dan kegiatan lainnya yang dilaksanakan oleh OPD.', 2, '', 'Server ', 'LDP (Lintas Data Prima)', 'Tidak Ada', 'Cloud Linux (Centos7)', 'Apache 2.4.46', 'MySQL 5.7.32', 'php 7.1', 'wordpress 4.9.16', 4, 4, 2, 1, 2, 2, 3, 3, 2, 1, 1, 2, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-10-20 12:23:17', NULL, '2021-10-20 12:23:17', NULL, NULL, NULL),
	(74, 'SW-DINDIK-001', 'bansos.pendidikan.kedirikota.go.id.', '124.40.255.186', 20, 1, 'Situs Resmi Dinas Pendidikan Kota Kediri (https://bansos.pendidikan.kedirikota.go.id/)', 'Situs Resmi Dinas Pendidikan Kota Kediri (https://bansos.pendidikan.kedirikota.go.id/)', 'Adalah situs portal yang memuat informasi terkait Bantuan Sosial Pendidikan. ', 2, '', 'Server ', 'LDP (Lintas Data Prima)', 'Tidak Ada', 'Cloud Linux (Centos7)', 'Apache 2.4.46', 'MySQL 5.7.32', 'PHP 5.6', 'CI 3.1.9', 1, 2, 2, 1, 3, 1, 0, 3, 2, 1, 1, 2, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-10-20 12:23:17', NULL, '2021-10-20 12:23:17', NULL, NULL, NULL),
	(75, 'SW-DINDIK-002', 'ketenagaan.pendidikan.kedirikota.go.id.', '124.40.255.186', 20, 1, 'Aplikasi Ketenagaan', 'Aplikasi Ketenagaan', 'Adalah aplikasi situs web kusus Bidang Ketenagaan Dinas Pendidikan Kota Kediri, yang memuat semua informasi, alur dan syarat terkait PPTK kususnya di dinas Pendidikan Kota Kediri.', 2, '', 'Server ', 'LDP (Lintas Data Prima)', 'Tidak Ada', 'Cloud Linux (Centos7)', 'Apache 2.4.46', 'MySQL 5.7.32', 'PHP 7.3', 'Wordpress versi  5', 1, 4, 2, 1, 2, 3, 0, 3, 2, 1, 1, 1, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-10-20 12:23:17', NULL, '2021-10-20 12:23:17', NULL, NULL, NULL),
	(76, 'SW-DINDIK-003', 'emas.pendidikan.kedirikota.go.id.', '124.40.255.186', 20, 1, 'Situs Web Resmi Program English Massive', 'Situs Web Resmi Program English Massive', 'Adalah situs portal yang memuat informasi terkait Program English Massive, mengelola data tutor, data kelas dan data spot, serta digunakan untuk pendaftaran spot online. Selain itu, situs ini juga sebagai sarana media informasi terkait kegiatan Program English Massive.', 2, '', 'Server ', 'LDP (Lintas Data Prima)', 'Tidak Ada', 'Cloud Linux (Centos7)', 'Apache 2.4.46', 'MySQL 5.7.32', 'PHP 5.6', 'CI 3.1.11', 1, 2, 2, 1, 3, 1, 0, 3, 2, 1, 3, 2, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-10-20 12:23:17', NULL, '2021-10-20 12:23:17', NULL, NULL, NULL),
	(77, 'SW-DINDIK-004', 'certificate.pendidikan.kedirikota.go.id.', '124.40.255.186', 20, 1, 'Aplikasi Sertifikat Online English Massive', 'Emas Online Certificate (EOC)', 'Adalah aplikasi situs web kusus untuk sertifikat online dari program English massive, yang  outputnya berupa sertifikat online/digital dan disertai validasi QRcode', 2, '', 'Server ', 'LDP (Lintas Data Prima)', 'Tidak Ada', 'Cloud Linux (Centos7)', 'Apache 2.4.46', 'MySQL 5.7.32', 'PHP 5.6', 'CI 3.1.11', 1, 2, 2, 1, 3, 1, 0, 3, 2, 1, 2, 2, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-10-20 12:23:17', NULL, '2021-10-20 12:23:17', NULL, NULL, NULL),
	(78, 'SW-DINDIK-005', 'backbone.pendidikan.kedirikota.go.id.', '36.66.199.27', 20, 2, 'Aplikasi Backbone Dapodik Dinas Pendidikan Kota Kediri', 'Aplikasi Backbone Dapodik Dinas Pendidikan Kota Kediri', 'Adalah aplikasi situs web kusus data master dapodik, aplikasi ini di sediakan oleh pustekom kemendikbud. (sinkronisasi dengan dapodik)', 2, '', 'Server ', 'LDP (Lintas Data Prima)', 'Tidak Ada', 'Cloud Linux (Centos7)', 'Apache 2.4.46', 'MySQL 5.7.32', 'PHP 7.3', 'Wordpress versi  5', 1, 2, 2, 1, 2, 3, 0, 3, 2, 1, 1, 1, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-10-20 12:23:17', NULL, '2021-10-20 12:23:17', NULL, NULL, NULL),
	(79, 'SW-DINDIK-006', 'ppdb.kedirikota.go.id.', '124.40.255.186', 20, 1, 'PPDB Online Kota Kediri', 'PPDB Online Kota Kediri', 'Adalah aplikasi situs web kusus untuk menangani Pendaftaran Peserta Didik Baru secara online, denga kerja sama dengan Telkom. ', 2, '', 'Server ', 'Telkom', 'Tidak Ada', 'Linux', 'Apache', 'MySQL', 'PHP', 'Tidak ada', 4, 2, 2, 1, 3, 1, 0, 3, 2, 1, 3, 2, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-10-20 12:23:17', NULL, '2021-10-20 12:23:17', NULL, NULL, NULL),
	(80, 'SW-DINDIK-007', 'pendidikan.kedirikota.go.id.', '124.40.255.186', 20, 1, 'Situs Resmi Dinas Pendidikan Kota Kediri (https://pendidikan.kedirikota.go.id/)', 'Situs Resmi Dinas Pendidikan Kota Kediri (https://pendidikan.kedirikota.go.id/)', 'Adalah situs portal utama yang memuat inoformasi terkait Pendidikan, dan sub aplikasi yang ada di Dinas Pendidikan Kota Kediri.', 2, '', 'Server ', 'LDP (Lintas Data Prima)', 'Tidak Ada', 'Cloud Linux (Centos7)', 'Apache 2.4.46', 'MySQL 5.7.32', 'PHP 5.6', 'CI 3.1.11', 1, 2, 2, 1, 3, 1, 0, 3, 2, 1, 1, 1, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-10-20 12:23:17', NULL, '2021-10-20 12:23:17', NULL, NULL, NULL),
	(81, 'SW-DINDIK-008', 'dashboard.pendidikan.kedirikota.go.id.', '36.66.199.27', 20, 2, 'Aplikasi Dashboard Dapodik', 'Aplikasi Dashboard Dapodik', 'Adalah aplikasi situs web kusus untuk data grafik dari data master aplikasi backbone, yang dalam penyajian data dalam bentuk Grafik.', 2, '', 'Server ', 'Dinas Pendidikan', 'Tidak Ada', 'Windows Server 2012', 'Web Server IIS', 'SQL Server 2012 v.11.0.3128.0', 'PHP 5.6.31', 'CI v 3.x', 1, 2, 2, 2, 1, 1, 0, 3, 2, 1, 2, 1, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-10-20 12:23:17', NULL, '2021-10-20 12:23:17', NULL, NULL, NULL),
	(82, 'SW-DINDIK-009', 'mutasisiswa.kedirikota.go.id.', '36.66.199.27', 20, 2, 'Aplikasi MutasiSiswa Dinas Pendidikan Kota Kediri', 'Aplikasi MutasiSiswa Dinas Pendidikan Kota Kediri', 'Adalah situs portal yang memuat informasi terkait Mutasi siswa SD dan SMP Negeri dan swasta se kota Kediri. Selain itu, situs ini juga sebagai sarana media informasi mengenai jumlah siswa per kelas tiap sekolah.', 2, '', 'Server ', 'Dinas Pendidikan', 'Tidak Ada', 'Windows Server 2012', 'Web Server IIS', 'SQL Server 2012 v.11.0.3128.0', 'PHP 5.6.31', 'CI v 3.x', 4, 0, 2, 1, 3, 1, 0, 3, 2, 2, 3, 1, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-10-20 12:23:17', NULL, '2021-10-20 12:23:17', NULL, NULL, NULL),
	(83, 'SW-DINDIK-010', '103.78.106.30/emas/index.php/login', '124.40.255.186', 20, 2, 'Aplikasi Managemen Tutor Program English Massive', 'Aplikasi Managemen Tutor Program English Massive', 'Adalah situs untuk menerima report tutor English Massive pada saat mengajar di Spot, rekap bulanan dan presensi online Tutor EMAS', 2, '', 'Server ', 'Dinas Kominfo', 'Tidak Ada', 'Linux Debian GNU/Linux 9', 'Apache 2.4.25', 'MySQL 10.1.26-MariaDB', 'PHP 7.0.30', 'CI 3.1.9', 1, 2, 2, 2, 2, 1, 0, 3, 2, 1, 3, 2, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-10-20 12:23:17', NULL, '2021-10-20 12:23:17', NULL, NULL, NULL),
	(84, 'SW-DINDIK-011', 'sibos.pendidikan.kedirikota.go.id.', '103.78.106.6', 20, 2, 'Aplikasi SIBOS', 'Aplikasi SIBOS', 'Aplikasi sistem informasi dana bos, yang digunakan untuk monitoring penggunaan anggaran dana bos secara rinci,informasi mulai dari penerimaan dana dari pusat, mutasi dana bos, peng-SPJ an sampai dengan belanja rinci dengan parameter integrasi DPA dari SIMDA.', 2, '', 'Server ', 'Dinas Kominfo', 'Tidak Ada', 'Windows Server 2012 r2', 'Apache 2.4.29', 'MySQL 10.1.28-MariaDB', 'PHP 5.6.32', 'CI 3.1', 2, 0, 2, 2, 3, 2, 0, 3, 2, 1, 4, 2, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-10-20 12:23:17', NULL, '2021-10-20 12:23:17', NULL, NULL, NULL),
	(85, 'SW-DINDIK-012', 'cundamani.kedirikota.go.id', '124.40.255.186', 20, 1, 'Aplikasi cundamani', 'Aplikasi cundamani', 'Adalah aplikasi situs web kusus untuk sertifikat online dari program English massive,\nyang  outputnya berupa sertifikat online/digital dan disertai validasi QRcode ', 2, '', 'Server ', 'Dinas Kominfo', 'Tidak Ada', 'CloudLinux (Centos 7)', 'Apache 2.4.46', 'MySQL 5.7.32', 'PHP 5.6', 'CI 3.1.10', 1, 2, 2, 1, 3, 1, 3, 3, 2, 1, 2, 2, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-10-20 12:23:17', NULL, '2021-10-20 12:23:17', NULL, NULL, NULL),
	(86, 'SW-PMPTSP-001', 'kswi.kedirikota.go.id.', '182.253.186.75', 18, 2, 'Kediri Single Window for Investment disingkat KSWI', 'Kediri Single Window for Investment disingkat KSWI', 'KSWI merupakan sistem layanan elektronik yang dipakai DPMPTSP Kota Kediri untuk proses pelayanan perizinan yang dibagi dalam 2 lingkup proses, yaitu frontend (proses inputan dari pemohon) dan backend (proses validasi hingga terbit SK). KSWI melayani proses perizinan lingkup lokal dan proses pemenuhan komitmen perizinan yang menjadi syarat persetujuan izin usaha pada OSS (Online Single Submission) milik pemerintah pusat. \n\nIzin local meliputi : Surat Izin Usaha Perdagangan Minuman Beralkohol, STR Tenaga Kesehatan Hewan, Izin Tempat Usaha/ Operasional Jasa Medik Veteriner, Izin Penelitian, Izin Penyehat Tradisional, Izin Toko Alat Kesehatan, Izin Penyelenggaraan Optikal, Izin Toko Alat Kesehatan, Izin Tenaga Kesehatan Perorangan (SIP/SIK), Izin Tenaga Kesehatan Perorangan (SIP/SIK) Magang, Izin Mendirikan Bangunan, Izin Pembuangan Limbah Cair, Izin Penyimpanan Sementara B3, Izin Reklame, Izin Prinsip Tata Ruang, Rekomendasi Lokasi, Izin Lokasi, Izin Penggunaan Pemanfaatan Tanah, Rekom Titik Pembangunan Menara Telekomunikasi.\n\nIzin untuk pemenuhan komitmen OSS : Izin Penyelenggaraan Satuan Pendidikan Non Formal, Izin Penyelenggaraan Satuan Pendidikan Formal, Izin LPK, Izin Usaha Mikro Obat Tradisional, Izin Toko Obat, Izin Apotek, Izin Operasional Pemberantasan Hama (Pestisida), Izin Pendirian Fasilitas Pelayanan Kesehatan, Izin Pendirian Fasilitas Pelayanan Kesehatan, Izin Operasional Fasilitas Pelayanan Kesehatan, Izin Usaha Angkutan, Izin Usaha Industri, Izin Usaha Jasa Konstruksi, Izin Pariwisata', 1, '', 'Server ', 'DPMPTSP Kota Kediri ', 'Scanner, Printer', 'Linux Ubuntu LTS 18.04', 'Apache 2.4.29', 'MySQL 5.7.32', 'php 5.1.6', 'CI 2.1.4', 4, 4, 2, 1, 3, 3, 4, 3, 3, 1, 5, 3, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-10-20 12:23:17', NULL, '2021-10-20 12:23:17', NULL, NULL, NULL),
	(87, 'SW-PMPTSP-002', 'dpm.kedirikota.go.id.', '124.40.255.186', 18, 1, '', '', '', 0, '', 'Server ', '', '', '', '', '', '', '', 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-10-20 12:23:17', NULL, '2021-10-20 12:23:17', NULL, NULL, NULL),
	(88, 'SW-DARSIP-001', 'Inlis.kedirikota.go.id.', '103.78.106.29', 19, 2, 'INLIS LITE', 'INLIS LITE', 'Inlis Lite dibangun dan dikembangkan oleh Perpustakaan Nasional RI (Perpustakaan Nasional RI) sejak tahun 2011. Merupakan perangkat lunak satu pintu bagi pengelola perpustakaan untuk menerapkan otomasi perpustakaan sekaligus mengembangkan perpustakaan digital / mengelola dan melayankan koleksi digital.', 2, '', 'Server ', 'Dinas Kominfo', 'Tidak Ada', 'Windows 7 pro', 'Apache 2.4.17', 'MySQL mariadb 10.1.10', 'PHP 5.6.19', 'Tidak ada', 4, 0, 2, 1, 3, 1, 4, 3, 2, 1, 4, 2, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-10-20 12:23:17', NULL, '2021-10-20 12:23:17', NULL, NULL, NULL),
	(89, 'SW-DPERUM-001', 'ara.kedirikota.go.id.', '103.78.106.12', 94, 2, 'ASMARA (Aplikasi Sistem Informasi Aparteman Rakyat)', 'ARA (Apartemen Rakyat)', 'Aplikasi yang mempermudah UPTD selaku pengelolaa Rusunawa untuk melakukan pengelolaan dan pelayanan di Rusanawa yang meliputi :\n1.Web  yang berisi kegiatan warga Rusunawa\n2. Download Formulir Pendaftaran \n3. Pembayaran Sewa dan Air\n4. Penerbitan Surat Peringatan\n5. Stock Barang\n6. Pengaduan Warga\n7. SMS gate away', 2, '', 'PC, Server, Modem GSM', 'Dinas Perumahan dan Kawasan Pemukiman', 'Tidak Ada', 'Linux ', 'Apache ', 'MySQL', 'php', 'Tidak ada', 4, 4, 2, 1, 3, 1, 4, 3, 2, 1, 2, 1, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-10-20 12:23:17', NULL, '2021-10-20 12:23:17', NULL, NULL, NULL),
	(90, 'SW-INSPEK-001', 'wbs-inspektorat.kedirikota.go.id.', '103.78.106.54', 11, 2, 'Whistle Blowing System (WBS)', 'Whistle Blowing System (WBS)', 'Whistle Blowing System (WBS) adalah sarana pelaporan bagi Aparatur Sipil Negara karena adanya suatu perilaku atau tindakan yang melanggar Kode Etik,Perilaku Disiplin atau Penyalah Gunaan Wewenang.\nWBS menyediakan sistem yang terkoordinasi dan terintegrasi mulai dari penerimaan laporan hingga tindak lanjut penegakan dugaan pelanggaran. Melalui sistem tersebut, ASN dapat melaporkan dugaan pelanggaran kode etik, perilaku, dan Penyalah Gunaan Wewenang. Setiap laporan akan dijaga kerahasiannya dan dalam hal terdapat bukti yang cukup akan ditindaklanjuti pada proses investigasi selanjutnya. Keberadaan WBS menciptakan sistem saling mengawasi terhadap kesesuaian perilaku dan ketaatan prosedur kerja yang dilaksanakan oleh ASN. WBS juga sebagai bentuk komitmen ASNI untuk senantiasa menjaga integritas dan profesionalitas, termasuk akuntabilitas dalam penegakan terhadap dugaan pelanggaran.', 2, '', 'Server ', 'Dinas Kominfo', 'Tidak Ada', 'Windows Server 2012 standard version', 'Apache 2.4.41', 'MySQL MariaDB 15.1-10.4.6', 'PHP 7.3.9', 'CI ver 2.2.26', 2, 1, 2, 1, 1, 1, 4, 3, 2, 2, 3, 3, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-10-20 12:23:17', NULL, '2021-10-20 12:23:17', NULL, NULL, NULL),
	(91, 'SW-INSPEK-002', 'inspektorat.kedirikota.go.id.', '124.40.255.186', 11, 1, '', '', '', 0, '', 'Server ', '', '', '', '', '', '', '', 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-10-20 12:23:17', NULL, '2021-10-20 12:23:17', NULL, NULL, NULL),
	(92, 'SW-RSGAM-001', 'rsudgambiran.kedirikota.go.id.', '124.40.255.186', 33, 1, 'Website : rsudgambiran.kedirikota.go.id', 'Website : rsudgambiran.kedirikota.go.id', 'Sebuah media informasi yang berfungsi untuk memperkenalkan profil Rumah Sakit, Layanan Kesehatan, Branding dan Promosi serta media komunikasi dari Rumah Sakit kepada publik', 2, '', 'Server ', 'Dinas Kominfo', 'Tidak Ada', 'Cloud Linux (Centos 7)', 'Apache 2.4.46', 'MySQL 5.7.32', 'php 7.3', 'wordpress 5.5.3', 4, 0, 2, 1, 1, 2, 4, 3, 3, 1, 2, 2, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-10-20 12:23:17', NULL, '2021-10-20 12:23:17', NULL, NULL, NULL),
	(93, 'SW-PASJOY-001', 'perumdapasarjoyoboyo.kedirikota.go.id.', '124.40.255.186', 95, 1, 'Website PD Pasar', 'Website PD Pasar', 'Website resmi Perumda Pasar Joyoboyo Kota Kediri', 2, '', 'Server ', 'Dinas Kominfo', 'Tidak Ada', '', '', '', '', '', 4, 4, 2, 1, 2, 2, 1, 3, 2, 1, 1, 1, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-10-20 12:23:17', NULL, '2021-10-20 12:23:17', NULL, NULL, NULL),
	(94, 'SW-BAZNAS-001', 'baznas.kedirikota.go.id.', '103.78.106.50', 96, 2, 'Baznas Kediri Kota', 'Baznas Kediri Kota', 'Website informasi seputar baznas (profil, visi, misi), zakat dan aplikasi basket dan registrasi zakat infaq.', 2, '', 'Server ', 'Dinas Kominfo', 'Tidak Ada', 'Linux 20.4', 'Apache 2.4.41', 'MySQL 8.0.22', 'PHP 7.4.3', 'Laravel ci 5.7.29', 4, 0, 2, 1, 1, 1, 0, 3, 2, 1, 4, 2, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-10-20 12:23:17', NULL, '2021-10-20 12:23:17', NULL, NULL, NULL),
	(95, 'SW-BAGUM-001', 'kopitahu.kedirikota.go.id.', '124.40.255.186', 7, 1, 'Kopi Tahu', '', 'Sistim Informasi tentang Usulan warga yang ditampung melalui kegiatan kopi-tahu diantarnya :\n-	Menampilkan Usulan Warga\n-	Menampilkan Statistik - Lokasi\n-	Menampilkan Statistik  - Bidang \n-	Menampilkan Statistik  - Warga', 2, '', 'Server ', 'Dinas Kominfo', 'Tidak Ada', 'Cloud Linux (Centos 7)', 'Apache 2.4.46', 'MySQL 5.7.32', 'php 7.0', 'CI 3.0', 1, 4, 2, 2, 1, 1, 4, 3, 2, 2, 5, 2, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-10-20 12:23:17', NULL, '2021-10-20 12:23:17', NULL, NULL, NULL),
	(96, 'SW-DISHUB-001', 'dishub.kedirikota.go.id.', '124.40.255.186', 37, 1, 'Website Dishub', 'Website Dishub', 'Aplikasi website untuk menyampaikan kegiatan dan layanan /fasilitas publik pada Dinas Perhubungan Kota Kediri', 2, '', 'Server ', 'Dinas Kominfo', 'Tidak Ada', 'CloudLinux ', 'Apache', 'MySQL', 'PHP', 'Cl', 4, 4, 2, 1, 1, 1, 1, 3, 2, 1, 1, 2, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-10-20 12:23:17', NULL, '2021-10-20 12:23:17', NULL, NULL, NULL);
/*!40000 ALTER TABLE `aplikasi` ENABLE KEYS */;

-- Dumping structure for table u1543793_sicatat.berita_acara
CREATE TABLE IF NOT EXISTS `berita_acara` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `aplikasi_id` int(11) DEFAULT NULL,
  `no_versi` varchar(50) DEFAULT NULL,
  `deskripsi` text DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `path_file` varchar(200) DEFAULT NULL,
  `_active` tinyint(4) NOT NULL DEFAULT 1,
  `_created_at` timestamp NULL DEFAULT current_timestamp(),
  `_created_by` int(11) DEFAULT NULL,
  `_updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `_updated_by` int(11) DEFAULT NULL,
  `_deleted_at` timestamp NULL DEFAULT NULL,
  `_deleted_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table u1543793_sicatat.berita_acara: ~0 rows (approximately)
/*!40000 ALTER TABLE `berita_acara` DISABLE KEYS */;
/*!40000 ALTER TABLE `berita_acara` ENABLE KEYS */;

-- Dumping structure for table u1543793_sicatat.data_umum
CREATE TABLE IF NOT EXISTS `data_umum` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `aplikasi_id` int(11) DEFAULT NULL,
  `nm_internal` varchar(200) DEFAULT NULL,
  `nm_eksternal` varchar(200) DEFAULT NULL,
  `deskripsi` text DEFAULT NULL,
  `_active` tinyint(4) NOT NULL DEFAULT 1,
  `_created_at` timestamp NULL DEFAULT current_timestamp(),
  `_created_by` int(11) DEFAULT NULL,
  `_updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `_updated_by` int(11) DEFAULT NULL,
  `_deleted_at` timestamp NULL DEFAULT NULL,
  `_deleted_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table u1543793_sicatat.data_umum: ~0 rows (approximately)
/*!40000 ALTER TABLE `data_umum` DISABLE KEYS */;
/*!40000 ALTER TABLE `data_umum` ENABLE KEYS */;

-- Dumping structure for table u1543793_sicatat.desk_sistem
CREATE TABLE IF NOT EXISTS `desk_sistem` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `aplikasi_id` int(11) DEFAULT NULL,
  `sertf_kelayakan_id` tinyint(4) DEFAULT NULL,
  `ket_sertf` varchar(200) DEFAULT NULL,
  `_active` tinyint(4) NOT NULL DEFAULT 1,
  `_created_at` timestamp NULL DEFAULT current_timestamp(),
  `_created_by` int(11) DEFAULT NULL,
  `_updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `_updated_by` int(11) DEFAULT NULL,
  `_deleted_at` timestamp NULL DEFAULT NULL,
  `_deleted_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table u1543793_sicatat.desk_sistem: ~0 rows (approximately)
/*!40000 ALTER TABLE `desk_sistem` DISABLE KEYS */;
/*!40000 ALTER TABLE `desk_sistem` ENABLE KEYS */;

-- Dumping structure for table u1543793_sicatat.hardware
CREATE TABLE IF NOT EXISTS `hardware` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `aplikasi_id` int(11) DEFAULT NULL,
  `prkt_utama` int(11) DEFAULT NULL,
  `info_data_center` int(11) DEFAULT NULL,
  `prkt_khusus` int(11) DEFAULT NULL,
  `os` int(11) DEFAULT NULL,
  `web_server` int(11) DEFAULT NULL,
  `basis_data` int(11) DEFAULT NULL,
  `bhs_pemrograman` int(11) DEFAULT NULL,
  `framework` int(11) DEFAULT NULL,
  `_active` tinyint(4) NOT NULL DEFAULT 1,
  `_created_at` timestamp NULL DEFAULT current_timestamp(),
  `_created_by` int(11) DEFAULT NULL,
  `_updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `_updated_by` int(11) DEFAULT NULL,
  `_deleted_at` timestamp NULL DEFAULT NULL,
  `_deleted_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table u1543793_sicatat.hardware: ~0 rows (approximately)
/*!40000 ALTER TABLE `hardware` DISABLE KEYS */;
/*!40000 ALTER TABLE `hardware` ENABLE KEYS */;

-- Dumping structure for table u1543793_sicatat.kebijakan
CREATE TABLE IF NOT EXISTS `kebijakan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `aplikasi_id` int(11) DEFAULT NULL,
  `ref_sasaran_id` int(11) DEFAULT NULL,
  `ref_dasarhukum_id` int(11) DEFAULT NULL,
  `ref_kategoriakses_id` int(11) DEFAULT NULL,
  `layanan_publik` int(11) DEFAULT NULL,
  `_active` tinyint(4) NOT NULL DEFAULT 1,
  `_created_at` timestamp NULL DEFAULT current_timestamp(),
  `_created_by` int(11) DEFAULT NULL,
  `_updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `_updated_by` int(11) DEFAULT NULL,
  `_deleted_at` timestamp NULL DEFAULT NULL,
  `_deleted_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table u1543793_sicatat.kebijakan: ~0 rows (approximately)
/*!40000 ALTER TABLE `kebijakan` DISABLE KEYS */;
/*!40000 ALTER TABLE `kebijakan` ENABLE KEYS */;

-- Dumping structure for table u1543793_sicatat.menu
CREATE TABLE IF NOT EXISTS `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent` int(11) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `code` varchar(245) CHARACTER SET latin1 DEFAULT NULL,
  `name` varchar(245) CHARACTER SET latin1 DEFAULT NULL,
  `url` varchar(245) CHARACTER SET latin1 DEFAULT NULL,
  `icon_type` varchar(245) CHARACTER SET latin1 DEFAULT NULL,
  `icon_keyword` varchar(245) CHARACTER SET latin1 DEFAULT NULL,
  `badge_type` varchar(245) CHARACTER SET latin1 DEFAULT NULL,
  `badge_keyword` varchar(245) CHARACTER SET latin1 DEFAULT NULL,
  `_active` tinyint(4) NOT NULL DEFAULT 1,
  `_created_at` timestamp NULL DEFAULT current_timestamp(),
  `_created_by` int(11) DEFAULT NULL,
  `_updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `_updated_by` int(11) DEFAULT NULL,
  `_deleted_at` timestamp NULL DEFAULT NULL,
  `_deleted_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table u1543793_sicatat.menu: ~4 rows (approximately)
/*!40000 ALTER TABLE `menu` DISABLE KEYS */;
INSERT INTO `menu` (`id`, `parent`, `order`, `code`, `name`, `url`, `icon_type`, `icon_keyword`, `badge_type`, `badge_keyword`, `_active`, `_created_at`, `_created_by`, `_updated_at`, `_updated_by`, `_deleted_at`, `_deleted_by`) VALUES
	(1, 0, 1, '001', NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-02-01 06:57:37', NULL, '2021-02-01 06:57:44', NULL, NULL, NULL),
	(2, 1, 2, '001:002', 'Daftar list aplikasi', 'Aplikasi', 'fa', 'fas fa-list', NULL, NULL, 1, '2021-02-01 06:58:09', NULL, '2021-09-29 04:23:41', NULL, NULL, NULL),
	(3, 1, 1, '001:001', 'Dashboard', 'Dashboard', 'fa', 'fas fa-tachometer-alt', NULL, NULL, 1, '2021-09-28 07:49:33', NULL, '2021-09-29 04:23:46', NULL, NULL, NULL),
	(4, 1, 3, '001:003', 'Report', 'Report', 'fa', 'fas fa-file-alt', NULL, NULL, 1, '2021-09-28 07:52:35', NULL, '2021-09-29 04:23:50', NULL, NULL, NULL);
/*!40000 ALTER TABLE `menu` ENABLE KEYS */;

-- Dumping structure for table u1543793_sicatat.pengguna
CREATE TABLE IF NOT EXISTS `pengguna` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) DEFAULT NULL,
  `reg_id` int(11) DEFAULT NULL,
  `username` varchar(255) CHARACTER SET latin1 NOT NULL,
  `password` varchar(255) CHARACTER SET latin1 NOT NULL,
  `nama` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `no_hp` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `_active` tinyint(4) NOT NULL DEFAULT 1,
  `_created_at` timestamp NULL DEFAULT current_timestamp(),
  `_created_by` int(11) DEFAULT NULL,
  `_updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `_updated_by` int(11) DEFAULT NULL,
  `_deleted_at` timestamp NULL DEFAULT NULL,
  `_deleted_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table u1543793_sicatat.pengguna: ~2 rows (approximately)
/*!40000 ALTER TABLE `pengguna` DISABLE KEYS */;
INSERT INTO `pengguna` (`id`, `role_id`, `reg_id`, `username`, `password`, `nama`, `no_hp`, `_active`, `_created_at`, `_created_by`, `_updated_at`, `_updated_by`, `_deleted_at`, `_deleted_by`) VALUES
	(1, 1, 1, 'admin', '$2y$11$T1kwFvfgOKMlQ7lzLxOkMeXCnB9/k.KUSmoFQKlWO42k9blkm08iK', 'admin', '085854445232', 1, '2021-02-01 06:55:26', NULL, '2021-02-02 03:59:04', NULL, NULL, NULL),
	(52, 1, 1, 'commandcenter', '$2y$11$T1kwFvfgOKMlQ7lzLxOkMeXCnB9/k.KUSmoFQKlWO42k9blkm08iK', 'commandcenter', NULL, 1, '2021-02-19 06:08:32', NULL, '2021-02-19 06:10:59', NULL, NULL, NULL);
/*!40000 ALTER TABLE `pengguna` ENABLE KEYS */;

-- Dumping structure for table u1543793_sicatat.pj_detail
CREATE TABLE IF NOT EXISTS `pj_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `aplikasi_id` int(11) DEFAULT NULL,
  `nama_pj` varchar(150) DEFAULT NULL,
  `NIP` varchar(30) DEFAULT NULL,
  `ref_opd_id` int(11) DEFAULT NULL,
  `alamat_opd` varchar(250) DEFAULT NULL,
  `provinsi` varchar(30) DEFAULT NULL,
  `kota` varchar(30) DEFAULT NULL,
  `kodepos` int(11) DEFAULT NULL,
  `no_hp` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table u1543793_sicatat.pj_detail: ~0 rows (approximately)
/*!40000 ALTER TABLE `pj_detail` DISABLE KEYS */;
/*!40000 ALTER TABLE `pj_detail` ENABLE KEYS */;

-- Dumping structure for table u1543793_sicatat.ref_dasarhukum
CREATE TABLE IF NOT EXISTS `ref_dasarhukum` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(200) DEFAULT NULL,
  `skor` tinyint(4) DEFAULT NULL,
  `_active` tinyint(4) NOT NULL DEFAULT 1,
  `_created_at` timestamp NULL DEFAULT current_timestamp(),
  `_created_by` int(11) DEFAULT NULL,
  `_updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `_updated_by` int(11) DEFAULT NULL,
  `_deleted_at` timestamp NULL DEFAULT NULL,
  `_deleted_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- Dumping data for table u1543793_sicatat.ref_dasarhukum: ~6 rows (approximately)
/*!40000 ALTER TABLE `ref_dasarhukum` DISABLE KEYS */;
INSERT INTO `ref_dasarhukum` (`id`, `nama`, `skor`, `_active`, `_created_at`, `_created_by`, `_updated_at`, `_updated_by`, `_deleted_at`, `_deleted_by`) VALUES
	(1, 'Tidak ada atau belum ada', 1, 1, '2021-09-29 01:21:07', NULL, '2021-09-29 01:21:07', NULL, NULL, NULL),
	(2, 'Ada tetapi masih dalam rancangan atau konsep atau SE', 2, 1, '2021-09-29 01:53:14', NULL, '2021-09-29 01:53:14', NULL, NULL, NULL),
	(3, 'Ada dari pereaturan pusat tetapi belum ada kebijakan tingkat daerah', 3, 1, '2021-09-29 01:53:40', NULL, '2021-09-29 01:53:40', NULL, NULL, NULL),
	(4, 'Sudah tersedia kebijakan tingkat daerah yang merupakan turunan kebijakan dari pusat', 4, 1, '2021-09-29 01:54:27', NULL, '2021-09-29 01:54:34', NULL, NULL, NULL),
	(5, 'Sudah tersedia kebijakan tingkat daerah yang merupakan turunan kebijakan dari pusat. Kebijakan sudah mengalami beberapa kali evaluasi', 5, 1, '2021-09-29 01:54:27', NULL, '2021-09-29 01:56:19', NULL, NULL, NULL);
/*!40000 ALTER TABLE `ref_dasarhukum` ENABLE KEYS */;

-- Dumping structure for table u1543793_sicatat.ref_dokumen
CREATE TABLE IF NOT EXISTS `ref_dokumen` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(150) DEFAULT NULL,
  `skor` tinyint(4) DEFAULT NULL,
  `_active` tinyint(4) NOT NULL DEFAULT 1,
  `_created_at` timestamp NULL DEFAULT current_timestamp(),
  `_created_by` int(11) DEFAULT NULL,
  `_updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `_updated_by` int(11) DEFAULT NULL,
  `_deleted_at` timestamp NULL DEFAULT NULL,
  `_deleted_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table u1543793_sicatat.ref_dokumen: ~4 rows (approximately)
/*!40000 ALTER TABLE `ref_dokumen` DISABLE KEYS */;
INSERT INTO `ref_dokumen` (`id`, `nama`, `skor`, `_active`, `_created_at`, `_created_by`, `_updated_at`, `_updated_by`, `_deleted_at`, `_deleted_by`) VALUES
	(1, 'Tidak ada petunjuk penggunaan aplikasi dan dokumentasi teknis', 1, 1, '2021-09-28 08:31:29', NULL, '2021-09-28 08:31:29', NULL, NULL, NULL),
	(2, 'Terdapat petunjuk penggunaan aplikasi', 3, 1, '2021-09-28 08:31:48', NULL, '2021-09-28 08:31:48', NULL, NULL, NULL),
	(3, 'Terdapat dokumen teknis', 4, 1, '2021-09-28 08:32:08', NULL, '2021-09-28 08:32:08', NULL, NULL, NULL),
	(4, 'Terdapat petunjuk penggunaan aplikasi dan dokumentasi teknis', 5, 1, '2021-09-28 08:32:31', NULL, '2021-09-28 08:32:31', NULL, NULL, NULL);
/*!40000 ALTER TABLE `ref_dokumen` ENABLE KEYS */;

-- Dumping structure for table u1543793_sicatat.ref_evaluasi
CREATE TABLE IF NOT EXISTS `ref_evaluasi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(200) DEFAULT NULL,
  `skor` tinyint(4) DEFAULT NULL,
  `_active` tinyint(4) NOT NULL DEFAULT 1,
  `_created_at` timestamp NULL DEFAULT current_timestamp(),
  `_created_by` int(11) DEFAULT NULL,
  `_updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `_updated_by` int(11) DEFAULT NULL,
  `_deleted_at` timestamp NULL DEFAULT NULL,
  `_deleted_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table u1543793_sicatat.ref_evaluasi: ~3 rows (approximately)
/*!40000 ALTER TABLE `ref_evaluasi` DISABLE KEYS */;
INSERT INTO `ref_evaluasi` (`id`, `nama`, `skor`, `_active`, `_created_at`, `_created_by`, `_updated_at`, `_updated_by`, `_deleted_at`, `_deleted_by`) VALUES
	(1, 'Jarang / Tidak pernah dilakukan evaluasi layanan secara terencana oleh manajemen (minimal eselon 3)', 1, 1, '2021-09-28 08:21:30', NULL, '2021-10-14 01:23:48', NULL, NULL, NULL),
	(2, 'Dilakukan evaluasi layanan secara terencana oleh manajemen (minimal eselon 3) minimal 1 kali dalam setahun', 3, 1, '2021-09-28 08:22:24', NULL, '2021-10-14 01:24:15', NULL, NULL, NULL),
	(3, 'Dilakukan evaluasi layanan secara terencana oleh manajemen (minimal eselon 3) lebih dari 1 kali dalam setahun', 5, 1, '2021-09-28 08:23:07', NULL, '2021-10-14 01:24:55', NULL, NULL, NULL);
/*!40000 ALTER TABLE `ref_evaluasi` ENABLE KEYS */;

-- Dumping structure for table u1543793_sicatat.ref_frekuensipemeliharaan
CREATE TABLE IF NOT EXISTS `ref_frekuensipemeliharaan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(150) DEFAULT NULL,
  `skor` tinyint(4) DEFAULT NULL,
  `_active` tinyint(4) NOT NULL DEFAULT 1,
  `_created_at` timestamp NULL DEFAULT current_timestamp(),
  `_created_by` int(11) DEFAULT NULL,
  `_updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `_updated_by` int(11) DEFAULT NULL,
  `_deleted_at` timestamp NULL DEFAULT NULL,
  `_deleted_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table u1543793_sicatat.ref_frekuensipemeliharaan: ~3 rows (approximately)
/*!40000 ALTER TABLE `ref_frekuensipemeliharaan` DISABLE KEYS */;
INSERT INTO `ref_frekuensipemeliharaan` (`id`, `nama`, `skor`, `_active`, `_created_at`, `_created_by`, `_updated_at`, `_updated_by`, `_deleted_at`, `_deleted_by`) VALUES
	(1, 'Jarang / Tidak pernah terjadi ada pemeliharaan yang terencana', 1, 1, '2021-09-28 08:17:58', NULL, '2021-09-28 08:17:58', NULL, NULL, NULL),
	(2, 'Pemeliharaan 1 kali dalam setahun', 3, 1, '2021-09-28 08:18:23', NULL, '2021-09-28 08:18:23', NULL, NULL, NULL),
	(3, 'Pemeliharaan lebih dari 1 kali dalam setahun (dalam bulan)', 5, 1, '2021-09-28 08:18:50', NULL, '2021-09-28 08:18:50', NULL, NULL, NULL);
/*!40000 ALTER TABLE `ref_frekuensipemeliharaan` ENABLE KEYS */;

-- Dumping structure for table u1543793_sicatat.ref_jenisaplikasi
CREATE TABLE IF NOT EXISTS `ref_jenisaplikasi` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) DEFAULT NULL,
  `skor` tinyint(4) DEFAULT NULL,
  `_active` tinyint(4) DEFAULT 1,
  `_created_at` timestamp NULL DEFAULT current_timestamp(),
  `_created_by` int(11) DEFAULT NULL,
  `_updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `_updated_by` int(11) DEFAULT NULL,
  `_deleted_at` timestamp NULL DEFAULT NULL,
  `_deleted_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table u1543793_sicatat.ref_jenisaplikasi: ~2 rows (approximately)
/*!40000 ALTER TABLE `ref_jenisaplikasi` DISABLE KEYS */;
INSERT INTO `ref_jenisaplikasi` (`id`, `nama`, `skor`, `_active`, `_created_at`, `_created_by`, `_updated_at`, `_updated_by`, `_deleted_at`, `_deleted_by`) VALUES
	(1, 'Khusus', 3, 1, '2021-09-22 14:15:16', 0, '2021-09-29 03:19:14', NULL, NULL, NULL),
	(2, 'Umum', 5, 1, '2021-09-22 14:16:06', 0, '2021-09-22 14:16:11', NULL, NULL, NULL);
/*!40000 ALTER TABLE `ref_jenisaplikasi` ENABLE KEYS */;

-- Dumping structure for table u1543793_sicatat.ref_kategoriakses
CREATE TABLE IF NOT EXISTS `ref_kategoriakses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(150) DEFAULT NULL,
  `skor` tinyint(4) DEFAULT NULL,
  `_active` tinyint(4) NOT NULL DEFAULT 1,
  `_created_at` timestamp NULL DEFAULT current_timestamp(),
  `_created_by` int(11) DEFAULT NULL,
  `_updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `_updated_by` int(11) DEFAULT NULL,
  `_deleted_at` timestamp NULL DEFAULT NULL,
  `_deleted_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table u1543793_sicatat.ref_kategoriakses: ~2 rows (approximately)
/*!40000 ALTER TABLE `ref_kategoriakses` DISABLE KEYS */;
INSERT INTO `ref_kategoriakses` (`id`, `nama`, `skor`, `_active`, `_created_at`, `_created_by`, `_updated_at`, `_updated_by`, `_deleted_at`, `_deleted_by`) VALUES
	(1, 'Melalui jaringan intra pemerintah ', 2, 1, '2021-09-29 03:32:44', NULL, '2021-09-29 03:32:44', NULL, NULL, NULL),
	(2, 'Internet (jalur publik)', 5, 1, '2021-09-29 03:33:03', NULL, '2021-09-29 03:33:03', NULL, NULL, NULL);
/*!40000 ALTER TABLE `ref_kategoriakses` ENABLE KEYS */;

-- Dumping structure for table u1543793_sicatat.ref_layananpublik
CREATE TABLE IF NOT EXISTS `ref_layananpublik` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(200) DEFAULT NULL,
  `skor` int(11) DEFAULT NULL,
  `_active` tinyint(4) NOT NULL DEFAULT 1,
  `_created_at` timestamp NULL DEFAULT current_timestamp(),
  `_created_by` int(11) DEFAULT NULL,
  `_updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `_updated_by` int(11) DEFAULT NULL,
  `_deleted_at` timestamp NULL DEFAULT NULL,
  `_deleted_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table u1543793_sicatat.ref_layananpublik: ~2 rows (approximately)
/*!40000 ALTER TABLE `ref_layananpublik` DISABLE KEYS */;
INSERT INTO `ref_layananpublik` (`id`, `nama`, `skor`, `_active`, `_created_at`, `_created_by`, `_updated_at`, `_updated_by`, `_deleted_at`, `_deleted_by`) VALUES
	(1, 'Ya', 5, 1, '2021-10-13 01:48:21', NULL, '2021-10-13 01:48:26', NULL, NULL, NULL),
	(2, 'Tidak', 1, 1, '2021-10-13 01:48:35', NULL, '2021-10-13 01:48:37', NULL, NULL, NULL);
/*!40000 ALTER TABLE `ref_layananpublik` ENABLE KEYS */;

-- Dumping structure for table u1543793_sicatat.ref_lokasi
CREATE TABLE IF NOT EXISTS `ref_lokasi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(150) DEFAULT NULL,
  `skor` tinyint(4) DEFAULT NULL,
  `_active` tinyint(4) NOT NULL DEFAULT 1,
  `_created_at` timestamp NULL DEFAULT current_timestamp(),
  `_created_by` int(11) DEFAULT NULL,
  `_updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `_updated_by` int(11) DEFAULT NULL,
  `_deleted_at` timestamp NULL DEFAULT NULL,
  `_deleted_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table u1543793_sicatat.ref_lokasi: ~2 rows (approximately)
/*!40000 ALTER TABLE `ref_lokasi` DISABLE KEYS */;
INSERT INTO `ref_lokasi` (`id`, `nama`, `skor`, `_active`, `_created_at`, `_created_by`, `_updated_at`, `_updated_by`, `_deleted_at`, `_deleted_by`) VALUES
	(1, 'VPS', 1, 1, '2021-09-29 03:28:02', NULL, '2021-09-29 03:28:04', NULL, NULL, NULL),
	(2, 'Dedicated', 5, 1, '2021-09-29 03:28:16', NULL, '2021-09-29 03:28:16', NULL, NULL, NULL);
/*!40000 ALTER TABLE `ref_lokasi` ENABLE KEYS */;

-- Dumping structure for table u1543793_sicatat.ref_opd
CREATE TABLE IF NOT EXISTS `ref_opd` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(250) DEFAULT NULL,
  `_active` tinyint(4) NOT NULL DEFAULT 1,
  `_created_at` timestamp NULL DEFAULT current_timestamp(),
  `_created_by` int(11) DEFAULT NULL,
  `_updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `_updated_by` int(11) DEFAULT NULL,
  `_deleted_at` timestamp NULL DEFAULT NULL,
  `_deleted_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=97 DEFAULT CHARSET=latin1;

-- Dumping data for table u1543793_sicatat.ref_opd: ~94 rows (approximately)
/*!40000 ALTER TABLE `ref_opd` DISABLE KEYS */;
INSERT INTO `ref_opd` (`id`, `nama`, `_active`, `_created_at`, `_created_by`, `_updated_at`, `_updated_by`, `_deleted_at`, `_deleted_by`) VALUES
	(1, 'BAGIAN PEMERINTAHAN', 1, '2021-10-06 13:16:19', NULL, '2021-10-06 13:16:19', NULL, NULL, NULL),
	(2, 'BAGIAN HUKUM', 1, '2021-10-06 13:16:19', NULL, '2021-10-06 13:16:19', NULL, NULL, NULL),
	(3, 'BAGIAN KESEJAHTERAAN RAKYAT', 1, '2021-10-06 13:16:19', NULL, '2021-10-06 13:16:19', NULL, NULL, NULL),
	(4, 'BAGIAN ADMINISTRASI PEMBANGUNAN', 1, '2021-10-06 13:16:19', NULL, '2021-10-06 13:16:19', NULL, NULL, NULL),
	(5, 'BAGIAN ADMINISTRASI PEREKONOMIAN', 1, '2021-10-06 13:16:19', NULL, '2021-10-06 13:16:19', NULL, NULL, NULL),
	(6, 'BAGIAN PENGADAAN BARANG/JASA', 1, '2021-10-06 13:16:19', NULL, '2021-10-06 13:16:19', NULL, NULL, NULL),
	(7, 'BAGIAN UMUM', 1, '2021-10-06 13:16:19', NULL, '2021-10-06 13:16:19', NULL, NULL, NULL),
	(8, 'BAGIAN ORGANISASI', 1, '2021-10-06 13:16:19', NULL, '2021-10-06 13:16:19', NULL, NULL, NULL),
	(9, 'BAGIAN PROTOKOL DAN KOMUNIKASI PIMPINAN', 1, '2021-10-06 13:16:19', NULL, '2021-10-06 13:16:19', NULL, NULL, NULL),
	(10, 'SEKRETARIS DPRD', 1, '2021-10-06 13:16:19', NULL, '2021-10-06 13:16:19', NULL, NULL, NULL),
	(11, 'INSPEKTORAT', 1, '2021-10-06 13:16:19', NULL, '2021-10-06 13:16:19', NULL, NULL, NULL),
	(12, 'SATUAN POLISI PAMONG PRAJA', 1, '2021-10-06 13:16:19', NULL, '2021-10-06 13:16:19', NULL, NULL, NULL),
	(13, 'BADAN PERENCANAAN, PEMBANGUNAN, PENELITIAN DAN PENGEMBANGAN', 1, '2021-10-06 13:16:19', NULL, '2021-10-06 13:16:19', NULL, NULL, NULL),
	(14, 'BADAN KEPEGAWAIAN, PENDIDIKAN DAN PELATIHAN DAERAH', 1, '2021-10-06 13:16:19', NULL, '2021-10-06 13:16:19', NULL, NULL, NULL),
	(15, 'DINAS PEMBERDAYAAN PEREMPUAN, PERLINDUNGAN ANAK, PENGENDALIAN PENDUDUK DAN KB', 1, '2021-10-06 13:16:19', NULL, '2021-10-06 13:16:19', NULL, NULL, NULL),
	(16, 'KANTOR KESATUAN BANGSA DAN POLITIK', 1, '2021-10-06 13:16:19', NULL, '2021-10-06 13:16:19', NULL, NULL, NULL),
	(17, 'DINAS LINGKUNGAN HIDUP, KEBERSIHAN, DAN PERTAMANAN', 1, '2021-10-06 13:16:19', NULL, '2021-10-06 13:16:19', NULL, NULL, NULL),
	(18, 'DINAS PENANAMAN MODAL DAN PELAYANAN TERPADU SATU PINTU', 1, '2021-10-06 13:16:19', NULL, '2021-10-06 13:16:19', NULL, NULL, NULL),
	(19, 'DINAS KEARSIPAN DAN PERPUSTAKAAN', 1, '2021-10-06 13:16:19', NULL, '2021-10-06 13:16:19', NULL, NULL, NULL),
	(20, 'DINAS PENDIDIKAN', 1, '2021-10-06 13:16:19', NULL, '2021-10-06 13:16:19', NULL, NULL, NULL),
	(21, 'DINAS KESEHATAN', 1, '2021-10-06 13:16:19', NULL, '2021-10-06 13:16:19', NULL, NULL, NULL),
	(22, 'UPT PUSKESMAS PESANTREN I', 1, '2021-10-06 13:16:19', NULL, '2021-10-06 13:16:19', NULL, NULL, NULL),
	(23, 'UPT PUSKESMAS PESANTREN II', 1, '2021-10-06 13:16:19', NULL, '2021-10-06 13:16:19', NULL, NULL, NULL),
	(24, 'UPT PUSKESMAS CAMPUREJO', 1, '2021-10-06 13:16:19', NULL, '2021-10-06 13:16:19', NULL, NULL, NULL),
	(25, 'UPT PUSKESMAS SUKORAME', 1, '2021-10-06 13:16:19', NULL, '2021-10-06 13:16:19', NULL, NULL, NULL),
	(26, 'UPT PUSKESMAS KOTA WILAYAH UTARA', 1, '2021-10-06 13:16:19', NULL, '2021-10-06 13:16:19', NULL, NULL, NULL),
	(27, 'UPT PUSKESMAS KOTA WILAYAH SELATAN', 1, '2021-10-06 13:16:19', NULL, '2021-10-06 13:16:19', NULL, NULL, NULL),
	(28, 'UPT PUSKESMAS NGLETIH', 1, '2021-10-06 13:16:19', NULL, '2021-10-06 13:16:19', NULL, NULL, NULL),
	(29, 'UPT PUSKESMAS MRICAN', 1, '2021-10-06 13:16:19', NULL, '2021-10-06 13:16:19', NULL, NULL, NULL),
	(30, 'UPT PUSKESMAS BALOWERTI', 1, '2021-10-06 13:16:19', NULL, '2021-10-06 13:16:19', NULL, NULL, NULL),
	(31, 'UPT LABKESDA', 1, '2021-10-06 13:16:19', NULL, '2021-10-06 13:16:19', NULL, NULL, NULL),
	(32, 'RSUD KILISUCI', 1, '2021-10-06 13:16:19', NULL, '2021-10-06 13:16:19', NULL, NULL, NULL),
	(33, 'UOBK RSUD GAMBIRAN', 1, '2021-10-06 13:16:19', NULL, '2021-10-06 13:16:19', NULL, NULL, NULL),
	(34, 'DINAS KEPENDUDUKAN DAN PENCATATAN SIPIL', 1, '2021-10-06 13:16:19', NULL, '2021-10-06 13:16:19', NULL, NULL, NULL),
	(35, 'DINAS SOSIAL', 1, '2021-10-06 13:16:19', NULL, '2021-10-06 13:16:19', NULL, NULL, NULL),
	(36, 'DINAS PEKERJAAN UMUM DAN PENATAAN RUANG', 1, '2021-10-06 13:16:19', NULL, '2021-10-06 13:16:19', NULL, NULL, NULL),
	(37, 'DINAS PERHUBUNGAN', 1, '2021-10-06 13:16:19', NULL, '2021-10-06 13:16:19', NULL, NULL, NULL),
	(38, 'DINAS KEBUDAYAAN PARIWISATA KEPEMUDAAN DAN OLAHRAGA', 1, '2021-10-06 13:16:19', NULL, '2021-10-06 13:16:19', NULL, NULL, NULL),
	(39, 'DINAS KOPERASI UMTK DAN TENAGA KERJA', 1, '2021-10-06 13:16:19', NULL, '2021-10-06 13:16:19', NULL, NULL, NULL),
	(40, 'DINAS KETAHANAN PANGAN DAN PERTANIAN', 1, '2021-10-06 13:16:19', NULL, '2021-10-06 13:16:19', NULL, NULL, NULL),
	(41, 'DINAS PERDAGANGAN DAN PERINDUSTRIAN', 1, '2021-10-06 13:16:19', NULL, '2021-10-06 13:16:19', NULL, NULL, NULL),
	(42, 'KECAMATAN KOTA', 1, '2021-10-06 13:16:19', NULL, '2021-10-06 13:16:19', NULL, NULL, NULL),
	(43, 'KELURAHAN BALOWERTI', 1, '2021-10-06 13:16:19', NULL, '2021-10-06 13:16:19', NULL, NULL, NULL),
	(44, 'KELURAHAN BANJARAN', 1, '2021-10-06 13:16:19', NULL, '2021-10-06 13:16:19', NULL, NULL, NULL),
	(45, 'KELURAHAN DANDANGAN', 1, '2021-10-06 13:16:19', NULL, '2021-10-06 13:16:19', NULL, NULL, NULL),
	(46, 'KELURAHAN JAGALAN', 1, '2021-10-06 13:16:19', NULL, '2021-10-06 13:16:19', NULL, NULL, NULL),
	(47, 'KELURAHAN KALIOMBO', 1, '2021-10-06 13:16:19', NULL, '2021-10-06 13:16:19', NULL, NULL, NULL),
	(48, 'KELURAHAN KAMPUNGDALEM', 1, '2021-10-06 13:16:19', NULL, '2021-10-06 13:16:19', NULL, NULL, NULL),
	(49, 'KELURAHAN KEMASAN', 1, '2021-10-06 13:16:19', NULL, '2021-10-06 13:16:19', NULL, NULL, NULL),
	(50, 'KELURAHAN MANISRENGGO', 1, '2021-10-06 13:16:19', NULL, '2021-10-06 13:16:19', NULL, NULL, NULL),
	(51, 'KELURAHAN NGADIREJO', 1, '2021-10-06 13:16:19', NULL, '2021-10-06 13:16:19', NULL, NULL, NULL),
	(52, 'KELURAHAN NGRONGGO', 1, '2021-10-06 13:16:19', NULL, '2021-10-06 13:16:19', NULL, NULL, NULL),
	(53, 'KELURAHAN PAKELAN', 1, '2021-10-06 13:16:19', NULL, '2021-10-06 13:16:19', NULL, NULL, NULL),
	(54, 'KELURAHAN POCANAN', 1, '2021-10-06 13:16:19', NULL, '2021-10-06 13:16:19', NULL, NULL, NULL),
	(55, 'KELURAHAN REJOMULYO', 1, '2021-10-06 13:16:19', NULL, '2021-10-06 13:16:19', NULL, NULL, NULL),
	(56, 'KELURAHAN RINGINANOM', 1, '2021-10-06 13:16:19', NULL, '2021-10-06 13:16:19', NULL, NULL, NULL),
	(57, 'KELURAHAN SEMAMPIR', 1, '2021-10-06 13:16:19', NULL, '2021-10-06 13:16:19', NULL, NULL, NULL),
	(58, 'KELURAHAN SETONO GEDONG', 1, '2021-10-06 13:16:19', NULL, '2021-10-06 13:16:19', NULL, NULL, NULL),
	(59, 'KELURAHAN SETONO PANDE', 1, '2021-10-06 13:16:19', NULL, '2021-10-06 13:16:19', NULL, NULL, NULL),
	(60, 'KECAMATAN MOJOROTO', 1, '2021-10-06 13:16:19', NULL, '2021-10-06 13:16:19', NULL, NULL, NULL),
	(61, 'KELURAHAN BANDAR KIDUL', 1, '2021-10-06 13:16:19', NULL, '2021-10-06 13:16:19', NULL, NULL, NULL),
	(62, 'KELURAHAN BANDAR LOR', 1, '2021-10-06 13:16:19', NULL, '2021-10-06 13:16:19', NULL, NULL, NULL),
	(63, 'KELURAHAN BANJAR MLATI', 1, '2021-10-06 13:16:19', NULL, '2021-10-06 13:16:19', NULL, NULL, NULL),
	(64, 'KELURAHAN BUJEL', 1, '2021-10-06 13:16:19', NULL, '2021-10-06 13:16:19', NULL, NULL, NULL),
	(65, 'KELURAHAN CAMPUREJO', 1, '2021-10-06 13:16:19', NULL, '2021-10-06 13:16:19', NULL, NULL, NULL),
	(66, 'KELURAHAN DERMO', 1, '2021-10-06 13:16:19', NULL, '2021-10-06 13:16:19', NULL, NULL, NULL),
	(67, 'KELURAHAN GAYAM', 1, '2021-10-06 13:16:19', NULL, '2021-10-06 13:16:19', NULL, NULL, NULL),
	(68, 'KELURAHAN LIRBOYO', 1, '2021-10-06 13:16:19', NULL, '2021-10-06 13:16:19', NULL, NULL, NULL),
	(69, 'KELURAHAN MOJOROTO', 1, '2021-10-06 13:16:19', NULL, '2021-10-06 13:16:19', NULL, NULL, NULL),
	(70, 'KELURAHAN MRICAN', 1, '2021-10-06 13:16:19', NULL, '2021-10-06 13:16:19', NULL, NULL, NULL),
	(71, 'KELURAHAN NGAMPEL', 1, '2021-10-06 13:16:19', NULL, '2021-10-06 13:16:19', NULL, NULL, NULL),
	(72, 'KELURAHAN POJOK', 1, '2021-10-06 13:16:19', NULL, '2021-10-06 13:16:19', NULL, NULL, NULL),
	(73, 'KELURAHAN SUKORAME', 1, '2021-10-06 13:16:19', NULL, '2021-10-06 13:16:19', NULL, NULL, NULL),
	(74, 'KELURAHAN TAMANAN', 1, '2021-10-06 13:16:19', NULL, '2021-10-06 13:16:19', NULL, NULL, NULL),
	(75, 'KECAMATAN PESANTREN', 1, '2021-10-06 13:16:19', NULL, '2021-10-06 13:16:19', NULL, NULL, NULL),
	(76, 'KELURAHAN NGLETIH', 1, '2021-10-06 13:16:19', NULL, '2021-10-06 13:16:19', NULL, NULL, NULL),
	(77, 'KELURAHAN PAKUNDEN', 1, '2021-10-06 13:16:19', NULL, '2021-10-06 13:16:19', NULL, NULL, NULL),
	(78, 'KELURAHAN BANGSAL', 1, '2021-10-06 13:16:19', NULL, '2021-10-06 13:16:19', NULL, NULL, NULL),
	(79, 'KELURAHAN BANARAN', 1, '2021-10-06 13:16:19', NULL, '2021-10-06 13:16:19', NULL, NULL, NULL),
	(80, 'KELURAHAN BAWANG', 1, '2021-10-06 13:16:19', NULL, '2021-10-06 13:16:19', NULL, NULL, NULL),
	(81, 'KELURAHAN BETET', 1, '2021-10-06 13:16:19', NULL, '2021-10-06 13:16:19', NULL, NULL, NULL),
	(82, 'KELURAHAN BLABAK', 1, '2021-10-06 13:16:19', NULL, '2021-10-06 13:16:19', NULL, NULL, NULL),
	(83, 'KELURAHAN BURENGAN', 1, '2021-10-06 13:16:19', NULL, '2021-10-06 13:16:19', NULL, NULL, NULL),
	(84, 'KELURAHAN JAMSAREN', 1, '2021-10-06 13:16:19', NULL, '2021-10-06 13:16:19', NULL, NULL, NULL),
	(85, 'KELURAHAN KETAMI', 1, '2021-10-06 13:16:19', NULL, '2021-10-06 13:16:19', NULL, NULL, NULL),
	(86, 'KELURAHAN TOSAREN', 1, '2021-10-06 13:16:19', NULL, '2021-10-06 13:16:19', NULL, NULL, NULL),
	(87, 'KELURAHAN SINGONEGARAN', 1, '2021-10-06 13:16:19', NULL, '2021-10-06 13:16:19', NULL, NULL, NULL),
	(88, 'KELURAHAN TEMPUREJO', 1, '2021-10-06 13:16:19', NULL, '2021-10-06 13:16:19', NULL, NULL, NULL),
	(89, 'KELURAHAN PESANTREN', 1, '2021-10-06 13:16:19', NULL, '2021-10-06 13:16:19', NULL, NULL, NULL),
	(90, 'KELURAHAN TINALAN', 1, '2021-10-06 13:16:19', NULL, '2021-10-06 13:16:19', NULL, NULL, NULL),
	(91, 'BADAN PENANGGULANGAN BENCANA DAERAH', 1, '2021-10-06 13:16:19', NULL, '2021-10-06 13:16:19', NULL, NULL, NULL),
	(92, 'BADAN PENDAPATAN, PENGELOLA KEUANGAN & ASET DAERAH', 1, '2021-10-06 13:16:19', NULL, '2021-10-06 13:16:19', NULL, NULL, NULL),
	(93, 'DINAS KOMUNIKASI DAN INFORMATIKA', 1, '2021-10-06 13:16:19', NULL, '2021-10-06 13:16:19', NULL, NULL, NULL),
	(94, 'DINAS PERUMAHAN DAN KAWASAN PEMUKIMAN', 1, '2021-10-06 13:16:19', NULL, '2021-10-06 13:16:19', NULL, NULL, NULL),
	(95, 'PD PASAR', 1, '2021-10-20 12:13:37', NULL, '2021-10-20 12:13:37', NULL, NULL, NULL),
	(96, 'BAZNAS', 1, '2021-10-20 12:14:02', NULL, '2021-10-20 12:14:02', NULL, NULL, NULL);
/*!40000 ALTER TABLE `ref_opd` ENABLE KEYS */;

-- Dumping structure for table u1543793_sicatat.ref_pj
CREATE TABLE IF NOT EXISTS `ref_pj` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(150) DEFAULT NULL,
  `skor` tinyint(4) DEFAULT NULL,
  `_active` tinyint(4) NOT NULL DEFAULT 1,
  `_created_at` timestamp NULL DEFAULT current_timestamp(),
  `_created_by` int(11) DEFAULT NULL,
  `_updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `_updated_by` int(11) DEFAULT NULL,
  `_deleted_at` timestamp NULL DEFAULT NULL,
  `_deleted_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table u1543793_sicatat.ref_pj: ~3 rows (approximately)
/*!40000 ALTER TABLE `ref_pj` DISABLE KEYS */;
INSERT INTO `ref_pj` (`id`, `nama`, `skor`, `_active`, `_created_at`, `_created_by`, `_updated_at`, `_updated_by`, `_deleted_at`, `_deleted_by`) VALUES
	(1, 'Tidak ada', 1, 1, '2021-09-29 01:06:22', NULL, '2021-09-29 01:06:22', NULL, NULL, NULL),
	(2, 'Ada (setingkat staff)', 3, 1, '2021-09-29 01:06:45', NULL, '2021-09-29 01:06:45', NULL, NULL, NULL),
	(3, 'Ada (setingkat kasi/kaba)', 5, 1, '2021-09-29 01:07:02', NULL, '2021-09-29 01:07:02', NULL, NULL, NULL);
/*!40000 ALTER TABLE `ref_pj` ENABLE KEYS */;

-- Dumping structure for table u1543793_sicatat.ref_pjteknis
CREATE TABLE IF NOT EXISTS `ref_pjteknis` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(150) DEFAULT NULL,
  `skor` tinyint(4) DEFAULT NULL,
  `_active` tinyint(4) NOT NULL DEFAULT 1,
  `_created_at` timestamp NULL DEFAULT current_timestamp(),
  `_created_by` int(11) DEFAULT NULL,
  `_updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `_updated_by` int(11) DEFAULT NULL,
  `_deleted_at` timestamp NULL DEFAULT NULL,
  `_deleted_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table u1543793_sicatat.ref_pjteknis: ~3 rows (approximately)
/*!40000 ALTER TABLE `ref_pjteknis` DISABLE KEYS */;
INSERT INTO `ref_pjteknis` (`id`, `nama`, `skor`, `_active`, `_created_at`, `_created_by`, `_updated_at`, `_updated_by`, `_deleted_at`, `_deleted_by`) VALUES
	(1, 'Tidak ada', 1, 1, '2021-09-29 01:10:45', NULL, '2021-09-29 01:10:45', NULL, NULL, NULL),
	(2, 'Ada (1 Orang)', 3, 1, '2021-09-29 01:10:59', NULL, '2021-09-29 01:10:59', NULL, NULL, NULL),
	(3, 'Ada (lebih dari 1 orang)', 5, 1, '2021-09-29 01:11:17', NULL, '2021-09-29 01:11:17', NULL, NULL, NULL);
/*!40000 ALTER TABLE `ref_pjteknis` ENABLE KEYS */;

-- Dumping structure for table u1543793_sicatat.ref_sasaran
CREATE TABLE IF NOT EXISTS `ref_sasaran` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(200) DEFAULT NULL,
  `skor` tinyint(4) DEFAULT NULL,
  `_active` tinyint(4) NOT NULL DEFAULT 1,
  `_created_at` timestamp NULL DEFAULT current_timestamp(),
  `_created_by` int(11) DEFAULT NULL,
  `_updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `_updated_by` int(11) DEFAULT NULL,
  `_deleted_at` timestamp NULL DEFAULT NULL,
  `_deleted_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table u1543793_sicatat.ref_sasaran: ~4 rows (approximately)
/*!40000 ALTER TABLE `ref_sasaran` DISABLE KEYS */;
INSERT INTO `ref_sasaran` (`id`, `nama`, `skor`, `_active`, `_created_at`, `_created_by`, `_updated_at`, `_updated_by`, `_deleted_at`, `_deleted_by`) VALUES
	(1, 'Administrasi Pemerintahan (Internal OPD)', 3, 1, '2021-09-29 01:16:54', NULL, '2021-09-29 01:16:54', NULL, NULL, NULL),
	(2, 'Administrasi Pemerintahan (Antar OPD)', 4, 1, '2021-09-29 01:17:18', NULL, '2021-09-29 01:17:18', NULL, NULL, NULL),
	(3, 'Administrasi Pemerintahan (dengan Provinsi atau Pemerintah Pusat', 5, 1, '2021-09-29 01:17:56', NULL, '2021-09-29 01:17:56', NULL, NULL, NULL),
	(4, 'Layanan Publik', 5, 1, '2021-09-29 01:18:07', NULL, '2021-09-29 01:18:07', NULL, NULL, NULL);
/*!40000 ALTER TABLE `ref_sasaran` ENABLE KEYS */;

-- Dumping structure for table u1543793_sicatat.ref_sertf
CREATE TABLE IF NOT EXISTS `ref_sertf` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) DEFAULT NULL,
  `skor` int(11) DEFAULT NULL,
  `_active` tinyint(4) NOT NULL DEFAULT 1,
  `_created_at` timestamp NULL DEFAULT current_timestamp(),
  `_created_by` int(11) DEFAULT NULL,
  `_updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `_updated_by` int(11) DEFAULT NULL,
  `_deleted_at` timestamp NULL DEFAULT NULL,
  `_deleted_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table u1543793_sicatat.ref_sertf: ~2 rows (approximately)
/*!40000 ALTER TABLE `ref_sertf` DISABLE KEYS */;
INSERT INTO `ref_sertf` (`id`, `nama`, `skor`, `_active`, `_created_at`, `_created_by`, `_updated_at`, `_updated_by`, `_deleted_at`, `_deleted_by`) VALUES
	(1, 'Ya', 5, 1, '2021-10-13 01:45:18', NULL, '2021-10-13 01:45:20', NULL, NULL, NULL),
	(2, 'Tidak', 1, 1, '2021-10-13 01:45:31', NULL, '2021-10-13 01:45:33', NULL, NULL, NULL);
/*!40000 ALTER TABLE `ref_sertf` ENABLE KEYS */;

-- Dumping structure for table u1543793_sicatat.ref_sistempengaman
CREATE TABLE IF NOT EXISTS `ref_sistempengaman` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) DEFAULT NULL,
  `skor` tinyint(4) DEFAULT NULL,
  `_active` tinyint(4) NOT NULL DEFAULT 1,
  `_created_at` timestamp NULL DEFAULT current_timestamp(),
  `_created_by` int(11) DEFAULT NULL,
  `_updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `_updated_by` int(11) DEFAULT NULL,
  `_deleted_at` timestamp NULL DEFAULT NULL,
  `_deleted_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table u1543793_sicatat.ref_sistempengaman: ~3 rows (approximately)
/*!40000 ALTER TABLE `ref_sistempengaman` DISABLE KEYS */;
INSERT INTO `ref_sistempengaman` (`id`, `nama`, `skor`, `_active`, `_created_at`, `_created_by`, `_updated_at`, `_updated_by`, `_deleted_at`, `_deleted_by`) VALUES
	(1, 'Tidak terdapat sistem pengaman pada aplikasi', 1, 1, '2021-09-22 14:34:23', NULL, '2021-09-22 14:34:23', NULL, NULL, NULL),
	(2, 'Terdapat sistem pengaman data dengan menggunakan sertifikat digital yang disediakan oleh dinkominfo', 3, 1, '2021-09-22 14:35:25', NULL, '2021-09-22 14:35:25', NULL, NULL, NULL),
	(3, 'Terdapat sistem pengaman data dengan menggunakan sertifikat digital yang disediakan oleh dinkominfo dan sistem pengaman tingkat lanjut lainnya yang disesuaikan dengan kebutuhan', 5, 1, '2021-09-22 14:36:17', NULL, '2021-09-22 14:36:17', NULL, NULL, NULL);
/*!40000 ALTER TABLE `ref_sistempengaman` ENABLE KEYS */;

-- Dumping structure for table u1543793_sicatat.ref_tenagateknis
CREATE TABLE IF NOT EXISTS `ref_tenagateknis` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(200) DEFAULT NULL,
  `skor` tinyint(4) DEFAULT NULL,
  `_active` tinyint(4) NOT NULL DEFAULT 1,
  `_created_at` timestamp NULL DEFAULT current_timestamp(),
  `_created_by` int(11) DEFAULT NULL,
  `_updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `_updated_by` int(11) DEFAULT NULL,
  `_deleted_at` timestamp NULL DEFAULT NULL,
  `_deleted_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table u1543793_sicatat.ref_tenagateknis: ~5 rows (approximately)
/*!40000 ALTER TABLE `ref_tenagateknis` DISABLE KEYS */;
INSERT INTO `ref_tenagateknis` (`id`, `nama`, `skor`, `_active`, `_created_at`, `_created_by`, `_updated_at`, `_updated_by`, `_deleted_at`, `_deleted_by`) VALUES
	(1, 'Tidak ada', 1, 1, '2021-09-22 14:43:19', NULL, '2021-09-22 14:43:19', NULL, NULL, NULL),
	(2, 'Satu orang dari internal', 2, 1, '2021-09-22 14:43:39', NULL, '2021-09-22 14:43:39', NULL, NULL, NULL),
	(3, 'Satu orang dari penyedia', 3, 1, '2021-09-22 14:43:54', NULL, '2021-09-22 14:43:54', NULL, NULL, NULL),
	(4, 'Lebih dari satu orang dari internal', 4, 1, '2021-09-22 14:44:13', NULL, '2021-09-22 14:44:13', NULL, NULL, NULL),
	(5, 'Lebih dari satu orang gabungan dari internal dan penyedia', 5, 1, '2021-09-22 14:44:37', NULL, '2021-10-14 01:29:57', NULL, NULL, NULL);
/*!40000 ALTER TABLE `ref_tenagateknis` ENABLE KEYS */;

-- Dumping structure for table u1543793_sicatat.ref_threat
CREATE TABLE IF NOT EXISTS `ref_threat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(150) DEFAULT NULL,
  `skor` tinyint(4) DEFAULT NULL,
  `keterangan` varchar(200) DEFAULT NULL,
  `_active` tinyint(4) NOT NULL DEFAULT 1,
  `_created_at` timestamp NULL DEFAULT current_timestamp(),
  `_created_by` int(11) DEFAULT NULL,
  `_updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `_updated_by` int(11) DEFAULT NULL,
  `_deleted_at` timestamp NULL DEFAULT NULL,
  `_deleted_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- Dumping data for table u1543793_sicatat.ref_threat: ~6 rows (approximately)
/*!40000 ALTER TABLE `ref_threat` DISABLE KEYS */;
INSERT INTO `ref_threat` (`id`, `nama`, `skor`, `keterangan`, `_active`, `_created_at`, `_created_by`, `_updated_at`, `_updated_by`, `_deleted_at`, `_deleted_by`) VALUES
	(1, 'Rare', 1, 'Jarang/Tidak pernah terjadi pada jangka waktu lebih dari 1 tahun', 1, '2021-09-29 00:54:13', NULL, '2021-09-29 00:56:25', NULL, NULL, NULL),
	(2, 'Annually', 2, 'Dapat terjadi satu-dua kali dalam jangka waktu 1 tahun', 1, '2021-09-29 00:58:55', NULL, '2021-09-29 00:58:55', NULL, NULL, NULL),
	(3, 'Monthly', 3, 'Dapat terjadi satu-dua kali tiap satu atau beberapa bulan', 1, '2021-09-29 00:59:43', NULL, '2021-09-29 01:00:10', NULL, NULL, NULL),
	(4, 'Weekly', 4, 'Dapat terjadi satu-dua kali tiap satu atau beberapa minggu', 1, '2021-09-29 01:00:55', NULL, '2021-09-29 01:01:20', NULL, NULL, NULL),
	(5, 'Daily', 5, 'Dapat terjadi satu-dua kali tiap satu atau beberapa minggu', 1, '2021-09-29 01:02:03', NULL, '2021-09-29 01:02:32', NULL, NULL, NULL);
/*!40000 ALTER TABLE `ref_threat` ENABLE KEYS */;

-- Dumping structure for table u1543793_sicatat.ref_tingkatkematangan
CREATE TABLE IF NOT EXISTS `ref_tingkatkematangan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) DEFAULT NULL,
  `indikator` varchar(255) DEFAULT NULL,
  `skor` tinyint(4) DEFAULT NULL,
  `_active` tinyint(4) DEFAULT 1,
  `_created_at` timestamp NULL DEFAULT current_timestamp(),
  `_created_by` int(11) DEFAULT NULL,
  `_updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `_updated_by` int(11) DEFAULT NULL,
  `_deleted_at` timestamp NULL DEFAULT NULL,
  `_deleted_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table u1543793_sicatat.ref_tingkatkematangan: ~5 rows (approximately)
/*!40000 ALTER TABLE `ref_tingkatkematangan` DISABLE KEYS */;
INSERT INTO `ref_tingkatkematangan` (`id`, `nama`, `indikator`, `skor`, `_active`, `_created_at`, `_created_by`, `_updated_at`, `_updated_by`, `_deleted_at`, `_deleted_by`) VALUES
	(1, '1 - Informasi', 'Layanan SPBE diberikan dalam bentuk informasi satu arah', 1, 1, '2021-09-22 14:18:53', NULL, '2021-09-22 14:18:53', NULL, NULL, NULL),
	(2, '2 - Interaksi', 'Kriteria Tingkat informasi telah terpenuhi. Layanan SPBE diberikan dalam bentuk interaksi dua arah', 2, 1, '2021-09-22 14:19:58', NULL, '2021-09-22 14:19:58', NULL, NULL, NULL),
	(3, '3 - Transaksi', 'Kriteria tingkat interaksi telah terpenuhi. layanan SPBE diberikan melalui satu kesatuan transaksi operasi dengan menggunakan beberapa sumber daya SPBE', 3, 1, '2021-09-22 14:21:29', NULL, '2021-09-22 14:21:29', NULL, NULL, NULL),
	(4, '4 - Kolaborasi', 'Kriteria tingkat transaksi telah terpenuhi. Layanan SPBE diberikan melalui integrasi/kolaborasi dengan layanan SPBE lain.', 4, 1, '2021-09-22 14:22:31', NULL, '2021-09-22 14:22:31', NULL, NULL, NULL),
	(5, '5 - Optimum', 'Kriteria tingkat kolaborasi telah terpenuhi. Layanan SPBE telah dilakukan perbaikan dan peningkatan kualitas menyesuaikan perubahan kebutuhan di lingkungan internal dan eksternal.', 5, 1, '2021-09-22 14:23:45', NULL, '2021-09-22 14:23:45', NULL, NULL, NULL);
/*!40000 ALTER TABLE `ref_tingkatkematangan` ENABLE KEYS */;

-- Dumping structure for table u1543793_sicatat.role
CREATE TABLE IF NOT EXISTS `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `_active` tinyint(4) NOT NULL DEFAULT 1,
  `_created_at` timestamp NULL DEFAULT current_timestamp(),
  `_created_by` int(11) DEFAULT NULL,
  `_updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `_updated_by` int(11) DEFAULT NULL,
  `_deleted_at` timestamp NULL DEFAULT NULL,
  `_deleted_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table u1543793_sicatat.role: ~0 rows (approximately)
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` (`id`, `name`, `_active`, `_created_at`, `_created_by`, `_updated_at`, `_updated_by`, `_deleted_at`, `_deleted_by`) VALUES
	(1, 'admin', 1, '2021-02-01 06:56:08', NULL, '2021-02-01 06:56:08', NULL, NULL, NULL),
	(5, 'viewer', 1, '2021-09-29 02:12:38', NULL, '2021-09-29 02:12:38', NULL, NULL, NULL);
/*!40000 ALTER TABLE `role` ENABLE KEYS */;

-- Dumping structure for table u1543793_sicatat.role_x_menu
CREATE TABLE IF NOT EXISTS `role_x_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `_active` tinyint(4) NOT NULL DEFAULT 1,
  `_created_at` timestamp NULL DEFAULT current_timestamp(),
  `_created_by` int(11) DEFAULT NULL,
  `_updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `_updated_by` int(11) DEFAULT NULL,
  `_deleted_at` timestamp NULL DEFAULT NULL,
  `_deleted_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table u1543793_sicatat.role_x_menu: ~5 rows (approximately)
/*!40000 ALTER TABLE `role_x_menu` DISABLE KEYS */;
INSERT INTO `role_x_menu` (`id`, `role_id`, `menu_id`, `_active`, `_created_at`, `_created_by`, `_updated_at`, `_updated_by`, `_deleted_at`, `_deleted_by`) VALUES
	(1, 1, 4, 1, '2021-02-01 07:00:13', NULL, '2021-02-05 09:08:42', NULL, NULL, NULL),
	(2, 2, 1, 0, '2021-02-01 13:19:36', NULL, '2021-02-05 00:32:17', NULL, NULL, NULL),
	(4, 2, 2, 0, '2021-02-01 14:37:32', NULL, '2021-02-05 00:32:17', NULL, NULL, NULL),
	(5, 1, 5, 1, '2021-02-02 01:14:31', NULL, '2021-02-05 02:02:29', NULL, NULL, NULL),
	(6, 1, 6, 1, '2021-02-08 01:43:18', NULL, '2021-02-08 01:43:19', NULL, NULL, NULL);
/*!40000 ALTER TABLE `role_x_menu` ENABLE KEYS */;

-- Dumping structure for table u1543793_sicatat.tata_kelola
CREATE TABLE IF NOT EXISTS `tata_kelola` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `aplikasi_id` int(11) DEFAULT NULL,
  `ref_frekuensipemeliharaan_id` int(11) DEFAULT NULL,
  `ref_evaluasi_id` int(11) DEFAULT NULL,
  `ref_dokumen_id` int(11) DEFAULT NULL,
  `ref_pj_id` int(11) DEFAULT NULL,
  `ref_pjteknis_id` int(11) DEFAULT NULL,
  `_active` tinyint(4) NOT NULL DEFAULT 1,
  `_created_at` timestamp NULL DEFAULT current_timestamp(),
  `_created_by` int(11) DEFAULT NULL,
  `_updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `_updated_by` int(11) DEFAULT NULL,
  `_deleted_at` timestamp NULL DEFAULT NULL,
  `_deleted_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table u1543793_sicatat.tata_kelola: ~0 rows (approximately)
/*!40000 ALTER TABLE `tata_kelola` DISABLE KEYS */;
/*!40000 ALTER TABLE `tata_kelola` ENABLE KEYS */;

-- Dumping structure for table u1543793_sicatat.teknologi
CREATE TABLE IF NOT EXISTS `teknologi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `aplikasi_id` int(11) DEFAULT NULL,
  `ref_jenisaplikasi_id` int(11) DEFAULT NULL,
  `ref_tingkatkematangan_id` int(11) DEFAULT NULL,
  `ref_sistempengamanan_id` int(11) DEFAULT NULL,
  `ref_tenagateknis_id` int(11) DEFAULT NULL,
  `_active` tinyint(4) NOT NULL DEFAULT 1,
  `_created_at` timestamp NULL DEFAULT current_timestamp(),
  `_created_by` int(11) DEFAULT NULL,
  `_updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `_updated_by` int(11) DEFAULT NULL,
  `_deleted_at` timestamp NULL DEFAULT NULL,
  `_deleted_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table u1543793_sicatat.teknologi: ~0 rows (approximately)
/*!40000 ALTER TABLE `teknologi` DISABLE KEYS */;
/*!40000 ALTER TABLE `teknologi` ENABLE KEYS */;

-- Dumping structure for table u1543793_sicatat.upload
CREATE TABLE IF NOT EXISTS `upload` (
  `id` int(11) DEFAULT NULL,
  `aplikasi_id` int(11) DEFAULT NULL,
  `_active` tinyint(4) NOT NULL DEFAULT 1,
  `_created_at` timestamp NULL DEFAULT current_timestamp(),
  `_created_by` int(11) DEFAULT NULL,
  `_updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `_updated_by` int(11) DEFAULT NULL,
  `_deleted_at` timestamp NULL DEFAULT NULL,
  `_deleted_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table u1543793_sicatat.upload: ~0 rows (approximately)
/*!40000 ALTER TABLE `upload` DISABLE KEYS */;
/*!40000 ALTER TABLE `upload` ENABLE KEYS */;

-- Dumping structure for table u1543793_sicatat.versi
CREATE TABLE IF NOT EXISTS `versi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `aplikasi_id` int(11) DEFAULT NULL,
  `no_versi` varchar(50) DEFAULT NULL,
  `deskripsi` text DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `path_file` varchar(200) DEFAULT NULL,
  `_active` tinyint(4) NOT NULL DEFAULT 1,
  `_created_at` timestamp NULL DEFAULT current_timestamp(),
  `_created_by` int(11) DEFAULT NULL,
  `_updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `_updated_by` int(11) DEFAULT NULL,
  `_deleted_at` timestamp NULL DEFAULT NULL,
  `_deleted_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table u1543793_sicatat.versi: ~0 rows (approximately)
/*!40000 ALTER TABLE `versi` DISABLE KEYS */;
INSERT INTO `versi` (`id`, `aplikasi_id`, `no_versi`, `deskripsi`, `tanggal`, `path_file`, `_active`, `_created_at`, `_created_by`, `_updated_at`, `_updated_by`, `_deleted_at`, `_deleted_by`) VALUES
	(1, 1, '1.0.0', 'Update fitur', '2021-10-01', NULL, 1, '2021-10-25 07:08:10', NULL, '2021-10-25 07:08:10', NULL, NULL, NULL),
	(2, 1, '1.0.1', 'Tambahan Fitur', '2021-10-02', NULL, 1, '2021-10-25 07:28:42', NULL, '2021-10-25 07:28:42', NULL, NULL, NULL);
/*!40000 ALTER TABLE `versi` ENABLE KEYS */;

/*!40103 SET TIME_ZONE=IFNULL(@OLD_TIME_ZONE, 'system') */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
