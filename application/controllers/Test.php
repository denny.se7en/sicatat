<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Test extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function index()
    {
        $datasebaran = $this->db->select('r.id as reg_id, k.nama_kel, s.riwayat_perjalanan, s.konfirmasi, s.suspect, s.sembuh, s.meninggal, s.tanggal')
            ->from('ref_kelurahan k')
            ->join('regional r', 'r.no_kec = k.no_kec and r.no_kel = k.no_kel', 'left')
            ->join('persebaran s', 's.kelurahan_id = k.id', 'left')
            ->get()
            ->result();

        $kelurahan = $this->db->select('k.id, r.id as reg_id, k.nama_kel')
            ->from('ref_kelurahan k')
            ->join('regional r', 'r.no_kec = k.no_kec and r.no_kel = k.no_kel', 'left')
            ->get()
            ->result();


        $sebaran = $this->db->from('persebaran')
            ->where(['_isactive'=>'1'])
            ->get()
            ->result();

        $tgl_1 = date("Y-m-d",strtotime("-7 days"));
        //$tgl_11 = date("Y-m-d",strtotime($this->input->post("add_tanggal_awal")." +1 day")); //untuk mengubah tanggal input jadi -1 hari agar perhitungan kasus menjadi benar
        $tgl_2 = date("Y-m-d");

        $tgl_diff1 = date("Y-m-d", strtotime("-14 days"));
        //$tgl_diff11 = date("Y-m-d",strtotime($this->input->post("add_tanggal_awal")." -6 day")); //untuk mengubah tanggal input jadi -1 hari agar perhitungan kasus menjadi benar
        $tgl_diff2 = date("Y-m-d",strtotime("-7 days"));

        $data['sebaran'] = $sebaran;

        $refcvd = $this->db->from('ref_kategoricvd')
            ->where(['_active' => '1'])
            ->get()
            ->result();

        $master_1 = [];
        $covid_1 = [];
        $sebelum = [];
        $ppkm_1 = [];
        $data_kel = [];

        foreach ($kelurahan as $kel) {

            
            $data_kel['reg_id'] = $kel->reg_id;
            $data_kel['nama_kel'] = $kel->nama_kel;
            $ppkm = $this->db->select("nama_kel,
                coalesce(sum(case when tanggal between '".$tgl_1."' and '".$tgl_2."' then _konfirmasi end),0) as konfirmasi,
                coalesce(sum(case when tanggal between '".$tgl_1."' and '".$tgl_2."' then _sembuh end),0) as sembuh,
                coalesce(sum(case when tanggal between '".$tgl_1."' and '".$tgl_2."' then _meninggal end),0) as meninggal,
                coalesce(coalesce(sum(case when tanggal between '".$tgl_1."' and '".$tgl_2."' then _konfirmasi end),0) / if(coalesce(sum(case when tanggal between '".$tgl_diff1."' and '".$tgl_diff2."' then _konfirmasi end),1)<1,1,coalesce(sum(case when tanggal between '".$tgl_diff1."' and '".$tgl_diff2."' then _konfirmasi end),1)),0) * 100 as kasus_baru")
                ->from('persebaran p')
                ->join('ref_kelurahan kel', 'p.kelurahan_id = kel.id', 'left')
                ->where(['kelurahan_id' => $kel->id,'_isactive' =>1])
                ->get()
                ->row();
            $ppkm_1['data_kel'] = $data_kel;
            $ppkm_1['konfirmasi'] = intval($ppkm->konfirmasi);
            $ppkm_1['sembuh'] = intval($ppkm->sembuh);
            $ppkm_1['meninggal'] = intval($ppkm->meninggal);
            $ppkm_1['kasus_baru'] = intval($ppkm->kasus_baru);
            $covid_1['data_kel'] = $data_kel;
            $covid_1['sebaran'] = $ppkm_1;
            $master_1[$kel->id] = [$covid_1];
        }
        
        $data = [
            'css_files' => [
                'assets/leaflet/leaflet.css'
            ],
            'pra_ppkm' => $master_1,
            'kelurahan' => $kelurahan,
            'refcvd' => $refcvd,
            'tgl_1' => $tgl_1,
            'tgl_2' => $tgl_2,
            'tgl_diff1' => $tgl_diff1,
            'tgl_diff2' => $tgl_diff2
        ];

        $this->template->fullscreen('v_test', $data);
    }
}
