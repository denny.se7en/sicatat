<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/* load the MX_Router class */
require APPPATH . "third_party/MX/Controller.php";

/**
 * Description of my_controller
 *
 * @author Administrator
 */
class MY_Controller extends MX_Controller
{

    function __construct()
    {
        parent::__construct();
        // $this->_guard();

        $this->halocovid->is_token_active();
        
        if (version_compare(CI_VERSION, '2.1.0', '<')) {
            $this->load->library('security');
        }
    }

    private function _guard()
    {

        // $usertoken = $this->session->usertoken;

        $endpoint = $this->db->from('endpoint')->where(['path' => uri_string(), '_active' => 1])->get()->row();

        if ($endpoint) {
            if ($this->session) {
                if ($this->session->logged_in) {
                    if (!$endpoint->in_session) {
                        redirect('main');
                    }
                } else {
                    if (!$endpoint->out_session) {
                        redirect('auth/login');
                    }
                }
            } else {
                if (!$endpoint->out_session) {
                    redirect('auth/login');
                }
            }
        }
    }
}

/* End of file MY_Controller.php */
/* Location: ./application/core/MY_Controller.php */