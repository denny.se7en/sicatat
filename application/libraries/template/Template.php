<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Template
{
    protected $CI;

    public function __construct()
    {
        $this->CI = &get_instance();
    }

    public function admin($view, $data = [])
    {
        $defaults = [
            'css_files' => [
                'assets/plugins/fontawesome-free/css/all.min.css',
                'assets/plugins/overlayScrollbars/css/OverlayScrollbars.min.css',
                'assets/css/adminlte.min.css',
                'assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css',
                'assets/plugins/chart.js/Chart.min.css',
                'assets/plugins/select2/css/select2.min.css',
                'assets/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css',
                'https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700',
                'https://fonts.googleapis.com/css?family=Material+Icons|Material+Icons+Outlined|Material+Icons+Two+Tone|Material+Icons+Round|Material+Icons+Sharp',
                'https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css',
                'https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css'
            ],
            'js_top'    => [
                'assets/plugins/jquery/jquery.min.js',
                'assets/plugins/jquery-knob/jquery.knob.min.js',
                'assets/leaflet/leaflet.js',
                'assets/plugins/chart.js/Chart.min.js',
                'assets/plugins/jquery/jquery.min.js',
                'assets/plugins/select2/js/select2.full.min.js'
            ],
            'js_files'  => [
                'assets/plugins/bootstrap/js/bootstrap.bundle.min.js',
                'assets/js/adminlte.js',
                'assets/my/my-graph.js',
                'assets/plugins/datatables/jquery.dataTables.min.js',
                'assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js',
                'assets/plugins/datatables-responsive/js/dataTables.responsive.min.js',
                'assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js',
                'https://cdn.jsdelivr.net/npm/sweetalert2@9',
                'https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js',
                'https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js',
                'https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js',
                'https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js',
                'https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js',
                'https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js',
                'https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js'
            ],
            'metadata'  => ['a'],
            'menus'     => $this->_get_menu(),
            'badge'     => $this->_get_badge(),
            'navbar'    => true,
            'header'    => true,
            'padding'   => true
        ];

        // echo json_encode($defaults); die();
        $data = $this->_merge_data($defaults, $data);

        $data['page'] = $view != '' ? $view : 'errors/html/error_404';
        // echo json_encode($data);die();
        $this->CI->load->view('template/admin/_admin_index', $data);
    }

    public function fullscreen($view, $data = [])
    {
        $defaults = [
            'css_files' => [
                'assets/plugins/fontawesome-free/css/all.min.css',
                'assets/plugins/overlayScrollbars/css/OverlayScrollbars.min.css',
                'assets/css/adminlte.min.css',
                'assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css',
                'assets/plugins/chart.js/Chart.min.css',
                'https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700',
                'https://fonts.googleapis.com/css?family=Material+Icons|Material+Icons+Outlined|Material+Icons+Two+Tone|Material+Icons+Round|Material+Icons+Sharp',
                'https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css',
                'https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css'
            ],
            'js_top'    => [
                'assets/plugins/jquery/jquery.min.js',
                'assets/leaflet/leaflet.js',
                'assets/leaflet/leaflet.ajax.js',
                'assets/plugins/chart.js/Chart.min.js',
                'assets/plugins/jquery-knob/jquery.knob.min.js',
            ],
            'js_files'  => [
                'assets/plugins/bootstrap/js/bootstrap.bundle.min.js',
                'assets/js/adminlte.js',
                'assets/my/my-graph.js',
                'assets/plugins/datatables/jquery.dataTables.min.js',
                'assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js',
                'assets/plugins/datatables-responsive/js/dataTables.responsive.min.js',
                'assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js',
                'https://cdn.jsdelivr.net/npm/sweetalert2@9',
                'https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js',
                'https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js',
                'https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js',
                'https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js',
                'https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js',
                'https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js',
                'https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js'
            ],
            'metadata'  => ['a'],
            'menus'     => $this->_get_menu(),
            'badge'     => $this->_get_badge(),
            'navbar'    => true,
            'header'    => true,
            'padding'   => true
        ];

        // echo json_encode($defaults); die();
        $data = $this->_merge_data($defaults, $data);

        $data['page'] = $view != '' ? $view : 'errors/html/error_404';
        // echo json_encode($data);die();
        $this->CI->load->view('template/fullscreen/_fullscreen_index', $data);
    }

    private function _merge_data($defaults, $data)
    {
        foreach ($data as $key => $value) {
            if (in_array($key, ['css_files', 'js_files', 'metadata'])) {
                $defaults[$key] = array_merge($defaults[$key], $data[$key]);
                unset($data[$key]);
            }
        }
        $defaults = array_merge($defaults, $data);

        return $defaults;
    }

    private function _get_menu($parent = 0)
    {
        $menus = $this->CI->db->select('m.*')
            ->from('menu m')
            ->where([
                'm.parent'    => $parent,
                'm._active'   => 1,
                // 'rxm._active' => 1,
                //'r.id'        => $this->CI->session->role_id
            ])
            ->order_by('m.code')
            ->get()
            ->result();
        foreach ($menus as $index => $menu) {
            $menu->child = $this->_get_menu($menu->id);
        }
        
        return $menus;
    }

    private function _get_badge()
    {
        $badge = [
            'pesan'     => 100,
            'pesana'    => 99,
        ];

        return $badge;
    }
}
