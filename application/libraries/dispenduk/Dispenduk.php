<?php

class Dispenduk
{
    protected $CI;

    public function __construct()
    {
        $this->CI = &get_instance();
        $this->CI->load->helper('array');
    }

    public function get_data_penduduk($nik, $display = ['nama_lengkap'])
    {

        // $url        = 'http://10.100.50.173:8000';
        $url        = 'http://api.kedirikota.go.id/dispendukcapil';
        $metode     = 'CALL_NIK';

        $option     = '3';
        $user_id    = 'AANKOMINFO';
        $password   = '1kmfok3dir1';
        $instansi   = 'KOMINFO';
        $ip_address = 'localhost';
        $ip_user    = 'localhost';

        $data = array(
            "nik" => $nik,
            "option" => $option,
            "user_id" => $user_id,
            "password" => $password,
            "instansi" => $instansi,
            "ip_address" => $ip_address,
            "ip_user" => $ip_user,
        );

        $data_string = json_encode($data);
        $url = $url . "/" . $instansi . "/" . $metode;
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        //curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt(
            $ch,
            CURLOPT_HTTPHEADER,
            array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string)
            )
        );

        $hasil = curl_exec($ch);

        if ($hasil == false) {
            $error_msg = "Maaf, sedang terjadi ganguan. Mohon dicoba beberapa saat lagi.";
            return ['valid' => FALSE, 'data' => [], 'error' => $error_msg];
        } else {
            $tmp = $hasil;
            $hasil_data = json_decode($hasil, true, 512);
            $hasil_data['json'] = $tmp;
            $ktps = json_decode($hasil, true);
        }

        curl_close($ch);

        $valid      = false;
        $data_ktp   = [];
        $data_kk   = [];
        $nama_kk = "";


        foreach ($ktps['content'] as $ktp) {
            if ($ktp['STAT_HBKEL'] == "KEPALA KELUARGA") {
                $data_kk = $ktp;
                $nama_kk = $data_kk['NAMA_LGKP'];
                break;
            }
        }


        foreach ($ktps['content'] as $ktp) {
            if (isset($ktp["NIK"]) && $ktp['NIK'] == $nik) {
                $valid = TRUE;
                $data_ktp = $ktp;
                break;
            }
        }

        if ($valid) {
            $data_ktp = [
                'nik'           =>  $data_ktp['NIK'],
                'no_kk'         =>  $data_ktp['NO_KK'],
                'nama_lengkap'  =>  $data_ktp['NAMA_LGKP'],
                'agama'         =>  $data_ktp['AGAMA'],
                'tempat_lahir'  =>  $data_ktp['TMPT_LHR'],
                'tanggal_lahir' =>  $data_ktp['TGL_LHR'],
                'pekerjaan'     =>  $data_ktp['JENIS_PKRJN'],
                'pendidikan'    =>  $data_ktp['PDDK_AKH'],
                'status_kawin'  =>  $data_ktp['STATUS_KAWIN'],
                'alamat'        =>  $data_ktp['ALAMAT'],
                'no_prop'       =>  $data_ktp['NO_PROP'],
                'prop_name'     =>  $data_ktp['PROP_NAME'],
                'no_kab'        =>  $data_ktp['NO_KAB'],
                'kab_name'      =>  $data_ktp['KAB_NAME'],
                'no_kec'        =>  $data_ktp['NO_KEC'],
                'kec_name'      =>  $data_ktp['KEC_NAME'],
                'no_kel'        =>  $data_ktp['NO_KEL'],
                'kel_name'      =>  $data_ktp['KEL_NAME'],
                'no_rw'         =>  $data_ktp['NO_RW'],
                'no_rt'         =>  $data_ktp['NO_RT'],
                'kode_pos'      =>  $data_ktp['KODE_POS'],
                'kepala_keluarga' => $nama_kk
            ];
        }

        if ($display[0] != '*')
            $data_ktp = elements($display, $data_ktp);

        return ['valid' => $valid, 'data' => $data_ktp];
    }
}
