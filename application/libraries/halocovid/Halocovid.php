<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Halocovid
{
    protected $CI;
    protected $halocovid_url;
    public function __construct()
    {
        $this->CI = &get_instance();
        header('Access-Control-Allow-Origin: https://halocovid.kedirikota.go.id');
        header("Access-Control-Allow-Credentials: true");
        // header("Referrer-Policy: no-referrer-when-downgrade");
        // header("Referrer-Policy: no-referrer");
        // header("Access-Control-Allow-Methods: GET, OPTIONS");
        $this->_init_constant();
    }

    private function _init_constant()
    {
        $production = false;

        if ($production === TRUE) {
            $this->halocovid_url = 'https://halocovid.kedirikota.go.id/';
        } else {
            $this->halocovid_url = 'http://127.0.0.1/halocovid/';
        }
    }

    public function sso()
    {
        $url    = $this->halocovid_url . 'auth/api/check_token_sso';
        $token  = $this->CI->input->post('token');
        
        $data = [
            'token' => $token
        ];
        
        return $this->_simple_post($url, $data);
    }

    public function logout()
    {
        if ($this->CI->session->userdata('halocovid_token')) {
            $url    = $this->halocovid_url . 'auth/api/logout_sso';
            $token  = $this->CI->session->userdata('halocovid_token');

            $data = [
                'token' => $token
            ];

            $this->_simple_post($url, $data);
        }
    }

    public function is_token_active()
    {
        if ($this->CI->session->userdata('halocovid_token')) {

            $url    = $this->halocovid_url . 'auth/api/is_token_active';
            $token  = $this->CI->session->userdata('halocovid_token');

            $data = [
                'token' => $token
            ];

            $active = $this->_simple_post($url, $data);

            if (!$active->valid) {
                $this->CI->session->sess_destroy();
            }
        }
    }

    private function _simple_post($url = '', $data = [])
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => $data,
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_SSL_VERIFYPEER => 0
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        return json_decode($response);
    }
}
