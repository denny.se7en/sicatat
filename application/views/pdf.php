<!DOCTYPE html>
<html>
<style>
    /* body{margin:50px 50px} */
</style>

<head>
    <meta charset='UTF-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1.0'>
    <title>Berita Acara</title>
</head>

<body>
    <img src="<?= base_url('assets/img/Kop.png') ?>" style="width:100%;margin-top :0px; margin-bottom:0px;"></img>
    <div style='text-align:center'>
        <h3 style="margin-top :0px; margin-bottom:0px;"><u>BERITA ACARA KESEPAKATAN RAPAT TEKNIS APLIKASI SPBE</u>
            <br>Nomor : 047 /&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;/ 419.113 / 2021
        </h3>
    </div>
    <div style="text-align:justify; text-justify: inter-word;margin-top :0px; margin-bottom:0px; margin-right:40px; margin-left:40px">
        <p style="margin-top :10px; margin-bottom:0px;"> &nbsp;&nbsp;&nbsp;&nbsp;Pada Hari <?=$tgl_haris?> dilaksanakan Rapat Teknis antara
            Dinas Komunikasi dan Informatika Kota Kediri dengan <?= $apps->nama_opd ?> Kota Kediri membahas tentang
            Aplikasi Sistem Pemerintah Berbasis Elektronik (SPBE) baru yang telah dibuat oleh <?= $apps->nama_opd ?>
            di <?=$apps->tempat_ba?><br>
            &nbsp;&nbsp;&nbsp;&nbsp;Dari hasil rapat teknis antara Dinas Komunikasi dan Informatika Kota Kediri dengan <?= $apps->nama_opd ?>
            Kota Kediri, disepakati beberapa hal, yaitu :<br>
        1. Maintenance aplikasi <?=$apps->hostname?> kedepan menjadi tanggung jawab <?= $apps->nama_maintenance ?><br>
        2. Pengembangan aplikasi <?=$apps->hostname?> kedepan menjadi tanggung jawab <?= $apps->nama_pengembangan ?><br>
        3. Pemeliharaan server aplikasi <?=$apps->hostname?> kedepan menjadi tanggung jawab <?= $apps->nama_server ?><br>
        4. Keamanan aplikasi (pemasangan SSL) aplikasi <?=$apps->hostname?> kedepan menjadi tanggung jawab <?= $apps->nama_ssl ?><br>
        5. Backup source code, database dan file aplikasi <?=$apps->hostname?> kedepan menjadi tanggung jawab <?= $apps->nama_backup ?><br>
        6. Apabila terjadi serangan (Hacker) pada Aplikasi <?=$apps->hostname?>, maka yang bertanggung jawab untuk melakukan tracing dan penanganannya (Restore) adalah <?= $apps->nama_restore ?></p>
        <p style="margin-top :10px; margin-bottom:0px;">Demikian berita acara kesepakatan ini dibuat dan dipergunakan sebagaimana mestinya.</p>

        <p style='text-align:center'>Yang Menyepakati,<br> Kediri, <?=$tgl_ids?></p>
        <table style="width:100%">
            <tr>
                <th style='text-align:center'>Kepala <?=$apps->nama_opd?></th>
                <th style='text-align:center'>Kepala Dinas Komunikasi dan Informatika</th>
            </tr>
            <tr>
                <td style='text-align:center'>Kota Kediri</td>
                <td style='text-align:center'>Kota Kediri</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td style='text-align:center'><u>Nama Kepala OPD</u></td>
                <td style='text-align:center'><u>Nama Kepala Diskominfo</u></td>
            </tr>
            <tr>
                <td style='text-align:center'>NIP Kepala OPD</td>
                <td style='text-align:center'>NIP Kepala Diskominfo</td>
            </tr>
        </table>
        <p style="margin-top :10px; margin-bottom:0px;">Catatan : <br>Apabila semua kewenangan 5 dan 6 point di atas diberikan kepada Dinas Kominfo, maka OPD berkewajiban untuk memberikan akses (username dan password) server, database, dan pengguna aplikasi kepada Kominfo).</p>
    </div>
</body>

</html>