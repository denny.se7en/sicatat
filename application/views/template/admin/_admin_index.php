<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>Si Catat Aplikasi</title>

    <?php
    foreach ($css_files as $index => $file) {
        if ($index != 0) {
            echo "\t";
        }

        if (strpos($file, "http://") !== false || strpos($file, "https://") !== false) {
            echo "<link type=\"text/css\" rel=\"stylesheet\" href=\"" . $file . "\" />\r\n";
        } else {
            echo "<link type=\"text/css\" rel=\"stylesheet\" href=\"" . base_url($file) . "\" />\r\n";
        }
    }

    echo "\n";

    foreach ($js_top as $index => $file) {
        echo "\t";

        if (strpos($file, "http://") !== false || strpos($file, "https://") !== false) {
            echo "<script src=\"" . $file . "\"></script>\r\n";
        } else {
            echo "<script src=\"" . base_url($file) . "\"></script>\r\n";
        }
    }

    ?>


</head>

<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed text-sm">
    <div class="wrapper">
        <?php if ($navbar) { ?>
            <!-- Navbar -->
            <?php $this->load->view('template/admin/admin_navbar') ?>
            <!-- /.navbar -->
        <?php } ?>
        <!-- Main Sidebar Container -->
        <?php $this->load->view('template/admin/admin_sidebar') ?>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper <?= !$padding ? 'p-0' : 'pb-4' ?> <?= !$navbar ? 'mt-0' : '' ?>" style="height:1px;overflow-y: auto;background-color:white!important;">

            <?php if ($header) { ?>
                <!-- Content Header (Page header) -->
                <?php $this->load->view('template/admin/admin_content_header') ?>
                <!-- /.content-header -->
            <?php } ?>

            <!-- Main content -->
            <div class="content <?= !$padding ? 'p-0' : '' ?>" style="min-height: 100%;">
                <div class="container-fluid <?= !$padding ? 'p-0' : '' ?>" style="min-height: 100%;">

                    <?php $this->load->view($page) ?>

                </div>
                <!--/. container-fluid -->
            </div>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <!-- Control Sidebar -->
        <aside class="control-sidebar control-sidebar-dark">
            <!-- Control sidebar content goes here -->
        </aside>
        <!-- /.control-sidebar -->

    </div>
    <!-- ./wrapper -->

    <!-- REQUIRED SCRIPTS -->
    <?php
    foreach ($js_files as $index => $file) {
        if ($index != 0) {
            echo "\t";
        }

        if (strpos($file, "http://") !== false || strpos($file, "https://") !== false) {
            echo "<script src=\"" . $file . "\"></script>\r\n";
        } else {
            echo "<script src=\"" . base_url($file) . "\"></script>\r\n";
        }
    }
    ?>

</body>

</html>