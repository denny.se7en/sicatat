<aside class="main-sidebar sidebar-light-primary elevation-1">
    <!-- Brand Logo -->
    <a href="<?= base_url() ?>" class="d-flex justify-content-start align-items-center brand-link">
        <img src="<?= base_url('assets/') ?>img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-2" style="opacity: 1">
        <span class="brand-text font-weight-bold">Si Catat Aplikasi</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex align-items-center">
            <div class="image">
                <img src="<?= base_url('assets/') ?>img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info" style="width: 100%;">
                <div class="d-block"><?= $this->session->nama ?></div>
                <div class="d-block" style="font-size: 0.8rem;color:grey;"><?= $this->session->jabatan ?></div>
                <div class="d-flex mt-2 justify-content-start">
                    <a href="<?= base_url('Auth/setting') ?>">
                        <div class="px-2 py-1 mr-2 d-flex align-items-center badge-info" style="border-radius: 5px;cursor:pointer;">
                            <div class="material-icons-outlined pr-1" style="font-size: 0.7rem;">power_settings_new</div>
                            <div style="font-size: 0.7rem;">Setting</div>
                        </div>
                    </a>
                    <a href="<?= base_url('Auth/logout') ?>">
                        <div class="px-2 py-1 mr-0 d-flex align-items-center badge-danger" style="border-radius: 5px;cursor:pointer;width: 100%;">
                            <div class="material-icons-outlined pr-1" style="font-size: 0.7rem;">power_settings_new</div>
                            <div style="font-size: 0.7rem;">Logout</div>
                        </div>
                    </a>
                </div>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column nav-child-indent" data-widget="treeview" role="menu" data-accordion="false">

                <?php foreach ($menus as $key => $category) {
                    if ($category->child) {

                        if ($category->name) {
                            echo '<li class="nav-header">' . strtoupper($category->name) . '</li>';
                        } else {
                            echo '';
                        }

                        // PARENT MENU
                        foreach ($category->child as $menu) {

                            if (strpos($menu->url, "http://") !== false || strpos($menu->url, "https://") !== false) {
                                $url = $menu->url;
                            } else {
                                $url = base_url($menu->url);
                            }

                ?>
                            <li class="nav-item <?= $menu->child ? 'has-treeview' : '' ?>">
                                <a href="<?= $url ?>" class="nav-link d-flex align-items-center">
                                    <?php
                                    if ($menu->icon_type == 'fa' && $menu->icon_keyword) echo '<i class="nav-icon ' . $menu->icon_keyword . '"></i>';
                                    if ($menu->icon_type == 'md' && $menu->icon_keyword) echo '<i class="nav-icon material-icons-outlined mr-3" style="font-size:1.5rem;">' . $menu->icon_keyword . '</i>';
                                    ?>
                                    <p>
                                        <?= $menu->name ?>
                                        <?= isset($menu->child) && $menu->child ? '<i class="right fas fa-angle-left"></i>' : '' ?>
                                        <?= isset($menu->badge_type) && $menu->badge_type && isset($badge[$menu->badge_keyword]) ? '<span class="badge ' . $menu->badge_type . ' right">' . $badge[$menu->badge_keyword] . '</span>' : '' ?>
                                    </p>
                                </a>

                                <?php if ($menu->child) { ?>
                                    <ul class="nav nav-treeview">
                                        <?php
                                        // CHILD MENU
                                        foreach ($menu->child as $child) {
                                            if (strpos($child->url, "http://") !== false || strpos($child->url, "https://") !== false) {
                                                $url = $child->url;
                                            } else {
                                                $url = base_url($child->url);
                                            }
                                        ?>
                                            <li class="nav-item">
                                                <a href="<?= $url ?>" class="nav-link d-flex align-items-center" style="color: #000;">
                                                    <!-- <i class="nav-icon material-icons-outlined">subdirectory_arrow_right</i> -->
                                                    <?php
                                                    if ($child->icon_type == 'fa' && $child->icon_keyword) echo '<i class="nav-icon ' . $child->icon_keyword . '"></i>';
                                                    if ($child->icon_type == 'md' && $child->icon_keyword) echo '<i class="nav-icon material-icons-outlined">' . $child->icon_keyword . '</i>';
                                                    ?>
                                                    <p><?= $child->name ?></p>
                                                    <?= isset($child->badge_type) && $child->badge_type && isset($badge[$child->badge_keyword]) ? '<span class="badge ' . $child->badge_type . ' right">' . $badge[$child->badge_keyword] . '</span>' : '' ?>
                                                </a>
                                            </li>
                                        <?php } ?>
                                    </ul>
                                <?php } ?>

                            </li>
                <?php
                        }
                    }
                }
                ?>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>