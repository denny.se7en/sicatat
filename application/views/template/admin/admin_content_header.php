<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark"><?= isset($title) && $title ? $title : '' ?></h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <?php
                    if(isset($breadcrumbs) && $breadcrumbs) {
                        foreach ($breadcrumbs as $index => $breadcrumb) {
                            if (($index + 1) < count($breadcrumbs)) {
                                echo '<li class="breadcrumb-item"><a href="#">' . $breadcrumb . '</a></li>';
                            } else {
                                echo '<li class="breadcrumb-item active">' . $breadcrumb . '</li>';
                            }
                        }
                    }
                    ?>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>