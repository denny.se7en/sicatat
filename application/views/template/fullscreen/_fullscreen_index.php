<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>SI Catat Kota Kediri</title>

    <?php
    foreach ($css_files as $index => $file) {
        if ($index != 0) {
            echo "\t";
        }

        if (strpos($file, "http://") !== false || strpos($file, "https://") !== false) {
            echo "<link type=\"text/css\" rel=\"stylesheet\" href=\"" . $file . "\" />\r\n";
        } else {
            echo "<link type=\"text/css\" rel=\"stylesheet\" href=\"" . base_url($file) . "\" />\r\n";
        }
    }

    echo "\n";

    foreach ($js_top as $index => $file) {
        echo "\t";

        if (strpos($file, "http://") !== false || strpos($file, "https://") !== false) {
            echo "<script src=\"" . $file . "\"></script>\r\n";
        } else {
            echo "<script src=\"" . base_url($file) . "\"></script>\r\n";
        }
    }

    ?>


</head>

<body>

    <?php $this->load->view($page) ?>

    <!-- REQUIRED SCRIPTS -->
    <?php
    foreach ($js_files as $index => $file) {
        if ($index != 0) {
            echo "\t";
        }

        if (strpos($file, "http://") !== false || strpos($file, "https://") !== false) {
            echo "<script src=\"" . $file . "\"></script>\r\n";
        } else {
            echo "<script src=\"" . base_url($file) . "\"></script>\r\n";
        }
    }
    ?>

</body>

</html>