function pieDiagram(canvasId, data, mode = 1, height = "") {
    let canvas = document.getElementById(canvasId);
    let ctx = canvas.getContext("2d");

    let chart = new Chart(ctx, {
        type: "pie",
        data: {
            labels: data.labels,
            datasets: data.datasets,
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            rotation: mode * Math.PI,
            circumference: mode * Math.PI,
            legend: {
                // display: false,
                position: "right",
                onHover: function (e) {
                    e.target.style.cursor = "pointer";
                },
            },
            animation: {
                animateScale: true,
                animateRotate: true,
            },
            layout: {
                padding: 30,
            },
            plugins: {
                datalabels: {
                    color: 'white',
                    labels: {
                        title: {
                            font: {
                                weight: 'bold'
                            }
                        },
                        value: {
                            color: 'green'
                        }
                    }
                }
            },
            hover: {
                onHover: function (e) {
                    var point = this.getElementAtEvent(e);
                    if (point.length) e.target.style.cursor = "pointer";
                    else e.target.style.cursor = "default";
                },
            }
        },
    });

}


function doughnutDiagram(canvasId, data, mode = 1, height = "") {
    let ctx = document.getElementById(canvasId).getContext("2d");

    new Chart(ctx, {
        type: "doughnut",
        data: {
            labels: data.labels,
            datasets: data.datasets,
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            rotation: mode * Math.PI,
            circumference: mode * Math.PI,
            legend: {
                position: "right",
                onHover: function (e) {
                    e.target.style.cursor = "pointer";
                },
            },
            animation: {
                animateScale: true,
                animateRotate: true,
            },
            layout: {
                padding: 30,
            },
            plugins: {
                datalabels: {
                    color: 'white',
                    labels: {
                        title: {
                            font: {
                                weight: 'bold'
                            }
                        },
                        value: {
                            color: 'green'
                        }
                    }
                }
            },
            hover: {
                onHover: function (e) {
                    var point = this.getElementAtEvent(e);
                    if (point.length) e.target.style.cursor = "pointer";
                    else e.target.style.cursor = "default";
                },
            },
        },
    });
}

function barDiagram(canvasId, data, height = "") {
    let ctx = document.getElementById(canvasId);
    if (height != "") ctx.height = height;

    let myBarChart = new Chart(ctx.getContext("2d"), {
        type: "bar",
        data: {
            labels: data.labels,
            datasets: data.datasets,
        },
        options: {
            scales: {
                xAxes: [
                    {
                        ticks: {
                            beginAtZero: true,
                            max: data.options.xMaxTicks,
                        },
                    },
                ],
                yAxes: [
                    {
                        ticks: {
                            beginAtZero: true,
                            max: data.options.yMaxTicks,
                        },
                    },
                ],
            },
            layout: {
                padding: 10,
            },
            plugins: {
                datalabels: {
                    color: 'white',
                    labels: {
                        title: {
                            font: {
                                weight: 'bold'
                            }
                        },
                        value: {
                            color: 'green'
                        }
                    }
                }
            },
            maintainAspectRatio: false,
            responsive: true,
            hover: {
                onHover: function (e) {
                    var point = this.getElementAtEvent(e);
                    if (point.length) e.target.style.cursor = "pointer";
                    else e.target.style.cursor = "default";
                },
            },
        },
    });
}

function horizontalBarDiagram(canvasId, data, height = "") {
    let ctx = document.getElementById(canvasId);
    if (height != "") ctx.height = height;

    let myBarChart = new Chart(ctx.getContext("2d"), {
        type: "horizontalBar",
        data: {
            labels: data.labels,
            datasets: data.datasets,
        },
        options: {
            scales: {
                xAxes: [
                    {
                        ticks: {
                            beginAtZero: true,
                            max: data.options.xMaxTicks,
                        },
                    },
                ],
                yAxes: [
                    {
                        ticks: {
                            beginAtZero: true,
                            max: 100,
                        },
                    },
                ],
            },
            plugins: {
                datalabels: {
                    color: 'white',
                    labels: {
                        title: {
                            font: {
                                weight: 'bold'
                            }
                        },
                        value: {
                            color: 'green'
                        }
                    }
                }
            },
            layout: {
                padding: 10,
            },
            maintainAspectRatio: false,
            responsive: true,
            hover: {
                onHover: function (e) {
                    var point = this.getElementAtEvent(e);
                    if (point.length) e.target.style.cursor = "pointer";
                    else e.target.style.cursor = "default";
                },
            },
        },
    });
}

function lineDiagram(canvasId, data, height = "") {
    let ctx = document.getElementById(canvasId);
    if (height != "") ctx.height = height;

    let myBarChart = new Chart(ctx.getContext("2d"), {
        type: "line",
        data: {
            labels: data.labels,
            datasets: data.datasets,
        },
        options: {
            scales: {
                xAxes: [
                    {
                        ticks: {
                            beginAtZero: true,
                            max: data.options.xMaxTicks,
                        },
                    },
                ],
                yAxes: [
                    {
                        ticks: {
                            beginAtZero: true,
                            max: 100,
                        },
                    },
                ],
            },
            plugins: {
                datalabels: {
                    color: 'white',
                    labels: {
                        title: {
                            font: {
                                weight: 'bold'
                            }
                        },
                        value: {
                            color: 'green'
                        }
                    }
                }
            },
            layout: {
                padding: 10,
            },
            maintainAspectRatio: false,
            responsive: true,
            hover: {
                onHover: function (e) {
                    var point = this.getElementAtEvent(e);
                    if (point.length) e.target.style.cursor = "pointer";
                    else e.target.style.cursor = "default";
                },
            },
        },
    });
}
